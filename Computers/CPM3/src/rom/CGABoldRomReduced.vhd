--
-- Multicore 2 / Multicore 2+
--
-- Copyright (c) 2017-2020 - Victor Trucco
--
-- All rights reserved
--
-- Redistribution and use in source and synthezised forms, with or without
-- modification, are permitted provided that the following conditions are met:
--
-- Redistributions of source code must retain the above copyright notice,
-- this list of conditions and the following disclaimer.
--
-- Redistributions in synthesized form must reproduce the above copyright
-- notice, this list of conditions and the following disclaimer in the
-- documentation and/or other materials provided with the distribution.
--
-- Neither the name of the author nor the names of other contributors may
-- be used to endorse or promote products derived from this software without
-- specific prior written permission.
--
-- THIS CODE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
-- AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
-- THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
-- PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE
-- LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
-- CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
-- SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
-- INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
-- CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
-- ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
-- POSSIBILITY OF SUCH DAMAGE.
--
-- You are responsible for any legal issues arising from your use of this code.
--
		
-- generated with romgen v3.0 by MikeJ
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.numeric_std.all;

entity CGABoldRomReduced is
	port (
		clk		: in    std_logic;
		addr		: in    std_logic_vector(9 downto 0);
		data		: out   std_logic_vector(7 downto 0)
	);
end;

architecture rtl of CGABoldRomReduced is

	type ROM_ARRAY is array(0 to 1023) of std_logic_vector(7 downto 0);
	constant ROM : ROM_ARRAY := (
		x"00",x"00",x"00",x"00",x"00",x"00",x"00",x"00", -- 0x0000
		x"7E",x"81",x"A5",x"81",x"BD",x"99",x"81",x"7E", -- 0x0008
		x"7E",x"FF",x"DB",x"FF",x"C3",x"E7",x"FF",x"7E", -- 0x0010
		x"6C",x"FE",x"FE",x"FE",x"7C",x"38",x"10",x"00", -- 0x0018
		x"10",x"38",x"7C",x"FE",x"7C",x"38",x"10",x"00", -- 0x0020
		x"38",x"7C",x"38",x"FE",x"FE",x"D6",x"10",x"38", -- 0x0028
		x"10",x"10",x"38",x"7C",x"FE",x"7C",x"10",x"38", -- 0x0030
		x"00",x"00",x"18",x"3C",x"3C",x"18",x"00",x"00", -- 0x0038
		x"FF",x"FF",x"E7",x"C3",x"C3",x"E7",x"FF",x"FF", -- 0x0040
		x"00",x"3C",x"66",x"42",x"42",x"66",x"3C",x"00", -- 0x0048
		x"FF",x"C3",x"99",x"BD",x"BD",x"99",x"C3",x"FF", -- 0x0050
		x"0F",x"07",x"0F",x"7D",x"CC",x"CC",x"CC",x"78", -- 0x0058
		x"3C",x"66",x"66",x"66",x"3C",x"18",x"7E",x"18", -- 0x0060
		x"3F",x"33",x"3F",x"30",x"30",x"70",x"F0",x"E0", -- 0x0068
		x"7F",x"63",x"7F",x"63",x"63",x"67",x"E6",x"C0", -- 0x0070
		x"18",x"DB",x"3C",x"E7",x"E7",x"3C",x"DB",x"18", -- 0x0078
		x"80",x"E0",x"F8",x"FE",x"F8",x"E0",x"80",x"00", -- 0x0080
		x"02",x"0E",x"3E",x"FE",x"3E",x"0E",x"02",x"00", -- 0x0088
		x"18",x"3C",x"7E",x"18",x"18",x"7E",x"3C",x"18", -- 0x0090
		x"66",x"66",x"66",x"66",x"66",x"00",x"66",x"00", -- 0x0098
		x"7F",x"DB",x"DB",x"7B",x"1B",x"1B",x"1B",x"00", -- 0x00A0
		x"3E",x"63",x"38",x"6C",x"6C",x"38",x"CC",x"78", -- 0x00A8
		x"00",x"00",x"00",x"00",x"7E",x"7E",x"7E",x"00", -- 0x00B0
		x"18",x"3C",x"7E",x"18",x"7E",x"3C",x"18",x"FF", -- 0x00B8
		x"18",x"3C",x"7E",x"18",x"18",x"18",x"18",x"00", -- 0x00C0
		x"18",x"18",x"18",x"18",x"7E",x"3C",x"18",x"00", -- 0x00C8
		x"00",x"18",x"0C",x"FE",x"0C",x"18",x"00",x"00", -- 0x00D0
		x"00",x"30",x"60",x"FE",x"60",x"30",x"00",x"00", -- 0x00D8
		x"00",x"00",x"C0",x"C0",x"C0",x"FE",x"00",x"00", -- 0x00E0
		x"00",x"24",x"66",x"FF",x"66",x"24",x"00",x"00", -- 0x00E8
		x"00",x"18",x"3C",x"7E",x"FF",x"FF",x"00",x"00", -- 0x00F0
		x"00",x"FF",x"FF",x"7E",x"3C",x"18",x"00",x"00", -- 0x00F8
		x"00",x"00",x"00",x"00",x"00",x"00",x"00",x"00", -- 0x0100
		x"30",x"78",x"78",x"30",x"30",x"00",x"30",x"00", -- 0x0108
		x"6C",x"6C",x"6C",x"00",x"00",x"00",x"00",x"00", -- 0x0110
		x"6C",x"6C",x"FE",x"6C",x"FE",x"6C",x"6C",x"00", -- 0x0118
		x"30",x"7C",x"C0",x"78",x"0C",x"F8",x"30",x"00", -- 0x0120
		x"00",x"C6",x"CC",x"18",x"30",x"66",x"C6",x"00", -- 0x0128
		x"38",x"6C",x"38",x"76",x"DC",x"CC",x"76",x"00", -- 0x0130
		x"60",x"60",x"C0",x"00",x"00",x"00",x"00",x"00", -- 0x0138
		x"18",x"30",x"60",x"60",x"60",x"30",x"18",x"00", -- 0x0140
		x"60",x"30",x"18",x"18",x"18",x"30",x"60",x"00", -- 0x0148
		x"00",x"66",x"3C",x"FF",x"3C",x"66",x"00",x"00", -- 0x0150
		x"00",x"30",x"30",x"FC",x"30",x"30",x"00",x"00", -- 0x0158
		x"00",x"00",x"00",x"00",x"00",x"30",x"30",x"60", -- 0x0160
		x"00",x"00",x"00",x"FC",x"00",x"00",x"00",x"00", -- 0x0168
		x"00",x"00",x"00",x"00",x"00",x"30",x"30",x"00", -- 0x0170
		x"06",x"0C",x"18",x"30",x"60",x"C0",x"80",x"00", -- 0x0178
		x"7C",x"C6",x"CE",x"DE",x"F6",x"E6",x"7C",x"00", -- 0x0180
		x"30",x"70",x"30",x"30",x"30",x"30",x"FC",x"00", -- 0x0188
		x"78",x"CC",x"0C",x"38",x"60",x"CC",x"FC",x"00", -- 0x0190
		x"78",x"CC",x"0C",x"38",x"0C",x"CC",x"78",x"00", -- 0x0198
		x"1C",x"3C",x"6C",x"CC",x"FE",x"0C",x"1E",x"00", -- 0x01A0
		x"FC",x"C0",x"F8",x"0C",x"0C",x"CC",x"78",x"00", -- 0x01A8
		x"38",x"60",x"C0",x"F8",x"CC",x"CC",x"78",x"00", -- 0x01B0
		x"FC",x"CC",x"0C",x"18",x"30",x"30",x"30",x"00", -- 0x01B8
		x"78",x"CC",x"CC",x"78",x"CC",x"CC",x"78",x"00", -- 0x01C0
		x"78",x"CC",x"CC",x"7C",x"0C",x"18",x"70",x"00", -- 0x01C8
		x"00",x"30",x"30",x"00",x"00",x"30",x"30",x"00", -- 0x01D0
		x"00",x"30",x"30",x"00",x"00",x"30",x"30",x"60", -- 0x01D8
		x"18",x"30",x"60",x"C0",x"60",x"30",x"18",x"00", -- 0x01E0
		x"00",x"00",x"FC",x"00",x"00",x"FC",x"00",x"00", -- 0x01E8
		x"60",x"30",x"18",x"0C",x"18",x"30",x"60",x"00", -- 0x01F0
		x"78",x"CC",x"0C",x"18",x"30",x"00",x"30",x"00", -- 0x01F8
		x"7C",x"C6",x"DE",x"DE",x"DE",x"C0",x"78",x"00", -- 0x0200
		x"30",x"78",x"CC",x"CC",x"FC",x"CC",x"CC",x"00", -- 0x0208
		x"FC",x"66",x"66",x"7C",x"66",x"66",x"FC",x"00", -- 0x0210
		x"3C",x"66",x"C0",x"C0",x"C0",x"66",x"3C",x"00", -- 0x0218
		x"F8",x"6C",x"66",x"66",x"66",x"6C",x"F8",x"00", -- 0x0220
		x"FE",x"62",x"68",x"78",x"68",x"62",x"FE",x"00", -- 0x0228
		x"FE",x"62",x"68",x"78",x"68",x"60",x"F0",x"00", -- 0x0230
		x"3C",x"66",x"C0",x"C0",x"CE",x"66",x"3E",x"00", -- 0x0238
		x"CC",x"CC",x"CC",x"FC",x"CC",x"CC",x"CC",x"00", -- 0x0240
		x"78",x"30",x"30",x"30",x"30",x"30",x"78",x"00", -- 0x0248
		x"1E",x"0C",x"0C",x"0C",x"CC",x"CC",x"78",x"00", -- 0x0250
		x"E6",x"66",x"6C",x"78",x"6C",x"66",x"E6",x"00", -- 0x0258
		x"F0",x"60",x"60",x"60",x"62",x"66",x"FE",x"00", -- 0x0260
		x"C6",x"EE",x"FE",x"FE",x"D6",x"C6",x"C6",x"00", -- 0x0268
		x"C6",x"E6",x"F6",x"DE",x"CE",x"C6",x"C6",x"00", -- 0x0270
		x"38",x"6C",x"C6",x"C6",x"C6",x"6C",x"38",x"00", -- 0x0278
		x"FC",x"66",x"66",x"7C",x"60",x"60",x"F0",x"00", -- 0x0280
		x"78",x"CC",x"CC",x"CC",x"DC",x"78",x"1C",x"00", -- 0x0288
		x"FC",x"66",x"66",x"7C",x"6C",x"66",x"E6",x"00", -- 0x0290
		x"78",x"CC",x"60",x"30",x"18",x"CC",x"78",x"00", -- 0x0298
		x"FC",x"B4",x"30",x"30",x"30",x"30",x"78",x"00", -- 0x02A0
		x"CC",x"CC",x"CC",x"CC",x"CC",x"CC",x"FC",x"00", -- 0x02A8
		x"CC",x"CC",x"CC",x"CC",x"CC",x"78",x"30",x"00", -- 0x02B0
		x"C6",x"C6",x"C6",x"D6",x"FE",x"EE",x"C6",x"00", -- 0x02B8
		x"C6",x"C6",x"6C",x"38",x"38",x"6C",x"C6",x"00", -- 0x02C0
		x"CC",x"CC",x"CC",x"78",x"30",x"30",x"78",x"00", -- 0x02C8
		x"FE",x"C6",x"8C",x"18",x"32",x"66",x"FE",x"00", -- 0x02D0
		x"78",x"60",x"60",x"60",x"60",x"60",x"78",x"00", -- 0x02D8
		x"C0",x"60",x"30",x"18",x"0C",x"06",x"02",x"00", -- 0x02E0
		x"78",x"18",x"18",x"18",x"18",x"18",x"78",x"00", -- 0x02E8
		x"10",x"38",x"6C",x"C6",x"00",x"00",x"00",x"00", -- 0x02F0
		x"00",x"00",x"00",x"00",x"00",x"00",x"00",x"FF", -- 0x02F8
		x"30",x"30",x"18",x"00",x"00",x"00",x"00",x"00", -- 0x0300
		x"00",x"00",x"78",x"0C",x"7C",x"CC",x"76",x"00", -- 0x0308
		x"E0",x"60",x"60",x"7C",x"66",x"66",x"DC",x"00", -- 0x0310
		x"00",x"00",x"78",x"CC",x"C0",x"CC",x"78",x"00", -- 0x0318
		x"1C",x"0C",x"0C",x"7C",x"CC",x"CC",x"76",x"00", -- 0x0320
		x"00",x"00",x"78",x"CC",x"FC",x"C0",x"78",x"00", -- 0x0328
		x"38",x"6C",x"60",x"F0",x"60",x"60",x"F0",x"00", -- 0x0330
		x"00",x"00",x"76",x"CC",x"CC",x"7C",x"0C",x"F8", -- 0x0338
		x"E0",x"60",x"6C",x"76",x"66",x"66",x"E6",x"00", -- 0x0340
		x"30",x"00",x"70",x"30",x"30",x"30",x"78",x"00", -- 0x0348
		x"0C",x"00",x"0C",x"0C",x"0C",x"CC",x"CC",x"78", -- 0x0350
		x"E0",x"60",x"66",x"6C",x"78",x"6C",x"E6",x"00", -- 0x0358
		x"70",x"30",x"30",x"30",x"30",x"30",x"78",x"00", -- 0x0360
		x"00",x"00",x"CC",x"FE",x"FE",x"D6",x"C6",x"00", -- 0x0368
		x"00",x"00",x"F8",x"CC",x"CC",x"CC",x"CC",x"00", -- 0x0370
		x"00",x"00",x"78",x"CC",x"CC",x"CC",x"78",x"00", -- 0x0378
		x"00",x"00",x"DC",x"66",x"66",x"7C",x"60",x"F0", -- 0x0380
		x"00",x"00",x"76",x"CC",x"CC",x"7C",x"0C",x"1E", -- 0x0388
		x"00",x"00",x"DC",x"76",x"66",x"60",x"F0",x"00", -- 0x0390
		x"00",x"00",x"7C",x"C0",x"78",x"0C",x"F8",x"00", -- 0x0398
		x"10",x"30",x"7C",x"30",x"30",x"34",x"18",x"00", -- 0x03A0
		x"00",x"00",x"CC",x"CC",x"CC",x"CC",x"76",x"00", -- 0x03A8
		x"00",x"00",x"CC",x"CC",x"CC",x"78",x"30",x"00", -- 0x03B0
		x"00",x"00",x"C6",x"D6",x"FE",x"FE",x"6C",x"00", -- 0x03B8
		x"00",x"00",x"C6",x"6C",x"38",x"6C",x"C6",x"00", -- 0x03C0
		x"00",x"00",x"CC",x"CC",x"CC",x"7C",x"0C",x"F8", -- 0x03C8
		x"00",x"00",x"FC",x"98",x"30",x"64",x"FC",x"00", -- 0x03D0
		x"1C",x"30",x"30",x"E0",x"30",x"30",x"1C",x"00", -- 0x03D8
		x"18",x"18",x"18",x"00",x"18",x"18",x"18",x"00", -- 0x03E0
		x"E0",x"30",x"30",x"1C",x"30",x"30",x"E0",x"00", -- 0x03E8
		x"76",x"DC",x"00",x"00",x"00",x"00",x"00",x"00", -- 0x03F0
		x"00",x"10",x"38",x"6C",x"C6",x"C6",x"FE",x"00"  -- 0x03F8
	);

begin

	process(clk)
	begin
		if rising_edge(clk) then
			data <= ROM(to_integer(unsigned(addr)));
		end if;
	end process;
end RTL;
