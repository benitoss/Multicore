/*

*/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "jv3.h"

#define GAPI   "\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00"
#define GAPII  "\x4E\x4E\x4E\x4E\x4E\x4E\x4E\x4E\x4E\x4E\x4E\x4E\x4E\x4E\x4E\x4E\x4E\x4E\x4E\x4E\x4E\x4E\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00"
#define GAPIII "\x4E\x4E\x4E\x4E\x4E\x4E\x4E\x4E\x4E\x4E\x4E\x4E\x4E\x4E\x4E\x4E\x4E\x4E\x4E\x4E\x4E\x4E\x00\x00\x00\x00\x00\x00\x00\x00"
#define GAPIV  "\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00"

#define POLY 0x1021
/*
//                                      16   12   5
// this is the CCITT CRC 16 polynomial X  + X  + X  + 1.
// This works out to be 0x1021, but the way the algorithm works
// lets us use 0x8408 (the reverse of the bit pattern).  The high
// bit is always assumed to be set, thus we only use 16 bits to
// represent the 17 bit value.
*/
unsigned short crc16(unsigned int CRC_init, char *data_p, unsigned short length)
{
	unsigned char i;
	unsigned int data;
	unsigned int crc = CRC_init;

	if (length == 0)
		return (~crc);

	do {
		for (i=0, data=((unsigned int)0xff & *data_p++) << 8; i < 8; i++, data <<= 1) {
			if ((crc & 0x8000) ^ (data & 0x8000)) {
				crc = (crc << 1) ^ POLY;
			} else {
				crc <<= 1;
			}
		}
	} while (--length);

//	crc = ~crc;
//	data = crc;
//	crc = (crc << 8) | (data >> 8 & 0xff);

	return (crc);
}

int main(int argc, char **argv) {
	FILE      *arqin;
	int       fileSize, trilha, setor, flags;
	int       i, t, s, ts, p, DAM, crc;
	char      *dados, trilhasetor[40][18][257];
	unsigned char bcrc[8];
	JV3_block bloco;

	if (argc < 3) {
		fprintf(stderr, "Use: %s <JV3 file> <MFM file>\n", argv[0]);
		return -1;
	}

	arqin = fopen((const char *)argv[1], "rb");
	if (!arqin) {
		fprintf(stderr, "Error opening %s\n", argv[1]);
		return -2;
	}

	fseek(arqin, 0L, SEEK_END);
	fileSize = ftell(arqin);
	fseek(arqin, 0L, SEEK_SET);

	dados = (char *)malloc(fileSize+1);
	fread(dados, fileSize, 1, arqin);
	fclose(arqin);

	memcpy(&bloco, dados, sizeof(JV3_block));
	p = sizeof(JV3_block);

	memset(trilhasetor, 0, sizeof(trilhasetor));

	for (i=0; i < 2901; i++) {
		if (bloco.headers1[i].track == 255) break;
		trilha = bloco.headers1[i].track;
		setor  = bloco.headers1[i].sector;
		flags = bloco.headers1[i].flags;
		DAM = 0;
		if (flags & JV3_DENSITY) {
			switch (flags & JV3_DAM) {
			case JV3_DAMDDF8:
				DAM = 0xF8;
				break;
			case JV3_DAMDDFB:
				DAM = 0xFB;
				break;
			}
		} else {
			switch (flags & JV3_DAM) {
			case JV3_DAMSDF8:
				DAM = 0xF8;
				break;

			case JV3_DAMSDF9:
				DAM = 0xF9;
				break;

			case JV3_DAMSDFA:
				DAM = 0xFA;
				break;

			case JV3_DAMSDFB:
				DAM = 0xFB;
				break;
			}
		}
		switch (flags & JV3_SIZE) {
		case JV3_SIZE256:
			ts = 256;
			break;

		case JV3_SIZE128:
			ts = 128;
			break;

		case JV3_SIZE1024:
			ts = 1024;
			break;

		case JV3_SIZE512:
			ts = 512;
			break;
		}
		if (ts != 256) {
			fprintf(stderr, "Track %2d, sector %2d != 256 bytes!\n", trilha, setor);
			return -3;
		}

		if (trilha > 39) {
			fprintf(stderr, "Track > 39\n");
			return -4;
		}

		if (setor > 18) {
			fprintf(stderr, "Sector > 18\n");
			return -4;
		}

		if ((p + ts) > fileSize) {
			fprintf(stderr, "Data miss!!\n");
			return -4;
		}

		memcpy(trilhasetor[trilha][setor-1], dados + p, 256);
		trilhasetor[trilha][setor-1][256] = DAM;
		p += ts;
	}

	free(dados);

	arqin = fopen((const char *)argv[2], "wb");
	if (!arqin) {
		fprintf(stderr, "Error opening %s\n", argv[2]);
		return -2;
	}
	for (t = 0; t < 40; t++) {
		for (s=0; s < 18; s++) {
			// escrever gaps e ids
			fwrite(GAPII, sizeof(GAPII)-1, 1, arqin);	// GAP II
			bcrc[0] = 0xA1;								// 
			bcrc[1] = 0xA1;								// 
			bcrc[2] = 0xA1;								// 
			bcrc[3] = 0xFE;								// IDAM
			bcrc[4] = t;								// Trilha
			bcrc[5] = 0;								// Lado
			bcrc[6] = s+1;								// Setor
			bcrc[7] = 1;								// Sector Length = 256
			fwrite(bcrc, 8, 1, arqin);
			crc = crc16(0xFFFF, (char *)bcrc, 8);
			fputc((crc >> 8) & 0xFF, arqin);			// CRC 1
			fputc(crc & 0xFF, arqin);					// CRC 2
			fwrite(GAPIII, sizeof(GAPIII)-1, 1, arqin);	// GAP III
			bcrc[0] = 0xA1;								// 
			bcrc[1] = 0xA1;								// 
			bcrc[2] = 0xA1;								// 
			bcrc[3] = trilhasetor[t][s][256];			// DAM
			fwrite(bcrc, 4, 1, arqin);
//			crc = crc16(0xFFFF, (char *)bcrc, 4);
			// escrever dados
			fwrite(trilhasetor[t][s], 256, 1, arqin);
			crc = crc16(crc, trilhasetor[t][s], 256);
			fputc((crc >> 8) & 0xFF, arqin);			// CRC 1
			fputc(crc & 0xFF, arqin);					// CRC 2
			// escrever gap entre setores
			fwrite(GAPIV, sizeof(GAPIV)-1, 1, arqin);		// GAP IV
		}
		// escrever gap entre fim e inicio da trilha
		fwrite(GAPI, sizeof(GAPI)-1, 1, arqin);			// GAP I
		ts = sizeof(GAPII)-1 + 8 + 2 + sizeof(GAPIII)-1 + 4 + 256 + 2 + sizeof(GAPIV)-1;
		ts *= 18;
		ts += sizeof(GAPI)-1;
		ts = 6272 - ts;
		while (ts > 0) {
			fputc(0xFF, arqin);
			--ts;
		}
	}
	fprintf(stderr, "Ready.\n");
	return 0;
}
