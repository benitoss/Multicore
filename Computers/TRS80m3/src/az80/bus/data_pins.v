/*
  
   Multicore 2 / Multicore 2+
  
   Copyright (c) 2017-2020 - Victor Trucco

  
   All rights reserved
  
   Redistribution and use in source and synthezised forms, with or without
   modification, are permitted provided that the following conditions are met:
  
   Redistributions of source code must retain the above copyright notice,
   this list of conditions and the following disclaimer.
  
   Redistributions in synthesized form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
  
   Neither the name of the author nor the names of other contributors may
   be used to endorse or promote products derived from this software without
   specific prior written permission.
  
   THIS CODE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
   AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
   THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
   PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE
   LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
   CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
   INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
   CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
   ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
   POSSIBILITY OF SUCH DAMAGE.
  
   You are responsible for any legal issues arising from your use of this code.
  
*/// Copyright (C) 1991-2013 Altera Corporation
// Your use of Altera Corporation's design tools, logic functions 
// and other software and tools, and its AMPP partner logic 
// functions, and any output files from any of the foregoing 
// (including device programming or simulation files), and any 
// associated documentation or information are expressly subject 
// to the terms and conditions of the Altera Program License 
// Subscription Agreement, Altera MegaCore Function License 
// Agreement, or other applicable license agreement, including, 
// without limitation, that your use is for the sole purpose of 
// programming logic devices manufactured by Altera and sold by 
// Altera or its authorized distributors.  Please refer to the 
// applicable agreement for further details.

// PROGRAM		"Quartus II 64-Bit"
// VERSION		"Version 13.0.1 Build 232 06/12/2013 Service Pack 1 SJ Web Edition"
// CREATED		"Thu Nov 06 23:28:26 2014"

module data_pins(
	bus_db_pin_oe,
	bus_db_pin_re,
	ctl_bus_db_we,
	clk,
	ctl_bus_db_oe,
	D,
	db
);


input wire	bus_db_pin_oe;
input wire	bus_db_pin_re;
input wire	ctl_bus_db_we;
input wire	clk;
input wire	ctl_bus_db_oe;
inout wire	[7:0] D;
inout wire	[7:0] db;

reg	[7:0] dout;
wire	[7:0] SYNTHESIZED_WIRE_0;
wire	SYNTHESIZED_WIRE_1;
wire	SYNTHESIZED_WIRE_2;
wire	[7:0] SYNTHESIZED_WIRE_3;
wire	[7:0] SYNTHESIZED_WIRE_4;





always@(posedge SYNTHESIZED_WIRE_1)
begin
if (SYNTHESIZED_WIRE_2)
	begin
	dout[7:0] <= SYNTHESIZED_WIRE_0[7:0];
	end
end

assign	SYNTHESIZED_WIRE_4 = {ctl_bus_db_we,ctl_bus_db_we,ctl_bus_db_we,ctl_bus_db_we,ctl_bus_db_we,ctl_bus_db_we,ctl_bus_db_we,ctl_bus_db_we} & db;

assign	SYNTHESIZED_WIRE_3 = {bus_db_pin_re,bus_db_pin_re,bus_db_pin_re,bus_db_pin_re,bus_db_pin_re,bus_db_pin_re,bus_db_pin_re,bus_db_pin_re} & D;

assign	SYNTHESIZED_WIRE_0 = SYNTHESIZED_WIRE_3 | SYNTHESIZED_WIRE_4;

assign	SYNTHESIZED_WIRE_2 = ctl_bus_db_we | bus_db_pin_re;

assign	db[7] = ctl_bus_db_oe ? dout[7] : 1'bz;
assign	db[6] = ctl_bus_db_oe ? dout[6] : 1'bz;
assign	db[5] = ctl_bus_db_oe ? dout[5] : 1'bz;
assign	db[4] = ctl_bus_db_oe ? dout[4] : 1'bz;
assign	db[3] = ctl_bus_db_oe ? dout[3] : 1'bz;
assign	db[2] = ctl_bus_db_oe ? dout[2] : 1'bz;
assign	db[1] = ctl_bus_db_oe ? dout[1] : 1'bz;
assign	db[0] = ctl_bus_db_oe ? dout[0] : 1'bz;

assign	D[7] = bus_db_pin_oe ? dout[7] : 1'bz;
assign	D[6] = bus_db_pin_oe ? dout[6] : 1'bz;
assign	D[5] = bus_db_pin_oe ? dout[5] : 1'bz;
assign	D[4] = bus_db_pin_oe ? dout[4] : 1'bz;
assign	D[3] = bus_db_pin_oe ? dout[3] : 1'bz;
assign	D[2] = bus_db_pin_oe ? dout[2] : 1'bz;
assign	D[1] = bus_db_pin_oe ? dout[1] : 1'bz;
assign	D[0] = bus_db_pin_oe ? dout[0] : 1'bz;

assign	SYNTHESIZED_WIRE_1 =  ~clk;


endmodule
