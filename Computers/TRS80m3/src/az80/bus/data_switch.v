/*
  
   Multicore 2 / Multicore 2+
  
   Copyright (c) 2017-2020 - Victor Trucco

  
   All rights reserved
  
   Redistribution and use in source and synthezised forms, with or without
   modification, are permitted provided that the following conditions are met:
  
   Redistributions of source code must retain the above copyright notice,
   this list of conditions and the following disclaimer.
  
   Redistributions in synthesized form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
  
   Neither the name of the author nor the names of other contributors may
   be used to endorse or promote products derived from this software without
   specific prior written permission.
  
   THIS CODE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
   AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
   THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
   PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE
   LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
   CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
   INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
   CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
   ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
   POSSIBILITY OF SUCH DAMAGE.
  
   You are responsible for any legal issues arising from your use of this code.
  
*/// Copyright (C) 1991-2013 Altera Corporation
// Your use of Altera Corporation's design tools, logic functions 
// and other software and tools, and its AMPP partner logic 
// functions, and any output files from any of the foregoing 
// (including device programming or simulation files), and any 
// associated documentation or information are expressly subject 
// to the terms and conditions of the Altera Program License 
// Subscription Agreement, Altera MegaCore Function License 
// Agreement, or other applicable license agreement, including, 
// without limitation, that your use is for the sole purpose of 
// programming logic devices manufactured by Altera and sold by 
// Altera or its authorized distributors.  Please refer to the 
// applicable agreement for further details.

// PROGRAM		"Quartus II 64-Bit"
// VERSION		"Version 13.0.1 Build 232 06/12/2013 Service Pack 1 SJ Web Edition"
// CREATED		"Mon Oct 13 12:33:19 2014"

module data_switch(
	sw_up_en,
	sw_down_en,
	db_down,
	db_up
);


input wire	sw_up_en;
input wire	sw_down_en;
inout wire	[7:0] db_down;
inout wire	[7:0] db_up;





assign	db_up[7] = sw_up_en ? db_down[7] : 1'bz;
assign	db_up[6] = sw_up_en ? db_down[6] : 1'bz;
assign	db_up[5] = sw_up_en ? db_down[5] : 1'bz;
assign	db_up[4] = sw_up_en ? db_down[4] : 1'bz;
assign	db_up[3] = sw_up_en ? db_down[3] : 1'bz;
assign	db_up[2] = sw_up_en ? db_down[2] : 1'bz;
assign	db_up[1] = sw_up_en ? db_down[1] : 1'bz;
assign	db_up[0] = sw_up_en ? db_down[0] : 1'bz;

assign	db_down[7] = sw_down_en ? db_up[7] : 1'bz;
assign	db_down[6] = sw_down_en ? db_up[6] : 1'bz;
assign	db_down[5] = sw_down_en ? db_up[5] : 1'bz;
assign	db_down[4] = sw_down_en ? db_up[4] : 1'bz;
assign	db_down[3] = sw_down_en ? db_up[3] : 1'bz;
assign	db_down[2] = sw_down_en ? db_up[2] : 1'bz;
assign	db_down[1] = sw_down_en ? db_up[1] : 1'bz;
assign	db_down[0] = sw_down_en ? db_up[0] : 1'bz;


endmodule
