/*
  
   Multicore 2 / Multicore 2+
  
   Copyright (c) 2017-2020 - Victor Trucco

  
   All rights reserved
  
   Redistribution and use in source and synthezised forms, with or without
   modification, are permitted provided that the following conditions are met:
  
   Redistributions of source code must retain the above copyright notice,
   this list of conditions and the following disclaimer.
  
   Redistributions in synthesized form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
  
   Neither the name of the author nor the names of other contributors may
   be used to endorse or promote products derived from this software without
   specific prior written permission.
  
   THIS CODE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
   AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
   THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
   PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE
   LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
   CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
   INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
   CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
   ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
   POSSIBILITY OF SUCH DAMAGE.
  
   You are responsible for any legal issues arising from your use of this code.
  
*//**************************************************/
/* minimig_de0_nan0_top.v                   		  */
/* Altera DE0 Nano FPGA Top File           		  */
/*                                         		  */
/* 2012, rok.krajnc@gmail.com               		  */
/* 2015, Stefan Kristiansson                		  */
/**************************************************/
/*                                         		  */
/* SM-X mini Top                            		  */
/* 2020, Victor Trucco		                		  */
/**************************************************/

`default_nettype none

`define MINIMIG_VIDEO_FILTER


module SM_AGA_top (

	  // clock inputs
	  input                 CLOCK_50,     // 50 MHz
	  
	  // push button inputs
	  input       [    2:1] btn_n_i,          
	  input       [    8:1] dip_i,          

	  // PS2
	  inout                 PS2_DAT,      // PS2 Keyboard Data
	  inout                 PS2_CLK,      // PS2 Keyboard Clock
	  inout                 PS2_MDAT,     // PS2 Mouse Data
	  inout                 PS2_MCLK,     // PS2 Mouse Clock
	  
	  // VGA
	  output                VGA_HS,       // VGA H_SYNC
	  output                VGA_VS,       // VGA V_SYNC
	  output      [  4:0]   VGA_R,        // VGA Red[7:0], 
	  output      [  4:0]   VGA_G,        // VGA Green[7:0] 
	  output      [  4:0]   VGA_B,        // VGA Blue[7:0] 
	  
	  // SD Card
	  input                 SD_MISO,      // SD Card Data            - spi MISO
	  output                SD_CS,        // SD Card Data 3          - spi CS
	  output                SD_MOSI,      // SD Card Command Signal  - spi MOSI
	  output                SD_CLK,       // SD Card Clock           - spi CLK
	  input                 sd_sw_i,      // SD Card switch
	  
	  // SDRAM
	  inout       [ 15:0]   SDRAM_DQ,      // SDRAM Data bus 16 Bits
	  output      [ 12:0]   SDRAM_A,       // SDRAM Address bus 12 Bits
	  output                SDRAM_DQML,    // SDRAM Low-byte Data Mask
	  output                SDRAM_DQMH,    // SDRAM High-byte Data Mask
	  output                SDRAM_nWE,     // SDRAM Write Enable
	  output                SDRAM_nCAS,    // SDRAM Column Address Strobe
	  output                SDRAM_nRAS,    // SDRAM Row Address Strobe
	  output                SDRAM_nCS,     // SDRAM Chip Select
	  output      [  1:0]   SDRAM_BA,      // SDRAM Bank Address 0-1
	  output                SDRAM_CLK,     // SDRAM Clock
	  output                SDRAM_CKE,     // SDRAM Clock Enable

	  // SOUND
	  output                AUDIO_L,      // sigma-delta DAC output left
	  output                AUDIO_R,       // sigma-delta DAC output right
	  input wire	ear_i,
		output wire	mic_o				= 1'b0,
	
		// Joysticks
		input wire	joy1_up_io,
		input wire	joy1_down_io,
		input wire	joy1_left_io,
		input wire	joy1_right_io,
		input wire	joy1_p6_io,
		input wire	joy1_p7_io,
		output wire	joy1_p8_io,
		input wire	joy2_up_io,
		input wire	joy2_down_io,
		input wire	joy2_left_io,
		input wire	joy2_right_io,
		input wire	joy2_p6_io,
		input wire	joy2_p7_io,
		output wire	joy2_p8_io,

		// External Slots
		inout [15:0] slot_A_o	, 
		inout [7:0]slot_D_io		, 
		inout slot_CS1_o			, 
		inout slot_CS2_o			, 
		inout slot_CLOCK_o		, 	
		inout slot_M1_o			, 	
		inout slot_MREQ_o			, 	
		inout slot_IOREQ_o		, 	
		inout slot_RD_o			, 	
		inout slot_WR_o			, 	
		inout slot_RESET_io		, 	
		inout slot_SLOT1_o		, 	
		inout slot_SLOT2_o		, 		
		inout slot_SLOT3_o		, 
		inout slot_BUSDIR_i		, 
		inout slot_RFSH_i			, 
		inout slot_INT_i			, 
		inout slot_WAIT_i			, 
		 
		output slot_DATA_OE_o	, 	
		output slot_DATA_DIR_o	, 

		// HDMI video
		output hdmi_pclk			, 
		output hdmi_de				, 
		input hdmi_int				, 
		output hdmi_rst			,  
		 
		// HDMI audio 
		output aud_sck				, 
		output aud_ws				, 
		output aud_i2s				, 
		 
		// HDMI programming
		inout hdmi_sda				, 
		inout hdmi_scl				, 
		 
		// ESP
		output esp_rx_o			, 
		input esp_tx_i			 
);

// External Slots
assign slot_A_o			= 16'bzzzzzzzzzzzzzzzz; 
assign slot_D_io			= 8'bzzzzzzzz;
assign slot_CS1_o			= 1'bz;
assign slot_CS2_o			= 1'bz;
assign slot_CLOCK_o		= 1'bz;	
assign slot_M1_o			= 1'bz;	
assign slot_MREQ_o		= 1'bz;	
assign slot_IOREQ_o		= 1'bz;	
assign slot_RD_o			= 1'bz;	
assign slot_WR_o			= 1'bz;	
assign slot_RESET_io		= 1'bz;	
assign slot_SLOT1_o		= 1'bz;	
assign slot_SLOT2_o		= 1'bz;		
assign slot_SLOT3_o		= 1'bz;
assign slot_BUSDIR_i		= 1'bz;
assign slot_RFSH_i		= 1'bz; 
assign slot_INT_i			= 1'bz;
assign slot_WAIT_i		= 1'bz; 
assign slot_DATA_OE_o	= 1'bz;	
assign slot_DATA_DIR_o	= 1'bz;

// HDMI video
assign hdmi_pclk			= 1'bz;
assign  hdmi_de			= 1'bz; 
assign hdmi_rst			= 1'bz; 

// HDMI audio
assign aud_sck				= 1'bz;
assign aud_ws				= 1'bz;
assign aud_i2s				= 1'bz;

// HDMI programming
assign hdmi_sda			= 1'bz;
assign hdmi_scl			= 1'bz;

// ESP
assign esp_rx_o			= 1'bz;

////////////////////////////////////////
// internal signals                   //
////////////////////////////////////////

// clock
wire           pll_in_clk;
wire           clk_114;
wire           clk_28;
wire           clk_sdram;
wire           pll_locked;
wire           clk_7;
wire           clk7_en;
wire           c1;
wire           c3;
wire           cck;
wire [ 10-1:0] eclk;
wire           clk_50;
wire           clk_50_pll;

// reset
wire           pll_rst;
wire           sdctl_rst;
wire           rst_50;
wire           rst_minimig;
wire           rst_cpu;

// ctrl
wire           rom_status;
wire           ram_status;
wire           reg_status;
wire           dram_status;
wire           ctrl_txd;
wire           ctrl_rxd;
wire [ 23-1:0] dram_adr;
wire           dram_cs;
wire           dram_we;
wire [  4-1:0] dram_sel;
wire [ 32-1:0] dram_dat_w;
wire [ 32-1:0] dram_dat_r;
wire           dram_ack;
wire           dram_err;

// bridge
wire [ 23-1:0] bridge_adr;
wire           bridge_cs;
wire           bridge_we;
wire [  4-1:0] bridge_sel;
wire [ 32-1:0] bridge_dat_w;
wire [ 32-1:0] bridge_dat_r;
wire           bridge_ack;
wire           bridge_err;

// tg68
wire           tg68_rst;
wire [ 16-1:0] tg68_dat_in;
wire [ 16-1:0] tg68_dat_out;
wire [ 32-1:0] tg68_adr;
wire [  3-1:0] tg68_IPL;
wire           tg68_dtack;
wire           tg68_as;
wire           tg68_uds;
wire           tg68_lds;
wire           tg68_rw;
wire           tg68_ena7RD;
wire           tg68_ena7WR;
wire           tg68_enaWR;
wire [ 16-1:0] tg68_cout;
wire           tg68_cpuena;
wire [  4-1:0] cpu_config;
wire [  6-1:0] memcfg;
wire           vsync;
wire [ 32-1:0] tg68_cad;
wire [  6-1:0] tg68_cpustate;
wire           tg68_cdma;
wire           tg68_clds;
wire           tg68_cuds;
wire [ 32-1:0] tg68_VBR_out;

// minimig
wire           minimig_rst_out;
wire [ 16-1:0] ram_data;      // sram data bus
wire [ 16-1:0] ramdata_in;    // sram data bus in
wire [ 22-1:1] ram_address;   // sram address bus
wire           _ram_bhe;      // sram upper byte select
wire           _ram_ble;      // sram lower byte select
wire           _ram_we;       // sram write enable
wire           _ram_oe;       // sram output enable
wire           _15khz;        // scandoubler disable
wire           SDO;           // SPI data output

// RS232
wire           minimig_txd;
wire           minimig_rxd;

// host bus
wire           host_cs;
wire [ 24-1:0] host_adr;
wire           host_we;
wire [  2-1:0] host_bs;
wire [ 16-1:0] host_wdat;
wire [ 16-1:0] host_rdat;
wire           host_ack;

// sdram
wire           reset_out;
wire [  4-1:0] sdram_cs;
wire [  2-1:0] sdram_dqm;


// ctrl
wire [  4-1:0] SPI_CS_N;
wire           SPI_DI;
wire           rst_ext;
wire [  4-1:0] ctrl_cfg;
wire [  4-1:0] ctrl_status;
wire [  4-1:0] sys_status;
wire [  4-1:0] mem_status;

// uart
wire           uart_sel;





// //////////////////
// jepalza: version MIST AGA
wire           clk7n_en;

wire           turbochipram;
wire           turbokick;
wire           cache_inhibit;
wire [  4-1:0] tg68_CACR_out;
wire           tg68_ovr;
wire           tg68_nrst_out;

wire [ 48-1:0] chip48;        // memoria 48 bits para video AGA


wire           floppy_fwr;
wire           floppy_frd; // al led naranja
wire           hd_fwr;
wire           hd_frd; // al led azul

wire [7:0] VGA_R8;
wire [7:0] VGA_G8;
wire [7:0] VGA_B8;

////////////////////////////////////////
// toplevel assignments               //
////////////////////////////////////////

// UART
// uart_sel = 0 => minimig, uart_sel = 1 => or1k
assign uart_sel         = 1'b1; // 0=modo A500, 1=modo rs232 salida de datos minimig
//assign stm_rx_o         = uart_sel ? ctrl_txd : minimig_txd;
assign ctrl_rxd         = uart_sel ? 1'b1 : 1'b1;
assign minimig_rxd      = uart_sel ? 1'b1     : 1'b1;

// SD card
assign SD_CS             = SPI_CS_N[0];

// SDRAM
assign SDRAM_CKE         = 1'b1;
assign SDRAM_CLK         = clk_sdram;
assign SDRAM_nCS         = sdram_cs[0];
assign SDRAM_DQML        = sdram_dqm[0];
assign SDRAM_DQMH        = sdram_dqm[1];

// ctrl
assign SPI_DI           = !SPI_CS_N[0] ? SD_MISO : SDO;
assign ctrl_cfg         = {1'b0, 1'b0, 1'b0, 1'b0};

// clock
assign pll_in_clk       = CLOCK_50;

// reset
assign rst_ext          = !btn_n_i[1]; // boton reset externo
assign pll_rst          = rst_ext;
assign sdctl_rst        = pll_locked & !rst_ext; 

// minimig (VGA scandoubler)
assign _15khz           = 1'b1; 



////////////////////////////////////////
// modules                            //
////////////////////////////////////////

// estado del sistema, para enviar, tanto al MIST como a los indivadores
assign sys_status = {vsync, ~tg68_rst, minimig_rst_out, ~reset_out};
assign mem_status = {rom_status, ram_status, reg_status, dram_status};

// indicadores led, solo tenemos tres
indicators indicadores(
  .clk          (clk_7       ),
  .rst          (~pll_locked ),
  .track        (		        ), // variable 7:0 de nÂº de pista, sin uso
  .f_wr         (floppy_fwr  ), // escritura FD
  .f_rd         (floppy_frd  ), // lectura FD
  .h_wr         (hd_fwr      ), // escritura HD
  .h_rd         (hd_frd      ), // lectura HD
  .status       (mem_status  ), // sin uso, pero lo dejo para depuracion
  .ctrl_status  (ctrl_status ), // estado del modulo de arranque inicial CTRL_TOP
  .sys_status   (sys_status  ), // estado del sistema
  .fifo_full    (		  		  ), // estado del "buffer" fifo, si uso
  .led          (            )  // salida a LEDS
);



//// control block ////
ctrl_top ctrl_top (

  // system
  .clk_in       (clk_50_pll       ),  // input 50MHz clock
  .rst_ext      (rst_ext          ),  // external reset input
  .clk_out      (clk_50           ),  // output 50MHz clock from internal PLL
  .rst_out      (rst_50           ),  // reset output from internal reset generator
  .rst_minimig  (rst_minimig      ),  // minimig reset output
  .rst_cpu      (rst_cpu          ),  // TG68K reset output
  
  // config
  .boot_sel     (1'b0             ),  // select FLASH boot location
  .ctrl_cfg     (ctrl_cfg         ),  // config for ctrl module
  
  // status
  .rom_status   (rom_status       ),  // ROM slave activity
  .reg_status   (reg_status       ),  // REG slave activity
  .dram_status  (dram_status      ),  // DRAM slave activity
  .ctrl_status  (ctrl_status      ),  // salida de estado de CTRL para llevar a los LED
  .sys_status   (sys_status		 ),  // SYS status input
  
  // DRAM interface
  .dram_adr     (dram_adr         ),
  .dram_cs      (dram_cs          ),
  .dram_we      (dram_we          ),
  .dram_sel     (dram_sel         ),
  .dram_dat_w   (dram_dat_w       ),
  .dram_dat_r   (dram_dat_r       ),
  .dram_ack     (dram_ack         ),
  .dram_err     (dram_err         ),
  
  // UART
  .uart_txd     (ctrl_txd         ),  // UART transmit output
  .uart_rxd     (ctrl_rxd         ),  // UART receive input
  
  // SPI
  .spi_cs_n     (SPI_CS_N         ),  // SPI chip select output
  .spi_clk      (SD_CLK           ),  // SPI clock
  .spi_do       (SD_MOSI          ),  // SPI data input
  .spi_di       (SPI_DI           )   // SPI data output
);


//// qmem async 32-to-32 bridge ////
qmem_bridge #(
  .MAW (23), // 23
  .MSW (4 ), // 4
  .MDW (32), // 32
  .SAW (23), // 23
  .SSW (4 ), // 4
  .SDW (32)  // 32
) qmem_bridge (
  // master
  .m_clk        (clk_50           ),
  .m_adr        (dram_adr         ),
  .m_cs         (dram_cs          ),
  .m_we         (dram_we          ),
  .m_sel        (dram_sel         ),
  .m_dat_w      (dram_dat_w       ),
  .m_dat_r      (dram_dat_r       ),
  .m_ack        (dram_ack         ),
  .m_err        (dram_err         ),
  // slave
  .s_clk        (clk_114          ),
  .s_adr        (bridge_adr       ),
  .s_cs         (bridge_cs        ),
  .s_we         (bridge_we        ),
  .s_sel        (bridge_sel       ),
  .s_dat_w      (bridge_dat_w     ),
  .s_dat_r      (bridge_dat_r     ),
  .s_ack        (bridge_ack       ),
  .s_err        (1'b0) // bridge_err      
);


//// amiga clocks ////
amiga_clk amiga_clk (
  .rst          (pll_rst          ), // async reset input
  .clk_in       (pll_in_clk       ), // input clock     (50MHZ EN MI PLACA)
  .clk_114      (clk_114          ), // output clock c0 (114.750000MHz)
  .clk_sdram    (clk_sdram        ), // output clock c2 (114.750000MHz, -146.25 deg)
  .clk_28       (clk_28           ), // output clock c1 ( 28.687500MHz)
  .clk_50       (clk_50_pll       ), // output clock 50Mhz
  .clk_7        (clk_7            ), // output clock 7  (  7.171875MHz)
  .clk7_en      (clk7_en          ), // output clock 7 enable (on 28MHz clock domain)
  .clk7n_en     (clk7n_en         ), // 7MHz negedge output clock enable (on 28MHz clock domain)
  .c1           (c1               ), // clk28m clock domain signal synchronous with clk signal
  .c3           (c3               ), // clk28m clock domain signal synchronous with clk signal delayed by 90 degrees
  .cck          (cck              ), // colour clock output (3.54 MHz)
  .eclk         (eclk             ), // 0.709379 MHz clock enable output (clk domain pulse)
  .locked       (pll_locked       )  // pll locked output
);

// jepalza, 68020? el del MIST, mejorado
TG68K tg68k (
  .clk          (clk_114          ),
  .reset        (tg68_rst         ),
  .clkena_in    (1'b1             ),
  .IPL          (tg68_IPL         ),
  .dtack        (tg68_dtack       ),
  .vpa          (1'b1             ),
  .ein          (1'b1             ),
  .addr         (tg68_adr         ),
  .data_read    (tg68_dat_in      ),
  .data_write   (tg68_dat_out     ),
  .as           (tg68_as          ),
  .uds          (tg68_uds         ),
  .lds          (tg68_lds         ),
  .rw           (tg68_rw          ),
  .e            (                 ),
  .vma          (                 ),
  .wrd          (                 ),
  .ena7RDreg    (tg68_ena7RD      ),
  .ena7WRreg    (tg68_ena7WR      ),
  .enaWRreg     (tg68_enaWR       ),
  .fromram      (tg68_cout        ),
  .ramready     (tg68_cpuena      ),
  .cpu          (cpu_config[1:0]  ),
	//
  .ramaddr      (tg68_cad         ),
  .cpustate     (tg68_cpustate    ),
  .nResetOut    (tg68_nrst_out    ),
  .skipFetch    (                 ),
  .cpuDMA       (tg68_cdma        ),
  .ramlds       (tg68_clds        ),
  .ramuds       (tg68_cuds        ),
  .VBR_out      (tg68_VBR_out     ),
  // nuevos en version 68020
  .turbochipram (turbochipram     ), // in
  .turbokick    (turbokick        ), // in
  .cache_inhibit(cache_inhibit    ), // out, hacia SDRAM
  .fastramcfg   ({&memcfg[5:4],memcfg[5:4]}), // IN , seleciona la FASTRAM:2 o 4mb estandar o 24 
  .CACR_out     (tg68_CACR_out    ), // out (buffer)
  .ovr          (tg68_ovr         ), // out
  // red, no implementada
  .eth_en       (1'b1),
  .sel_eth      (),
  .frometh      (16'd0),
  .ethready     (1'b0)
);

//// sdram desde minimig DE0-Nano, ampliada por jepalza para MIST
sdram_ctrl sdram (
  // sys
  .sysclk       (clk_114          ),
  .c_7m         (clk_7            ),
  .reset_in     (sdctl_rst        ),
  .cache_rst    (tg68_rst         ),
  .reset_out    (reset_out        ),
  .cache_ena    ( 1  ), // siempre fijo?
  
  // sdram
  .sdaddr       (SDRAM_A          ),
  .sd_cs        (sdram_cs         ),
  .ba           (SDRAM_BA         ),
  .sd_we        (SDRAM_nWE        ),
  .sd_ras       (SDRAM_nRAS       ),
  .sd_cas       (SDRAM_nCAS       ),
  .dqm          (sdram_dqm        ),
  .sdata        (SDRAM_DQ         ),
  
  // host
  .host_cs      (bridge_cs        ),
  .host_adr     ({bridge_adr[22], 2'b11, bridge_adr[21:0]}), // jepalza, antes 2'b00, ahora 11, para cargar en lo mas alto
  .host_we      (bridge_we        ),
  .host_bs      (bridge_sel       ),
  .host_wdat    (bridge_dat_w     ),
  .host_rdat    (bridge_dat_r     ),
  .host_ack     (bridge_ack       ),
  
  // chip
  .chipAddr     ({2'b00, ram_address[21:1]}),
  .chipL        (_ram_ble         ),
  .chipU        (_ram_bhe         ),
  .chipRW       (_ram_we          ),
  .chip_dma     (_ram_oe          ),
  .chipWR       (ram_data         ),
  .chipRD       (ramdata_in       ),
  .chip48       (chip48           ), // out, jepalza, para dar mas RAM al modo AGA
  
  // cpu
  .cpuAddr      (tg68_cad[24:1]   ),
  .cpustate     (tg68_cpustate    ),
  .cpuL         (tg68_clds        ),
  .cpuU         (tg68_cuds        ),
  .cpu_dma      (tg68_cdma        ),
  .cpuWR        (tg68_dat_out     ),
  .cpuRD        (tg68_cout        ),
  .enaWRreg     (tg68_enaWR       ),
  .ena7RDreg    (tg68_ena7RD      ),
  .ena7WRreg    (tg68_ena7WR      ),
  .cpuena       (tg68_cpuena      )
);



// jepalza, nuevo minimig, con soporte AGA, proviene del MIST, con partes adaptadas por mi
minimig minimig (
  //m68k pins
  .cpu_address  (tg68_adr[23:1]   ), // M68K address bus
  .cpu_data     (tg68_dat_in      ), // M68K data bus
  .cpudata_in   (tg68_dat_out     ), // M68K data in
  ._cpu_ipl     (tg68_IPL         ), // M68K interrupt request
  ._cpu_as      (tg68_as          ), // M68K address strobe
  ._cpu_uds     (tg68_uds         ), // M68K upper data strobe
  ._cpu_lds     (tg68_lds         ), // M68K lower data strobe
  .cpu_r_w      (tg68_rw          ), // M68K read / write
  ._cpu_dtack   (tg68_dtack       ), // M68K data acknowledge
  ._cpu_reset   (tg68_rst         ), // OUT M68K reset
  ._cpu_reset_in(tg68_nrst_out    ), // IN  M68K reset out
  .cpu_vbr      (tg68_VBR_out     ), // M68K VBR
  .ovr          (tg68_ovr         ), // NMI override address decoding
  //sram pins
  .ram_data     (ram_data         ), // SRAM data bus
  .ramdata_in   (ramdata_in       ), // SRAM data bus in
  .ram_address  (ram_address[21:1]), // SRAM address bus
  ._ram_bhe     (_ram_bhe         ), // SRAM upper byte select
  ._ram_ble     (_ram_ble         ), // SRAM lower byte select
  ._ram_we      (_ram_we          ), // SRAM write enable
  ._ram_oe      (_ram_oe          ), // SRAM output enable
  .chip48       (chip48           ), // input big chipram read
  //system  pins
  .rst_ext      (rst_minimig      ), // in reset from ctrl block
  .rst_out      (minimig_rst_out  ), // ''out'' (aÃ±adido por JEPALZA) minimig reset status
  .clk          (clk_28           ), // in desde clk principal clock c1 ( 28.687500MHz)
  .clk7_en      (clk7_en          ), // in 7MHz clock enable
  .clk7n_en     (clk7n_en         ), // in 7MHz negedge clock enable
  .c1           (c1               ), // in clk28m clock domain signal synchronous with clk signal
  .c3           (c3               ), // in clk28m clock domain signal synchronous with clk signal delayed by 90 degrees
  .cck          (cck              ), // in colour clock output (3.54 MHz)
  .eclk         (eclk             ), // in 0.709379 MHz clock enable output (clk domain pulse)
  //rs232 pins
  .rxd          (minimig_rxd),//UART_RX          ),  // RS232 receive, jepalza, para poder desviar TX-RX
  .txd          (minimig_txd),//UART_TX          ),  // RS232 send
  .cts          (1'b0             ),  // RS232 clear to send
  .rts          (                 ),  // RS232 request to send
  //I/O
  ._joy1        ({2'b00,joy1_p7_io, joy1_p6_io,joy1_up_io,joy1_down_io,joy1_left_io,joy1_right_io}),  // joystick 1 [fire4,fire3,fire2,fire,up,down,left,right] (default mouse port)
  ._joy2        ({2'b00,joy2_p7_io, joy2_p6_io,joy2_up_io,joy2_down_io,joy2_left_io,joy2_right_io}),  // joystick 2 [fire4,fire3,fire2,fire,up,down,left,right] (default joystick port)
  .mouse_btn1   (1'b1             ), // mouse button 1
  .mouse_btn2   (1'b1             ), // mouse button 2
  // todo esto solo es para el SPI del MIST, lo anulo
  .mouse_btn         (), // todo anulado
  .kbd_mouse_data    (),
  .kbd_mouse_type    (),
  .kbd_mouse_strobe  (),
  .kms_level    	 (),
  //
  ._15khz       (_15khz           ),  // scandoubler disable
  .pwrled       (),//led              ),  // power led
  .msdat        (PS2_MDAT         ),  // PS2 mouse data
  .msclk        (PS2_MCLK         ),  // PS2 mouse clk
  .kbddat       (PS2_DAT          ),  // PS2 keyboard data
  .kbdclk       (PS2_CLK          ),  // PS2 keyboard clk
  //host controller interface (SPI), jepalza, las cinco lineas MIST cambiadas
  ._scs         (SPI_CS_N[3:1]    ),//{SPI_SS4,SPI_SS3,SPI_SS2}  ),  // SPI chip select
  .direct_sdi   (SD_MISO          ),//SPI_DO           ),  // SD Card direct in  SPI_SDO
  .sdi          (SD_MOSI          ),//SPI_DI           ),  // SPI data input
  .sdo          (SDO              ),//minimig_sdo      ),  // SPI data output
  .sck          (SD_CLK           ),//SPI_SCK          ),  // SPI clock
  //video
  ._hsync       (VGA_HS),   // horizontal sync
  ._vsync       (VGA_VS),   // vertical sync
  .red          (VGA_R8 ),  // red
  .green        (VGA_G8 ),  // green
  .blue         (VGA_B8 ),  // blue
  //audio
  .left         (AUDIO_L          ),  // audio bitstream left
  .right        (AUDIO_R          ),  // audio bitstream right
  .ldata        (                 ),  // left DAC data
  .rdata        (                 ),  // right DAC data
  //user i/o
  .cpu_config   (cpu_config       ), // out CPU config
  .memcfg       (memcfg           ), // out memory config
  .turbochipram (turbochipram     ), // out turbo chipRAM
  .turbokick    (turbokick        ), // out turbo kickstart
  .init_b       (vsync            ), // out (jepalza) vertical sync for MCU (sync OSD update), necesario para nuestro OSD
  // salidas de estados del MIST, para los led de nuestra placa
  .fifo_full    (                 ), 
  .trackdisp    (		             ), // floppy track number, variable 7:0 para indicar nÂº de pista, pero sin uso
  .secdisp      (                 ), // sector
  .floppy_fwr   (floppy_fwr       ), // floppy fifo writing
  .floppy_frd   (floppy_frd       ), // floppy fifo reading
  .hd_fwr       (hd_fwr           ), // hd fifo writing
  .hd_frd       (hd_frd           )  // hd fifo  ading
);

assign joy1_p8_io = 1'b0;
assign joy2_p8_io = 1'b0;

assign VGA_R = VGA_R8[7:3] ; // me quedo con los 6 bits altos
assign VGA_G = VGA_G8[7:3] ; // para llegar a los de 
assign VGA_B = VGA_B8[7:3] ; // nuestra placa

endmodule
