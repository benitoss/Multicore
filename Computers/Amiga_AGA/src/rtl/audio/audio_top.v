/*
  
   Multicore 2 / Multicore 2+
  
   Copyright (c) 2017-2020 - Victor Trucco

  
   All rights reserved
  
   Redistribution and use in source and synthezised forms, with or without
   modification, are permitted provided that the following conditions are met:
  
   Redistributions of source code must retain the above copyright notice,
   this list of conditions and the following disclaimer.
  
   Redistributions in synthesized form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
  
   Neither the name of the author nor the names of other contributors may
   be used to endorse or promote products derived from this software without
   specific prior written permission.
  
   THIS CODE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
   AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
   THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
   PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE
   LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
   CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
   INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
   CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
   ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
   POSSIBILITY OF SUCH DAMAGE.
  
   You are responsible for any legal issues arising from your use of this code.
  
*//********************************************/
/* audio_top.v                              */
/*                                          */
/* 2012, rok.krajnc@gmail.com               */
/********************************************/


module audio_top (
  input  wire           clk,
  input  wire           rst_n,
  // config
  input  wire           mix,
  // audio shifter
  input  wire [ 15-1:0] rdata,
  input  wire [ 15-1:0] ldata,
  input  wire           exchan,
  output wire           aud_bclk,
  output wire           aud_daclrck,
  output wire           aud_dacdat,
  output wire           aud_xck,
  // I2C audio config
  output wire           i2c_sclk,
  inout                 i2c_sdat
);



////////////////////////////////////////
// modules                            //
////////////////////////////////////////

// don't include these two modules for sim, as they have some probems in simulation
`ifndef SOC_SIM


// audio shifter
audio_shifter audio_shifter (
  .clk          (clk              ),
  .nreset       (rst_n            ),
  .mix          (mix              ),
  .rdata        (rdata            ),
  .ldata        (ldata            ),
  .exchan       (exchan           ),
  .aud_bclk     (aud_bclk         ),
  .aud_daclrck  (aud_daclrck      ),
  .aud_dacdat   (aud_dacdat       ),
  .aud_xck      (aud_xck          )
);


// I2C audio config
//I2C_AV_Config audio_config (
//  // host side
//  .iCLK         (clk              ),
//  .iRST_N       (rst_n            ),
//  // i2c side
//  .oI2C_SCLK    (i2c_sclk         ),
//  .oI2C_SDAT    (i2c_sdat         )
//);


`endif // SOC_SIM



endmodule

