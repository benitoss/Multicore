/*
  
   Multicore 2 / Multicore 2+
  
   Copyright (c) 2017-2020 - Victor Trucco

  
   All rights reserved
  
   Redistribution and use in source and synthezised forms, with or without
   modification, are permitted provided that the following conditions are met:
  
   Redistributions of source code must retain the above copyright notice,
   this list of conditions and the following disclaimer.
  
   Redistributions in synthesized form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
  
   Neither the name of the author nor the names of other contributors may
   be used to endorse or promote products derived from this software without
   specific prior written permission.
  
   THIS CODE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
   AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
   THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
   PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE
   LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
   CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
   INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
   CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
   ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
   POSSIBILITY OF SUCH DAMAGE.
  
   You are responsible for any legal issues arising from your use of this code.
  
*//* ****************************************************************************
 This Source Code Form is subject to the terms of the
 Open Hardware Description License, v. 1.0. If a copy
 of the OHDL was not distributed with this file, You
 can obtain one at http://juliusbaxter.net/ohdl/ohdl.txt

 Description: mor1kx tick timer unit

 Copyright (C) 2012 Authors

 Author(s): Julius Baxter <juliusbaxter@gmail.com>

***************************************************************************** */

`include "mor1kx-defines.v"

module mor1kx_ticktimer
  (
   input 	 clk,
   input 	 rst,

   output [31:0] spr_ttmr_o,
   output [31:0] spr_ttcr_o,

   // SPR Bus interface
   input         spr_access_i,
   input         spr_we_i,
   input [15:0]  spr_addr_i,
   input [31:0]  spr_dat_i,
   output        spr_bus_ack,
   output [31:0] spr_dat_o
   );

   // Registers
   reg [31:0]    spr_ttmr;
   reg [31:0]    spr_ttcr;

   wire spr_ttmr_access;
   wire spr_ttcr_access;

   // ttcr control wires
   wire          ttcr_clear;
   wire          ttcr_run;
   wire          ttcr_match;

   assign spr_ttmr_o = spr_ttmr;
   assign spr_ttcr_o = spr_ttcr;

   assign spr_ttmr_access =
     spr_access_i &
     (`SPR_OFFSET(spr_addr_i) == `SPR_OFFSET(`OR1K_SPR_TTMR_ADDR));
   assign spr_ttcr_access =
     spr_access_i &
     (`SPR_OFFSET(spr_addr_i) == `SPR_OFFSET(`OR1K_SPR_TTCR_ADDR));

   assign spr_bus_ack = spr_access_i;
   assign spr_dat_o = (spr_access_i & spr_ttcr_access) ? spr_ttcr :
                      (spr_access_i & spr_ttmr_access) ? spr_ttmr : 0;

   assign ttcr_match = spr_ttcr[27:0] == spr_ttmr[27:0];

   // Timer SPR control
   always @(posedge clk `OR_ASYNC_RST)
     if (rst)
       spr_ttmr <= 0;
     else if (spr_we_i & spr_ttmr_access)
       spr_ttmr <= spr_dat_i[31:0];
     else if (ttcr_match & spr_ttmr[29])
       spr_ttmr[28] <= 1; // Generate interrupt

   // Modes (spr_ttmr[31:30]):
   // 00 Tick timer is disabled.
   // 01 Timer is restarted on ttcr_match.
   // 10 Timer stops when ttcr_match is true.
   // 11 Timer does not stop when ttcr_match is true
   assign ttcr_clear = (spr_ttmr[31:30] == 2'b01) & ttcr_match;
   assign ttcr_run = (spr_ttmr[31:30] != 2'b00) & !ttcr_match |
		     (spr_ttmr[31:30] == 2'b11);

   always @(posedge clk `OR_ASYNC_RST)
     if (rst)
       spr_ttcr <= 0;
     else if (spr_we_i & spr_ttcr_access)
       spr_ttcr <= spr_dat_i[31:0];
     else if (ttcr_clear)
       spr_ttcr <= 0;
     else if (ttcr_run)
       spr_ttcr <= spr_ttcr + 1;

endmodule // mor1kx_ticktimer
