SofaFTP 1.0 by Louthrax

SofaFTP is an MSX-DOS 2 FTP client
for GR8NET and UNAPI interfaces

FTP server has to support STAT
command and passive mode. It must
also list directories in UNIX mode

Usage: SFTP <server> [commands] [/U]

<server> is an FTP server

[commands] is an optional list of
FTP commands separated by "!",
with spaces replaced by "*"

[/U] forces use of UNAPI interface

SofaFTP specific commands:

CD [remote_dir]
  Go to [remote_dir] on
  server, or display current
  directory if not specified

DIR [remote_dir]
  List content of [remote_dir] on
  server, or current directory if
  not specified

GET <remote_file> [local_file]
  Get <remote_file> from server to
  current directory on local
  machine, or to [local_file] if
  specified

GETU <remote_file> [local_file]
  Same as GET but only if
  [local_file] is older than
  <remote_file>

GETR [remote_dir] [local_dir]
  Recursively get directory from
  server to directory on local
  machine. If not specified, current
  directories are used

GETRU [remote_dir] [local_dir]
  Same as GETR but only if local
  files are older than remote
  files

HELP
  Display this help, plus remote
  help

LCD [local_dir]
  Go to [local_dir] on local
  machine, or display current
  directory if not specified

LDIR [local_dir]
  List content of [local_dir] on
  local machine, or current
  directory if not specified

PUT <local_file> [remote_file]
  Put <local_file> from local
  machine to current directory on
  server, or to [remote_file] if
  specified

PUTU <local_file> [remote_file]
  Same as PUT but only if
  [remote_file] is older than
  <local_file>

PUTR [local_dir] [remote_dir]
  Recursively put directory from
  local machine to directory
  on server. It not specified,
  current directories are used

PUTRU [local_dir] [remote_dir]
  Same as PUTR but only if remote
  files are older than local files

QUIT
  Disconnect from server and quit

TA <hours>[:<minutes>]
  Adjust times obtained from server,
  or display current value if not
  specified

VERBOSE [0|1]
  Set verbose mode to ON (1) or OFF
  (0), or display current mode if
  not specified
