#!/bin/bash

find_core () {
  CORE_NAME=$( find ./core/ -iname "*.$1" -type f -printf "%f\n" )
  if [[ ! -z $CORE_NAME ]]; then
    INI_NAME=${CORE_NAME::-4}.INI
    echo "DIS_SD" > ./core/$INI_NAME
  fi
}

if [ "$EUID" -ne 0 ]
  then echo "Please run as root"
  exit 1
fi

DEVICE=$1

usage () {
  echo "$0 {device}"
  echo "Ex.:"
  echo "$0 /dev/sdX"
  exit 2
}

if [ -z "$DEVICE" ]; then
  echo "Missing device name."
  usage
fi

[ -z "$( which sfdisk )" ] && echo "sfdisk not found!" && exit 1
[ -z "$( which mkfs.msdos )" ] && echo "mkfs.msdos not found!" && exit 1
[ -z "$( which mcopy )" ] && echo "mcopy not found!" && exit 1
[ -z "$( which mattrib )" ] && echo "truncate not found!" && exit 1

echo "Multicore SD Card creator (2022.05.04)"
echo "======================================"
echo

if [[ "$DEVICE" =~ .+[0-9]$ ]]; then
   DEVICE=${DEVICE::-1}
fi

sfdisk -l $DEVICE | head -n 1
[ ${?} -ne 0 ] && echo "Device not ready." && exit 9

echo
echo "#########################"
echo "######## WARNING ########"
echo "#########################"
echo
echo "This operation WILL DESTROY ALL DATA on '$DEVICE'"
echo
df -h | grep "$DEVICE"
echo
while true; do
    read -p "Do you wish to continue? [Yes/No] " yn
    case $yn in
        [Yy]* ) break;;
        [Nn]* ) echo "No changes made."; exit 0;;
        * ) echo "Please answer yes or no.";;
    esac
done

echo
while true; do
    read -p "Is it for OCM/SMX? [Y/n] " yn
    case $yn in
        [Nn]* ) SMXHB=0; break;;
        [Yy]*|"" ) SMXHB=1; break;;
        * ) echo "Please answer Yes or No.";;
    esac
done

if [[ "$SMXHB" == "0" ]]; then
  find_core "mcp"
  find_core "mc2"
  find_core "ua2"
  find_core "np1"

  if [[ -z "$INI_NAME" ]]; then
    echo
    echo "Core file (*.mcp *.mc2 *.ua2 *.np1) not found at './core' directory."
    echo "Please add a Core file before continue."
    exit 1
  fi
fi

echo
while true; do
    read -p "Include EXTRAS? [Y/n] " yn
    case $yn in
        [Nn]* ) EXTRAS=0; break;;
        [Yy]*|"" ) EXTRAS=1; break;;
        * ) echo "Please answer Yes or No.";;
    esac
done

echo
while true; do
    read -p "Include NETWORK? [Y/n] " yn
    case $yn in
        [Nn]* ) NETWORK=0; break;;
        [Yy]*|"" ) NETWORK=1; break;;
        * ) echo "Please answer Yes or No.";;
    esac
done

#MAXPARTSIZE=2147483648
MAXPARTSIZE=4293918720

umount $DEVICE?
sleep 1

DEVICESIZE=$( sfdisk -l $DEVICE | head -n 1 | cut -d' ' -f5 )

SIZEB=$DEVICESIZE
[ "$DEVICESIZE" -gt "$MAXPARTSIZE" ] && SIZEB=$MAXPARTSIZE

echo
echo "# Creating partition"
echo

NSECTORS=$( expr $SIZEB / 512 - 1 )
echo 1 $NSECTORS 6 | sfdisk -q --force $DEVICE
sync
sleep 1

echo
echo "## Formatting device"
echo

# CYLINDERS=$( expr $NSECTORS / 16065 )
# mformat -i $DEVICE@@512 -v FAT16MSX -t $CYLINDERS -h 255 -n 63 -H 1 -m 248 ::
mkfs.msdos -F16 -R1 -a -nFAT16MSX "$DEVICE"1
sync
sleep 1

if [[ "$SMXHB" == "0" ]]; then
  echo "### Copying BIOS file"
  echo
  mcopy -i $DEVICE@@512 sdbios/OCM-BIOS.DAT ::
  mattrib -i $DEVICE@@512 +h ::OCM-BIOS.DAT
fi

echo "#### Copying system files"
echo
mcopy -i $DEVICE@@512 os/MSXDOS2.SYS ::
mcopy -i $DEVICE@@512 os/NEXTOR.SYS ::
mcopy -i $DEVICE@@512 os/COMMAND2.COM ::

mattrib -i $DEVICE@@512 +s ::MSXDOS2.SYS
mattrib -i $DEVICE@@512 +s ::NEXTOR.SYS
mattrib -i $DEVICE@@512 +s ::COMMAND2.COM

mcopy -i $DEVICE@@512 system/* ::
[[ "$EXTRAS" == "1" ]] && echo "**** Copying EXTRAS" && mcopy -os -i $DEVICE@@512 extras/* ::
[[ "$NETWORK" == "1" ]] && echo "**** Copying NETWORK" && mcopy -os -i $DEVICE@@512 network/* ::

echo "**** Copying OTHER" && mcopy -os -i $DEVICE@@512 other/* ::
mdel -i $DEVICE@@512 ::README

echo

mcopy -os -i $DEVICE@@512 qhelp/QHELP.BAT ::QHELP.BAT
[[ "$EXTRAS" == "1" ]] && mcopy -os -i $DEVICE@@512 qhelp/QHELP-extras.BAT ::QHELP.BAT
[[ "$NETWORK" == "1" ]] && mcopy -os -i $DEVICE@@512 qhelp/QHELP-network.BAT ::QHELP.BAT
[[ "$EXTRAS" == "1" ]] && [[ "$NETWORK" == "1" ]] && mcopy -os -i $DEVICE@@512 qhelp/QHELP-extras-network.BAT ::QHELP.BAT

for f in $( find ./core/ -name "*.*" -type f -printf "%f\n" ); do
  echo "##### Copying ./core files: "$f
  mcopy -i $DEVICE@@512 core/$f ::
  if echo "$f" | grep -iqF core; then
    mattrib -i $DEVICE@@512 +h ::$f
  fi
done

sync

echo "Done"
