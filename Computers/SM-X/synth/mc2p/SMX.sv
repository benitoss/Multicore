/*

   Multicore 2 / Multicore 2+
   Copyright (c) 2017-2021 - Victor Trucco


   All rights reserved

   Redistribution and use in source and synthezised forms, with or without
   modification, are permitted provided that the following conditions are met:

   Redistributions of source code must retain the above copyright notice,
   this list of conditions and the following disclaimer.

   Redistributions in synthesized form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.

   Neither the name of the author nor the names of other contributors may
   be used to endorse or promote products derived from this software without
   specific prior written permission.

   THIS CODE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
   AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
   THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
   PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE
   LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
   CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
   INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
   CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
   ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
   POSSIBILITY OF SUCH DAMAGE.

   You are responsible for any legal issues arising from your use of this code.

*///============================================================================
//  MSX top level for MiST
//
//  This program is free software; you can redistribute it and/or modify it
//  under the terms of the GNU General Public License as published by the Free
//  Software Foundation; either version 2 of the License, or (at your option)
//  any later version.
//
//  This program is distributed in the hope that it will be useful, but WITHOUT
//  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
//  more details.
//
//  You should have received a copy of the GNU General Public License along
//  with this program; if not, write to the Free Software Foundation, Inc.,
//  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
//============================================================================
//
//============================================================================
//
//  Multicore 2+ Top by Victor Trucco
//
//============================================================================

`default_nettype none

module SMX_mc2p
(
    // Clocks
    input wire  clock_50_i,

    // Buttons
    input wire [4:1]    btn_n_i,

    // SRAM (IS61WV20488FBLL-10)
    output wire [20:0]sram_addr_o  = 21'b000000000000000000000,
    inout wire  [7:0]sram_data_io   = 8'bzzzzzzzz,
    output wire sram_we_n_o     = 1'b1,
    output wire sram_oe_n_o     = 1'b1,

    // SDRAM (W9825G6KH-6)
    output [12:0] SDRAM_A,
    output  [1:0] SDRAM_BA,
    inout  [15:0] SDRAM_DQ,
    output        SDRAM_DQMH,
    output        SDRAM_DQML,
    output        SDRAM_CKE,
    output        SDRAM_nCS,
    output        SDRAM_nWE,
    output        SDRAM_nRAS,
    output        SDRAM_nCAS,
    output        SDRAM_CLK,

    // PS2
    inout wire  ps2_clk_io        = 1'bz,
    inout wire  ps2_data_io       = 1'bz,
    inout wire  ps2_mouse_clk_io  = 1'bz,
    inout wire  ps2_mouse_data_io = 1'bz,

    // SD Card
    output wire sd_cs_n_o         = 1'bZ,
    output wire sd_sclk_o         = 1'bZ,
    output wire sd_mosi_o         = 1'bZ,
    input wire  sd_miso_i,

    // Joysticks
    output wire joy_clock_o       = 1'b1,
    output wire joy_load_o        = 1'b1,
    input  wire joy_data_i,
    output wire joy_p7_o          = 1'b1,

    // Audio
    output      AUDIO_L,
    output      AUDIO_R,
    input wire  ear_i,
    output wire mic_o             = 1'b0,

    // VGA
    output  [4:0] VGA_R,
    output  [4:0] VGA_G,
    output  [4:0] VGA_B,
    output        VGA_HS,
    output        VGA_VS,

    //STM32
    input wire  stm_tx_i,
    output wire stm_rx_o,
    output wire stm_rst_o           = 1'bz, // '0' to hold the microcontroller reset line, to free the SD card

    input         SPI_SCK,
    output        SPI_DO,
    input         SPI_DI,
    input         SPI_SS2,
    output wire   SPI_nWAIT        = 1'b1, // '0' to hold the microcontroller data streaming

    inout [31:0] GPIO,

    output LED                    = 1'b1 // '0' is LED on

);


//---------------------------------------------------------
//-- MC2+ defaults
//---------------------------------------------------------
assign GPIO = 32'bzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzz;
assign stm_rst_o    = 1'bZ;
assign stm_rx_o = 1'bZ;

//no SRAM for this core
assign sram_we_n_o  = 1'b1;
assign sram_oe_n_o  = 1'b1;

wire joy1_up_i, joy1_down_i, joy1_left_i, joy1_right_i, joy1_p6_i, joy1_p9_i;
wire joy2_up_i, joy2_down_i, joy2_left_i, joy2_right_i, joy2_p6_i, joy2_p9_i;

wire joyserial_clk;

always @(posedge clk_sys) begin
    joyserial_clk <= ~joyserial_clk;
end
joystick_serial  joystick_serial
(
    .clk_i           ( joyserial_clk ),
    .joy_data_i      ( joy_data_i ),
    .joy_clk_o       ( joy_clock_o ),
    .joy_load_o      ( joy_load_o ),

    .joy1_up_o       ( joy1_up_i ),
    .joy1_down_o     ( joy1_down_i ),
    .joy1_left_o     ( joy1_left_i ),
    .joy1_right_o    ( joy1_right_i ),
    .joy1_fire1_o    ( joy1_p6_i ),
    .joy1_fire2_o    ( joy1_p9_i ),

    .joy2_up_o       ( joy2_up_i ),
    .joy2_down_o     ( joy2_down_i ),
    .joy2_left_o     ( joy2_left_i ),
    .joy2_right_o    ( joy2_right_i ),
    .joy2_fire1_o    ( joy2_p6_i ),
    .joy2_fire2_o    ( joy2_p9_i )
);



/*

//--- Joystick read with sega 6 button support----------------------

reg clk_sega_s;
localparam CLOCK = 21484; // clk_sys speed
localparam TIMECLK = (9 * (CLOCK / 1000)); //calculate 9us state time based on input clock
reg [9:0] delay = TIMECLK;

always@(posedge clk_sys)
begin
    delay <= delay - 10'd1;

    if (delay == 10'd0)
        begin
            clk_sega_s <= ~clk_sega_s;
            delay <= TIMECLK;
        end
end


reg [11:0]joy1_s;
reg [11:0]joy2_s;
reg joyP7_s;

reg [7:0]state_v = 8'd0;
reg j1_sixbutton_v = 1'b0;
reg j2_sixbutton_v = 1'b0;

always @(negedge clk_sega_s)
begin


        state_v <= state_v + 1;


        case (state_v)          //-- joy_s format MXYZ SACB RLDU
            8'd0:
                joyP7_s <=  1'b0;

            8'd1:
                joyP7_s <=  1'b1;

            8'd2:
                begin
                    joy1_s[3:0] <= {joy1_right_i, joy1_left_i, joy1_down_i, joy1_up_i}; //-- R, L, D, U
                    joy2_s[3:0] <= {joy2_right_i, joy2_left_i, joy2_down_i, joy2_up_i}; //-- R, L, D, U
                    joy1_s[5:4] <= {joy1_p9_i, joy1_p6_i}; //-- C, B
                    joy2_s[5:4] <= {joy2_p9_i, joy2_p6_i}; //-- C, B
                    joyP7_s <= 1'b0;
                    j1_sixbutton_v <= 1'b0; //-- Assume it's not a six-button controller
                    j2_sixbutton_v <= 1'b0; //-- Assume it's not a six-button controller
                end

            8'd3:
                begin
                    if (joy1_right_i == 1'b0 && joy1_left_i == 1'b0) // it's a megadrive controller
                            joy1_s[7:6] <= { joy1_p9_i , joy1_p6_i }; //-- Start, A
                    else
                            joy1_s[7:4] <= { 1'b1, 1'b1, joy1_p9_i, joy1_p6_i }; //-- read A/B as master System

                    if (joy2_right_i == 1'b0 && joy2_left_i == 1'b0) // it's a megadrive controller
                            joy2_s[7:6] <= { joy2_p9_i , joy2_p6_i }; //-- Start, A
                    else
                            joy2_s[7:4] <= { 1'b1, 1'b1, joy2_p9_i, joy2_p6_i }; //-- read A/B as master System


                    joyP7_s <= 1'b1;
                end

            8'd4:
                joyP7_s <= 1'b0;

            8'd5:
                begin
                    if (joy1_right_i == 1'b0 && joy1_left_i == 1'b0 && joy1_down_i == 1'b0 && joy1_up_i == 1'b0 )
                        j1_sixbutton_v <= 1'b1; // --it's a six button


                    if (joy2_right_i == 1'b0 && joy2_left_i == 1'b0 && joy2_down_i == 1'b0 && joy2_up_i == 1'b0 )
                        j2_sixbutton_v <= 1'b1; // --it's a six button


                    joyP7_s <= 1'b1;
                end

            8'd6:
                begin
                    if (j1_sixbutton_v == 1'b1)
                        joy1_s[11:8] <= { joy1_right_i, joy1_left_i, joy1_down_i, joy1_up_i }; //-- Mode, X, Y e Z


                    if (j2_sixbutton_v == 1'b1)
                        joy2_s[11:8] <= { joy2_right_i, joy2_left_i, joy2_down_i, joy2_up_i }; //-- Mode, X, Y e Z


                    joyP7_s <= 1'b0;
                end

            default:
                joyP7_s <= 1'b1;

        endcase
        if (joy1_s[7] || joy1_s[11])
            osd_keys[7:0] <= { osd_s[7], osd_s[6], osd_s[5], osd_s[4]&joy1_s[4]&joy1_s[5]&joy1_s[6], osd_s[3]&joy1_s[3], osd_s[2]&joy1_s[2], osd_s[1]&joy1_s[1], osd_s[0]&joy1_s[0]};
        else
            osd_keys[7:0] <= { 1'b0, 1'b1, 1'b1, osd_s[4]&joy1_s[4]&joy1_s[5]&joy1_s[6], osd_s[3]&joy1_s[3], osd_s[2]&joy1_s[2], osd_s[1]&joy1_s[1], osd_s[0]&joy1_s[0]};
end

assign joy_0 = {joy1_s[5], joy1_s[4], joy1_s[0], joy1_s[1], joy1_s[2], joy1_s[3]};
assign joy_1 = {joy2_s[5], joy2_s[4], joy2_s[0], joy2_s[1], joy2_s[2], joy2_s[3]};

assign joy_p7_o = joyP7_s;
*/
assign joy_p7_o = (midi_active_s) ? midi_o_s : 1'b1;
assign joy_0 = { joy1_p9_i, joy1_p6_i, joy1_up_i, joy1_down_i, joy1_left_i, joy1_right_i};
assign joy_1 = { joy2_p9_i, joy2_p6_i, joy2_up_i, joy2_down_i, joy2_left_i, joy2_right_i};

reg [7:0]osd_keys = 8'b11111111;
assign osd_keys = { osd_s[7:5], osd_s[4] & joy1_p9_i&joy1_p6_i, osd_s[3]&joy1_right_i, osd_s[2]&joy1_left_i, osd_s[1]&joy1_down_i, osd_s[0]&joy1_up_i};

reg [7:0] pump_s = 8'b11111111;
PumpSignal PumpSignal (clk_sys, ~pll_locked, /*ioctl_download */ sd_flg, pump_s);

//-----------------------------------------------------------------

assign LED  = ~leds[0];
//assign LED  = mouse_strobe;

`include "build_id.v"
parameter CONF_STR = {
//    "D,disable STM SD;",
    "P,INIT;",
    "I1,IMG/VHD,Load Image...;",
    "OAB,Scanlines,Off,25%,50%,75%;",
    "OC,Blend,Off,On;",
    "O2,CPU Clock,Normal,Turbo;",
    "O3,Slot1,MegaSCC+ 1MB,External;",
    "O45,Slot2,MegaSCC+ 2MB,External,MegaRAM 2MB,MegaRAM 1MB;",
    "O6,RAM,2048kB,4096kB;",
    "OD,Slot 0,Expanded,Primary;",
    "O7,OPL3 sound,Yes,No;",
    "O9,Tape sound,Off,On;",
    "O1,Scandoubler,On,Off;",
    "OEF,Keymap,BR,EN,ES,FR;",
    "T0,Reset;"
};

////////////////////   CLOCKS   ///////////////////

wire pll_locked;
wire clk_sys;
wire memclk;

pll pll
(
    .inclk0(clock_50_i),
    .c0(clk_sys),
    .c1(memclk),
    .c2(SDRAM_CLK),
    .locked(pll_locked)
);

//assign SDRAM_CLK = memclk;

//////////////////   MiST I/O   ///////////////////
wire  [7:0] joy_0;
wire  [7:0] joy_1;
wire  [1:0] buttons;
wire [63:0] status;
wire        ypbpr;
wire        sd_flag;

reg  [31:0] sd_lba;
reg         sd_rd = 0;
reg         sd_wr = 0;
wire        sd_conf;
wire        sd_ack;
wire        sd_ack_conf;
wire        sd_sdhc;
wire  [8:0] sd_buff_addr;
wire  [7:0] sd_buff_dout;
wire  [7:0] sd_buff_din;
wire        sd_buff_wr;
wire        img_mounted;
wire        img_readonly;
wire [31:0] img_size;

//wire        ps2_kbd_clk;
//wire        ps2_kbd_data;

wire  [8:0] mouse_x;
wire  [8:0] mouse_y;
wire  [7:0] mouse_flags;
wire        mouse_strobe;

wire [63:0] rtc;
/*
user_io #(.STRLEN($size(CONF_STR)>>3), .PS2DIV(1500)) user_io
(
        .clk_sys(clk_sys),
        .clk_sd(clk_sys),
        .SPI_SS_IO(CONF_DATA0),
        .SPI_CLK(SPI_SCK),
        .SPI_MOSI(SPI_DI),
        .SPI_MISO(SPI_DO),

        .conf_str(CONF_STR),

        .status(status),
        .scandoubler_disable(scandoubler_disable),
        .ypbpr(ypbpr),
        .buttons(buttons),
        .rtc(rtc),

        .joystick_0(joy_0),
        .joystick_1(joy_1),

        .sd_conf(sd_conf),
        .sd_ack(sd_ack),
        .sd_ack_conf(sd_ack_conf),
        .sd_sdhc(sd_sdhc),
        .sd_rd(sd_rd),
        .sd_wr(sd_wr),
        .sd_lba(sd_lba),
        .sd_buff_addr(sd_buff_addr),
        .sd_din(sd_buff_din),
        .sd_dout(sd_buff_dout),
        .sd_dout_strobe(sd_buff_wr),

        .ps2_kbd_clk(ps2_kbd_clk),
        .ps2_kbd_data(ps2_kbd_data),

        .mouse_x(mouse_x),
        .mouse_y(mouse_y),
        .mouse_flags(mouse_flags),
        .mouse_strobe(mouse_strobe),

        // unused
        .switches(),
        .ps2_mouse_clk(),
        .ps2_mouse_data(),
        .joystick_analog_0(),
        .joystick_analog_1()
);
*/

wire sd_sclk_o_s, sd_cs_n_o_s, sd_mosi_o_s, sd_miso_i_s;

sd_card sd_card
(
        .clk_sys(clk_sys),
        .img_mounted(img_mounted),
        .img_size(img_size),
        .sd_conf(sd_conf),
        .sd_ack(sd_ack),
        .sd_ack_conf(sd_ack_conf),
        .sd_sdhc(sd_sdhc),
        .sd_rd(sd_rd),
        .sd_wr(sd_wr),
        .sd_lba(sd_lba),
        .sd_buff_addr(sd_buff_addr),
        .sd_buff_din(sd_buff_din),
        .sd_buff_dout(sd_buff_dout),
        .sd_buff_wr(sd_buff_wr),
        .allow_sdhc(1),
        .sd_sck(sd_sclk_o_s),
        .sd_cs(sd_cs_n_o_s),
        .sd_sdi(sd_mosi_o_s),
        .sd_sdo(sd_miso_i_s)
);

reg [7:0]osd_s;
wire ioctl_download, external_sd_disabled;


data_io_nes #(
    .STRLEN(($size(CONF_STR)>>3)))
data_io_nes
(
    .clk_sys    ( clk_sys ),
    .SPI_SCK    ( SPI_SCK ),
    .SPI_DI     ( SPI_DI  ),
    .SPI_SS2    ( SPI_SS2 ),
    .SPI_DO     ( SPI_DO  ),

    .data_in    ( osd_keys & pump_s  ),
    .conf_str   ( CONF_STR ),
    .status     ( status ),
    .sd_flag    ( sd_flag ),
    .ioctl_download ( ioctl_download ),

        //--------------------------------------------

    .clk_sd(clk_sys),
    .sd_conf(sd_conf),
    .sd_sdhc(sd_sdhc),
    .sd_lba(sd_lba),
    .sd_rd({1'b0,sd_rd}),
    .sd_wr({1'b0,sd_wr}),
    .sd_ack(sd_ack),
    .sd_buff_addr(sd_buff_addr),
    .sd_dout(sd_buff_dout),
    .sd_din(sd_buff_din),
    .sd_dout_strobe(sd_buff_wr),
    .sd_din_strobe(),
    .img_mounted(img_mounted),
    .img_size(img_size),

    .external_sd_disabled ( external_sd_disabled ) //output: signal DIS_SD from .INI to disable the STM SD pins 
);

wire [5:0] audio_li;
wire [5:0] audio_ri;
wire [5:0] audio_l;
wire [5:0] audio_r;

wire [5:0] joya = joy_0[5:0];
wire [5:0] joyb = joy_1[5:0];
wire [5:0] msx_joya;
wire [5:0] msx_joyb;
wire       msx_stra;
wire       msx_strb;

wire       Sd_Ck;
wire       Sd_Cm;
wire [3:0] Sd_Dt;

//wire       msx_ps2_kbd_clk = ps2_kbd_clk;
//wire       msx_ps2_kbd_data = (ps2_kbd_data == 1'b0 ? ps2_kbd_data : 1'bZ);
reg  [7:0] dipsw;
wire [7:0] leds;

reg reset;
wire resetW = status[0] | ~btn_n_i[4] | img_mounted | (PoR_counter != 25'd0) | ext_cart_reset_s;

wire sd_flg = sd_flag | status[31];

reg [5:0] clock_div_q;

reg last_PoR;
reg [24:0] PoR_counter = 25'd0;

always @(posedge clk_sys) 
begin
    last_PoR <= sd_flg;

    if (sd_flg && ~last_PoR) PoR_counter <= 25'b1111111111111111111111111;

    if (PoR_counter > 25'd0) PoR_counter <= PoR_counter - 25'd1;

end



always @(posedge clk_sys) begin
    reset <= resetW;
    dipsw <= {1'b0, ~status[6], ~status[5], status[4], status[3], ~status[1] & status[8], status[1], ~status[2]};
    audio_li <= audio_l;
    if (status[9]) 
        begin
        audio_ri <= audio_r;
        end
    else 
        audio_ri<= audio_l;

    if (reset)
        clock_div_q <= 6'd0;
    else
        clock_div_q <= clock_div_q + 6'd1;

end




assign msx_joya = mouse_en ? /*(mouse ? 1'bZ : mouse)*/ mouse : (~joya & ~msx_stra ? joya : 1'bZ);
assign msx_joyb =                                     (~joyb & ~msx_strb ? joyb : 1'bZ);

//reg        mouse_en = 0;
wire       mouse_en;
reg  [5:0] mouse;
    
    
ps2mouse mousectrl
(
    .clk        ( clock_div_q[5] ),      //-- need a slower clock to avoid loosing data
    .reset      ( reset ), //-- slow reset signal
 
    .ps2mdat    ( ps2_mouse_data_io ), //  -- mouse PS/2 data
    .ps2mclk    ( ps2_mouse_clk_io ),  //      -- mouse PS/2 clk
 
    .xcount     ( mouse_x ),       //    -- mouse X counter      
    .ycount     ( mouse_y ),       //    -- mouse Y counter
    .zcount     (   ),   //-- mouse Z counter
    .mleft      ( mouse_flags[0] ),  //-- left mouse button output
    .mright     ( mouse_flags[1] ),  //-- right mouse button output
    .mthird     (                ),  //-- third(middle) mouse button output
   
    .sof        ( 1'b0 ),             //    -- mouse joystick emulation enable bit
    .mou_emu    ( 0 ),   //-- mouse with joystick input
    
    .test_load  ( 1'b0 ),            //        -- load test value to mouse counter
    .test_data  ( 0 ),  //  -- mouse counter test value
    .mouse_data_out ( mouse_strobe ) //-- mouse has data top present
);

always @(posedge clk_sys) begin

    reg        stra_d;
    reg  [7:0] mouse_x_latch;
    reg  [7:0] mouse_y_latch;
    reg  [1:0] mouse_state;
    reg [17:0] mouse_timeout;

    if (reset) begin
        mouse_en <= 0;
        mouse_state <= 0;
    end
    else if (mouse_strobe) mouse_en <= 1;
    else if (~&joya) mouse_en <= 0;

    if (mouse_strobe) begin
        mouse_x_latch <= mouse_x; //~mouse_x + 1'd1; //2nd complement of x
        mouse_y_latch <= mouse_y;
    end

    mouse[5:4] <= mouse_flags[1:0];
    if (mouse_en) begin
        if (mouse_timeout) begin
            mouse_timeout <= mouse_timeout - 1'd1;
            if (mouse_timeout == 1) mouse_state <= 0;
        end

        stra_d <= msx_stra;
        if (stra_d ^ msx_stra) begin
            mouse_timeout <= 18'd100000;
            mouse_state <= mouse_state + 1'd1;
            case (mouse_state)
            2'b00: mouse[3:0] <= {mouse_x_latch[4],mouse_x_latch[5],mouse_x_latch[6],mouse_x_latch[7]};
            2'b01: mouse[3:0] <= {mouse_x_latch[0],mouse_x_latch[1],mouse_x_latch[2],mouse_x_latch[3]};
            2'b10: mouse[3:0] <= {mouse_y_latch[4],mouse_y_latch[5],mouse_y_latch[6],mouse_y_latch[7]};
            2'b11:
            begin
                mouse[3:0] <= {mouse_y_latch[0],mouse_y_latch[1],mouse_y_latch[2],mouse_y_latch[3]};
                mouse_x_latch <= 0;
                mouse_y_latch <= 0;
            end
            endcase
        end
    end
end


//assign sd_sclk_o = Sd_Ck;
//assign sd_mosi_o = Sd_Cm;
//assign sd_cs_n_o = Sd_Dt[3];
//assign Sd_Dt[0] = sd_miso_i;

always @(*) begin
    if (external_sd_disabled == 1'b0 || pump_s != 8'b11111111) //use IMG or under initial reset
    begin
        sd_sclk_o = 1'bZ;
        sd_mosi_o = 1'bZ;
        sd_cs_n_o = 1'bZ;

        sd_sclk_o_s = Sd_Ck;
        sd_mosi_o_s = Sd_Cm;
        sd_cs_n_o_s = Sd_Dt[3];
        Sd_Dt[0] = sd_miso_i_s;
    end
    else //use External SD
    begin
        sd_sclk_o_s = 1'b1;
        sd_mosi_o_s = 1'b1;
        sd_cs_n_o_s = 1'b1;

        sd_sclk_o = Sd_Ck;
        sd_mosi_o = Sd_Cm;
        sd_cs_n_o = Sd_Dt[3];
        Sd_Dt[0] = sd_miso_i;
    end
end

wire midi_o_s;
wire midi_active_s;

emsx_top emsx
(
//        -- Clock, Reset ports
        .clk21m     (clk_sys),
        .memclk     (memclk),
        .pSltRst_n  (~reset),

//        -- SD-RAM ports
        .pMemAdr   ( SDRAM_A ),
        .pMemDat   ( SDRAM_DQ ),
        .pMemLdq   ( SDRAM_DQML ),
        .pMemUdq   ( SDRAM_DQMH ),
        .pMemWe_n  ( SDRAM_nWE ),
        .pMemCas_n ( SDRAM_nCAS ),
        .pMemRas_n ( SDRAM_nRAS ),
        .pMemCs_n  ( SDRAM_nCS ),
        .pMemBa0   ( SDRAM_BA[0] ),
        .pMemBa1   ( SDRAM_BA[1] ),
        .pMemCke   ( SDRAM_CKE ),

//        -- PS/2 keyboard ports
        .pPs2Clk   (ps2_clk_io),
        .pPs2Dat   (ps2_data_io),

//        -- Joystick ports (Port_A, Port_B)
        .pJoyA      ( {msx_joya[5:4], msx_joya[0], msx_joya[1], msx_joya[2], msx_joya[3]} ),
        .pStra      ( msx_stra ),
        .pJoyB      ( {msx_joyb[5:4], msx_joyb[0], msx_joyb[1], msx_joyb[2], msx_joyb[3]} ),
        .pStrb      ( msx_strb ),

//        -- SD/MMC slot ports
//        .pSd_Ck     (sd_sclk_o), // (Sd_Ck),
//        .pSd_Cm     (sd_mosi_o), // (Sd_Cm),
//        .pSd_Dt     ({sd_cs_n_o,2'bZZ,sd_miso_i}), // (Sd_Dt),

//        .pSd_Ck     (sd_sclk_o_s), // (Sd_Ck),
//        .pSd_Cm     (sd_mosi_o_s), // (Sd_Cm),
//        .pSd_Dt     ({sd_cs_n_o_s,2'bZZ,sd_miso_i_s}), // (Sd_Dt),

        .pSd_Ck     (Sd_Ck),
        .pSd_Cm     (Sd_Cm),
        .pSd_Dt     (Sd_Dt),

//        -- DIP switch, Lamp ports
        .pDip       (dipsw),
        .pLed       (leds),

//        -- Video, Audio/CMT ports
        .pDac_VR    (R_O),      // RGB_Red / Svideo_C
        .pDac_VG    (G_O),      // RGB_Grn / Svideo_Y
        .pDac_VB    (B_O),      // RGB_Blu / CompositeVideo
        .pVideoHS_n (HSync),    // HSync(RGB15K, VGA31K)
        .pVideoVS_n (VSync),    // VSync(RGB15K, VGA31K)

        .CmtIn      (ear_i),
        .mic_o      (mic_o),
        .pDac_SL    (audio_l),
        .pDac_SR    (audio_r),

        .osd_o      (osd_s),
        .opl3_enabled ( ~status[7] ),
        .slot0_exp  ( ~status[13] ),
        .kbd_layout ( {status[14], status[15]} ),

      // SM-X
        .esp_rx_o   ( esp_rx_o ),
        .esp_tx_i   ( esp_tx_i ),

        .midi_o        ( midi_o_s ),
        .midi_active_o ( midi_active_s ),

        // Slots
        .pCpuClk         ( slot_CLOCK_o  ), 
        .pSltSltsl_n     ( slot_SLOT1_o  ), 
        .pSltSlts2_n     ( slot_SLOT2_o  ), 

        .pSltMerq_n      ( slot_MREQ_o   ),
        .pSltIorq_n      ( slot_IOREQ_o  ), 
        .pSltRd_n        ( slot_RD_o     ), 
        .pSltWr_n        ( slot_WR_o     ), 

        .pSltAdr         ( ext_cart_a    ), 
        .pSltDat         ( sram_data_io  ), 
        .pSltBdir_n      ( slot_BUSDIR_i ), 

        .pSltCs1_n       ( slot_CS1_o    ), 
        .pSltCs2_n       ( slot_CS2_o    ), 
        .pSltCs12_n      ( slot_CS12_o   ), 
        .pSltRfsh_n      ( slot_RFSH_i   ), 
        .pSltWait_n      ( slot_WAIT_i   ), 
        .pSltInt_n       ( GPIO[27] ),//slot_INT_i    ), 
        .pSltM1_n        ( slot_M1_o     )

);



//////////////////   VIDEO   //////////////////
wire  [5:0] R_O;
wire  [5:0] G_O;
wire  [5:0] B_O;
wire        HSync, VSync, CSync;

wire [5:0] osd_r_o, osd_g_o, osd_b_o;
/*
osd osd
(
    .clk_sys(clk_sys),
    .SPI_DI(SPI_DI),
    .SPI_SCK(SPI_SCK),
    .SPI_SS3(SPI_SS2),
    .R_in(R_O),
    .G_in(G_O),
    .B_in(B_O),
    .HSync(HSync),
    .VSync(VSync),
    .R_out(osd_r_o),
    .G_out(osd_g_o),
    .B_out(osd_b_o)
    );

assign VGA_R  = osd_r_o;
assign VGA_G  = osd_g_o;
assign VGA_B  = osd_b_o;
assign VGA_HS = HSync;
assign VGA_VS = VSync;
*/
     mist_video #( .OSD_COLOR ( 3'b001 )) mist_video_inst
     (
         .clk_sys     ( memclk ), //clk_sys ),
         .scanlines   ( status[11:10] ),
         .rotate      ( 2'b00 ),
         .scandoubler_disable  ( 1'b1 ),
         .ce_divider  ( 1'b0 ), //1 para clk_sys ou 0 com clksdram para usar blend
         .blend       ( status[12] ),
         .no_csync    ( 1'b1 ),

         .SPI_SCK     ( SPI_SCK ),
         .SPI_SS3     ( SPI_SS2 ),
         .SPI_DI      ( SPI_DI ),

         .HSync       ( HSync ),
         .VSync       ( VSync ),
         .R           ( R_O ),
         .G           ( G_O ),
         .B           ( B_O ),

         .VGA_HS      ( VGA_HS ),
         .VGA_VS      ( VGA_VS ),
         .VGA_R       ( VGA_R ),
         .VGA_G       ( VGA_G ),
         .VGA_B       ( VGA_B ),

         .osd_enable  ( )
     );

assign AUDIO_L = audio_li[0];
assign AUDIO_R = audio_ri[0];
/*
//////////////////   AUDIO   //////////////////
dac #(6) dac_l
(
    .clk_i(clk_sys),
    .res_n_i(1'b1),
    .dac_i(audio_li),
    .dac_o(AUDIO_L)
);

dac #(6) dac_r
(
    .clk_i(clk_sys),
    .res_n_i(1'b1),
    .dac_i(audio_ri),
    .dac_o(AUDIO_R)
);
*/


//////////////////   Expansion #02   //////////////////
wire ext_cart_detect, ext_cart_detect_db, ext_lvc_oe, ext_lvc_dir;
wire ext_cart_detect_r, ext_cart_reset_s;
reg [15:0] ext_cart_reset_cnt = 16'b1111111111111111;

wire [7:0] ext_cart_d, cpu_do_s;
wire [15:0] ext_cart_a, cpu_a_s;
wire ext_cs, ext_rd, ext_wr, ext_reset, ext_clock, ext_wait;
wire cpu_rd_s, cpu_wr_s;

wire sw1, sw2, cs1,cs2,cs12;
wire slot_SLOT1_o, slot_SLOT2_o, slot_SLOT3_o;
wire slot_CS1_o, slot_CS2_o, slot_CS12_o;
wire slot_IOREQ_o, slot_MREQ_o, slot_RD_o, slot_WR_o, slot_M1_o;
wire slot_RFSH_i, slot_WAIT_i, slot_INT_i, slot_BUSDIR_i;
wire slot_CLOCK_o, slot_RESET_o, slot_RESET_io;
wire P1_io, P2_io, P3_io, P4_io, P5_io;

wire esp_rx_o, esp_tx_i;

//--- alias
assign ext_cart_detect = GPIO[5];

assign GPIO[13] = ext_lvc_dir;
assign GPIO[12] = ext_lvc_oe;

assign esp_tx_i = GPIO[7];
assign GPIO[11] = esp_rx_o;

assign sram_addr_o[4]  =  ( ext_cart_detect_db ) ? ext_cart_a[0]  : 1'bZ;
assign sram_addr_o[5]  =  ( ext_cart_detect_db ) ? ext_cart_a[1]  : 1'bZ;
assign sram_addr_o[2]  =  ( ext_cart_detect_db ) ? ext_cart_a[2]  : 1'bZ;
assign sram_addr_o[3]  =  ( ext_cart_detect_db ) ? ext_cart_a[3]  : 1'bZ;
assign sram_addr_o[0]  =  ( ext_cart_detect_db ) ? ext_cart_a[4]  : 1'bZ;
assign sram_addr_o[1]  =  ( ext_cart_detect_db ) ? ext_cart_a[5]  : 1'bZ;
assign sram_addr_o[10] =  ( ext_cart_detect_db ) ? ext_cart_a[6]  : 1'bZ;
assign sram_addr_o[11] =  ( ext_cart_detect_db ) ? ext_cart_a[7]  : 1'bZ;
assign sram_addr_o[8]  =  ( ext_cart_detect_db ) ? ext_cart_a[8]  : 1'bZ;
assign sram_addr_o[15] =  ( ext_cart_detect_db ) ? ext_cart_a[9]  : 1'bZ;
assign sram_addr_o[12] =  ( ext_cart_detect_db ) ? ext_cart_a[10] : 1'bZ;
assign sram_addr_o[13] =  ( ext_cart_detect_db ) ? ext_cart_a[11] : 1'bZ;
assign sram_addr_o[9]  =  ( ext_cart_detect_db ) ? ext_cart_a[12] : 1'bZ;
assign sram_addr_o[6]  =  ( ext_cart_detect_db ) ? ext_cart_a[13] : 1'bZ;
assign sram_addr_o[7]  =  ( ext_cart_detect_db ) ? ext_cart_a[14] : 1'bZ;
assign sram_addr_o[14] =  ( ext_cart_detect_db ) ? ext_cart_a[15] : 1'bZ;

assign GPIO[21] = ( ext_cart_detect_db ) ? slot_SLOT1_o : 1'bZ;
assign GPIO[22] = ( ext_cart_detect_db ) ? slot_SLOT2_o : 1'bZ;
assign GPIO[26] = ( ext_cart_detect_db ) ? slot_SLOT3_o : 1'bZ;

assign GPIO[25] = ( ext_cart_detect_db ) ? slot_CS1_o : 1'bZ;
assign GPIO[24] = ( ext_cart_detect_db ) ? slot_CS2_o : 1'bZ;
assign GPIO[23] = ( ext_cart_detect_db ) ? slot_CS12_o : 1'bZ;

assign sram_addr_o[16] = ( ext_cart_detect_db ) ? slot_MREQ_o : 1'bZ;
assign GPIO[18] = ( ext_cart_detect_db ) ? slot_IOREQ_o : 1'bZ;
assign GPIO[16] = ( ext_cart_detect_db ) ? slot_RD_o : 1'bZ;
assign GPIO[14] = ( ext_cart_detect_db ) ? slot_WR_o : 1'bZ;
assign GPIO[19] = ( ext_cart_detect_db ) ? slot_M1_o : 1'bZ;

assign sw1 = GPIO[8];
assign sw2 = GPIO[6];
assign slot_WAIT_i = GPIO[29];
//assign slot_RFSH_i = GPIO[20];
//assign slot_INT_i = GPIO[27];
assign slot_BUSDIR_i = GPIO[31];



assign GPIO[10] = ( ext_cart_detect_db ) ? slot_CLOCK_o : 1'bZ;

assign GPIO[0] = P1_io;
assign GPIO[2] = P2_io;
assign GPIO[3] = P3_io;
assign GPIO[4] = P4_io;
assign GPIO[1] = P5_io;

//assign GPIO[9] = slot_RESET_o; //reset out
//assign GPIO[28] = slot_RESET_io; //reset inout
// end alias
   
//assign slot_RESET_o = ~reset;
//assign slot_RESET_io = ~reset;

assign ext_lvc_oe =  ( ext_cart_detect_db  == 1'b0 ) ? 1'b1 : (slot_SLOT1_o == 1'b0) ? 1'b0 : (slot_SLOT2_o == 1'b0) ? 1'b0 : (slot_IOREQ_o == 1'b0 && slot_BUSDIR_i == 1'b0) ? 1'b0 : 1'b1;
assign ext_lvc_dir = ( ext_cart_detect_db  == 1'b0 ) ? 1'b0 : ~slot_RD_o; // port A=SLOT, B=FPGA     DIR(1)=A to B   

assign slot_SLOT3_o = slot_SLOT1_o;

//clear the inputs tri-state I/Os
assign GPIO[8:5] = 4'bzzzz;
assign GPIO[29] = 1'bz;
assign GPIO[31] = 1'bz;

debounce #(23) debounce
(
    .clk_i     ( clk_sys ),
    .button_i  ( ~ext_cart_detect ),
    .result_o  ( ext_cart_detect_db )
);

// Reset when the external module is turned on or off
always @ (posedge clk_sys)
    begin

          ext_cart_detect_r <= ext_cart_detect_db;

          if ((ext_cart_detect_db == 1'b0 && ext_cart_detect_r == 1'b1) ||
              (ext_cart_detect_db == 1'b1 && ext_cart_detect_r == 1'b0)) // reset signal on edges
             ext_cart_reset_cnt <= 16'b1111111111111111;


          if (ext_cart_reset_cnt > 16'd0)
            begin
                ext_cart_reset_s <= 1'b1;
                ext_cart_reset_cnt <= ext_cart_reset_cnt - 1;
            end
          else
            ext_cart_reset_s <= 1'b0;
end

endmodule
