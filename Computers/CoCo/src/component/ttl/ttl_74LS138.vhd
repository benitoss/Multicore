--
-- Multicore 2 / Multicore 2+
--
-- Copyright (c) 2017-2020 - Victor Trucco
--
-- All rights reserved
--
-- Redistribution and use in source and synthezised forms, with or without
-- modification, are permitted provided that the following conditions are met:
--
-- Redistributions of source code must retain the above copyright notice,
-- this list of conditions and the following disclaimer.
--
-- Redistributions in synthesized form must reproduce the above copyright
-- notice, this list of conditions and the following disclaimer in the
-- documentation and/or other materials provided with the distribution.
--
-- Neither the name of the author nor the names of other contributors may
-- be used to endorse or promote products derived from this software without
-- specific prior written permission.
--
-- THIS CODE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
-- AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
-- THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
-- PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE
-- LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
-- CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
-- SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
-- INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
-- CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
-- ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
-- POSSIBILITY OF SUCH DAMAGE.
--
-- You are responsible for any legal issues arising from your use of this code.
--
		
library IEEE;
use IEEE.std_logic_1164.all;
Use IEEE.std_logic_unsigned.all;
use ieee.numeric_std.all;

entity ttl_74ls138 is
	port
  (
  	-- input
  	a				: in std_logic;
  	b				: in std_logic;
  	c				: in std_logic;

		g1			: in std_logic;
		g2a_n		: in std_logic;
		g2b_n		: in std_logic;
		
  	-- output
  	y_n			: out std_logic_vector(7 downto 0)
	);
end ttl_74ls138;

architecture SYN of ttl_74ls138 is

	signal enabled	: std_logic;
	
begin

	enabled <= g1 and not g2a_n and not g2b_n;

	y_n(0) <= '1' when enabled = '0' else
						not (not a and not b and not c);
	y_n(1) <= '1' when enabled = '0' else
						not (a and not b and not c);
	y_n(2) <= '1' when enabled = '0' else
						not (not a and b and not c);
	y_n(3) <= '1' when enabled = '0' else
						not (a and b and not c);
	y_n(4) <= '1' when enabled = '0' else
						not (not a and not b and c);
	y_n(5) <= '1' when enabled = '0' else
						not (a and not b and c);
	y_n(6) <= '1' when enabled = '0' else
						not (not a and b and c);
	y_n(7) <= '1' when enabled = '0' else
						not (a and b and c);

end SYN;

library IEEE;
use IEEE.std_logic_1164.all;
Use IEEE.std_logic_unsigned.all;
use ieee.numeric_std.all;

entity ttl_74ls138_p is
	port
  (
  	-- input
  	a				: in std_logic;
  	b				: in std_logic;
  	c				: in std_logic;

		g1			: in std_logic;
		g2a			: in std_logic;
		g2b			: in std_logic;
		
  	-- output
  	y				: out std_logic_vector(7 downto 0)
	);
end ttl_74ls138_p;

architecture SYN of ttl_74ls138_p is

	signal g2a_n	: std_logic;
	signal g2b_n	: std_logic;
	signal y_n		: std_logic_vector(7 downto 0);
	
begin

	g2a_n <= not g2a;
	g2b_n <= not g2b;
	y <= not y_n;
	
	ttl_74ls138_inst : entity work.ttl_74ls138
		port map
		(
			a				=> a,
			b				=> b,
			c				=> c,
			g1			=> g1,
			g2a_n		=> g2a_n,
			g2b_n		=> g2b_n,
			y_n			=> y_n
		);
		
end SYN;

