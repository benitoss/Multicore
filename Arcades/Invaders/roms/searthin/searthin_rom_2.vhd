--
-- Multicore 2 / Multicore 2+
--
-- Copyright (c) 2017-2020 - Victor Trucco
--
-- All rights reserved
--
-- Redistribution and use in source and synthezised forms, with or without
-- modification, are permitted provided that the following conditions are met:
--
-- Redistributions of source code must retain the above copyright notice,
-- this list of conditions and the following disclaimer.
--
-- Redistributions in synthesized form must reproduce the above copyright
-- notice, this list of conditions and the following disclaimer in the
-- documentation and/or other materials provided with the distribution.
--
-- Neither the name of the author nor the names of other contributors may
-- be used to endorse or promote products derived from this software without
-- specific prior written permission.
--
-- THIS CODE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
-- AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
-- THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
-- PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE
-- LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
-- CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
-- SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
-- INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
-- CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
-- ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
-- POSSIBILITY OF SUCH DAMAGE.
--
-- You are responsible for any legal issues arising from your use of this code.
--
		
-- generated with romgen by MikeJ
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity SEARTHIN_ROM_2 is
  port (
    CLK         : in    std_logic;
    ADDR        : in    std_logic_vector(10 downto 0);
    DATA        : out   std_logic_vector(7 downto 0)
    );
end;

architecture RTL of SEARTHIN_ROM_2 is


  type ROM_ARRAY is array(0 to 2047) of std_logic_vector(7 downto 0);
  constant ROM : ROM_ARRAY := (
    x"AF",x"32",x"C1",x"20",x"CD",x"CF",x"01",x"3A", -- 0x0000
    x"67",x"20",x"0F",x"DA",x"72",x"08",x"CD",x"13", -- 0x0008
    x"02",x"CD",x"CF",x"01",x"CD",x"B1",x"00",x"CD", -- 0x0010
    x"D1",x"19",x"06",x"20",x"CD",x"FA",x"18",x"CD", -- 0x0018
    x"18",x"16",x"CD",x"0A",x"19",x"CD",x"F3",x"15", -- 0x0020
    x"CD",x"88",x"09",x"3A",x"82",x"20",x"A7",x"CA", -- 0x0028
    x"EF",x"09",x"CD",x"BB",x"13",x"CD",x"35",x"09", -- 0x0030
    x"CD",x"D8",x"08",x"CD",x"2C",x"17",x"CD",x"59", -- 0x0038
    x"0A",x"CA",x"49",x"08",x"06",x"04",x"CD",x"FA", -- 0x0040
    x"18",x"CD",x"75",x"17",x"D3",x"06",x"CD",x"04", -- 0x0048
    x"18",x"C3",x"1F",x"08",x"00",x"00",x"00",x"11", -- 0x0050
    x"BA",x"1A",x"CD",x"F3",x"08",x"06",x"98",x"DB", -- 0x0058
    x"01",x"0F",x"0F",x"DA",x"6D",x"08",x"0F",x"DA", -- 0x0060
    x"98",x"07",x"C3",x"7F",x"07",x"3E",x"01",x"C3", -- 0x0068
    x"9B",x"07",x"CD",x"1A",x"02",x"C3",x"14",x"08", -- 0x0070
    x"3A",x"08",x"20",x"47",x"2A",x"09",x"20",x"EB", -- 0x0078
    x"C3",x"86",x"08",x"00",x"00",x"00",x"3A",x"67", -- 0x0080
    x"20",x"67",x"2E",x"FC",x"C9",x"21",x"11",x"2B", -- 0x0088
    x"11",x"70",x"1B",x"0E",x"0E",x"CD",x"F3",x"08", -- 0x0090
    x"3A",x"67",x"20",x"0F",x"3E",x"1C",x"21",x"11", -- 0x0098
    x"37",x"D4",x"FF",x"08",x"3E",x"B0",x"32",x"C0", -- 0x00A0
    x"20",x"3A",x"C0",x"20",x"A7",x"C8",x"E6",x"04", -- 0x00A8
    x"C2",x"BC",x"08",x"CD",x"CA",x"09",x"CD",x"31", -- 0x00B0
    x"19",x"C3",x"BD",x"0B",x"06",x"30",x"21",x"1C", -- 0x00B8
    x"26",x"3A",x"67",x"20",x"0F",x"DA",x"CB",x"08", -- 0x00C0
    x"21",x"1C",x"38",x"CD",x"CB",x"14",x"C3",x"A9", -- 0x00C8
    x"08",x"DB",x"02",x"E6",x"01",x"C3",x"E7",x"17", -- 0x00D0
    x"3A",x"82",x"20",x"FE",x"0C",x"D0",x"3E",x"F9", -- 0x00D8
    x"32",x"7E",x"20",x"C9",x"3A",x"CE",x"20",x"A7", -- 0x00E0
    x"C0",x"21",x"1C",x"38",x"06",x"30",x"C3",x"CB", -- 0x00E8
    x"14",x"0E",x"03",x"1A",x"D5",x"CD",x"FF",x"08", -- 0x00F0
    x"D1",x"13",x"0D",x"C2",x"F3",x"08",x"C9",x"11", -- 0x00F8
    x"00",x"1E",x"E5",x"26",x"00",x"6F",x"29",x"29", -- 0x0100
    x"29",x"19",x"EB",x"E1",x"06",x"08",x"D3",x"06", -- 0x0108
    x"C3",x"39",x"14",x"3A",x"09",x"20",x"FE",x"78", -- 0x0110
    x"D0",x"2A",x"91",x"20",x"7D",x"B4",x"C2",x"29", -- 0x0118
    x"09",x"21",x"00",x"06",x"3E",x"01",x"32",x"83", -- 0x0120
    x"20",x"2B",x"22",x"91",x"20",x"C9",x"CD",x"11", -- 0x0128
    x"16",x"2E",x"FF",x"7E",x"C9",x"CD",x"10",x"19", -- 0x0130
    x"2B",x"2B",x"7E",x"A7",x"C8",x"06",x"50",x"DB", -- 0x0138
    x"02",x"07",x"00",x"D2",x"48",x"09",x"06",x"30", -- 0x0140
    x"CD",x"C1",x"13",x"00",x"00",x"B8",x"D8",x"CD", -- 0x0148
    x"2E",x"09",x"34",x"7E",x"F5",x"21",x"01",x"25", -- 0x0150
    x"24",x"24",x"3D",x"C2",x"58",x"09",x"06",x"10", -- 0x0158
    x"11",x"60",x"1C",x"CD",x"39",x"14",x"F1",x"3C", -- 0x0160
    x"CD",x"8B",x"1A",x"CD",x"10",x"19",x"2B",x"2B", -- 0x0168
    x"36",x"00",x"3E",x"FF",x"32",x"99",x"20",x"06", -- 0x0170
    x"10",x"C3",x"FA",x"18",x"21",x"A0",x"1D",x"FE", -- 0x0178
    x"02",x"D8",x"23",x"FE",x"04",x"D8",x"23",x"C9", -- 0x0180
    x"CD",x"CA",x"09",x"3A",x"F1",x"20",x"A7",x"C8", -- 0x0188
    x"AF",x"32",x"F1",x"20",x"E5",x"2A",x"F2",x"20", -- 0x0190
    x"EB",x"E1",x"7E",x"83",x"27",x"77",x"5F",x"23", -- 0x0198
    x"7E",x"8A",x"27",x"77",x"57",x"23",x"7E",x"23", -- 0x01A0
    x"66",x"6F",x"C3",x"AD",x"09",x"7A",x"CD",x"B2", -- 0x01A8
    x"09",x"7B",x"D5",x"F5",x"0F",x"0F",x"0F",x"0F", -- 0x01B0
    x"E6",x"0F",x"CD",x"C5",x"09",x"F1",x"E6",x"0F", -- 0x01B8
    x"CD",x"C5",x"09",x"D1",x"C9",x"C6",x"1A",x"C3", -- 0x01C0
    x"FF",x"08",x"3A",x"67",x"20",x"0F",x"21",x"F8", -- 0x01C8
    x"20",x"D8",x"21",x"FC",x"20",x"C9",x"21",x"02", -- 0x01D0
    x"24",x"36",x"00",x"23",x"7D",x"E6",x"1F",x"FE", -- 0x01D8
    x"1C",x"DA",x"E8",x"09",x"11",x"06",x"00",x"19", -- 0x01E0
    x"7C",x"FE",x"40",x"DA",x"D9",x"09",x"C9",x"CD", -- 0x01E8
    x"3C",x"0A",x"AF",x"32",x"E9",x"20",x"CD",x"D6", -- 0x01F0
    x"09",x"3A",x"67",x"20",x"F5",x"CD",x"E4",x"01", -- 0x01F8
    x"F1",x"32",x"67",x"20",x"3A",x"67",x"20",x"67", -- 0x0200
    x"E5",x"2E",x"FE",x"7E",x"3C",x"77",x"FE",x"01", -- 0x0208
    x"CC",x"B3",x"19",x"CD",x"15",x"18",x"00",x"00", -- 0x0210
    x"7E",x"E1",x"2E",x"FC",x"77",x"23",x"36",x"38", -- 0x0218
    x"7C",x"0F",x"DA",x"33",x"0A",x"3E",x"21",x"32", -- 0x0220
    x"98",x"20",x"CD",x"F5",x"01",x"CD",x"04",x"19", -- 0x0228
    x"C3",x"04",x"08",x"CD",x"EF",x"01",x"CD",x"C0", -- 0x0230
    x"01",x"C3",x"04",x"08",x"CD",x"59",x"0A",x"C2", -- 0x0238
    x"52",x"0A",x"3E",x"30",x"32",x"C0",x"20",x"3A", -- 0x0240
    x"C0",x"20",x"A7",x"C8",x"CD",x"59",x"0A",x"CA", -- 0x0248
    x"47",x"0A",x"CD",x"59",x"0A",x"C2",x"52",x"0A", -- 0x0250
    x"C9",x"3A",x"15",x"20",x"FE",x"FF",x"C9",x"3A", -- 0x0258
    x"EF",x"20",x"A7",x"CA",x"7C",x"0A",x"48",x"06", -- 0x0260
    x"08",x"CD",x"FA",x"18",x"41",x"78",x"CD",x"7C", -- 0x0268
    x"09",x"7E",x"21",x"F3",x"20",x"36",x"00",x"2B", -- 0x0270
    x"77",x"2B",x"36",x"01",x"21",x"62",x"20",x"C9", -- 0x0278
    x"3E",x"02",x"32",x"C1",x"20",x"D3",x"06",x"3A", -- 0x0280
    x"CB",x"20",x"A7",x"CA",x"85",x"0A",x"AF",x"32", -- 0x0288
    x"C1",x"20",x"C9",x"D5",x"1A",x"CD",x"FF",x"08", -- 0x0290
    x"D1",x"3E",x"07",x"32",x"C0",x"20",x"3A",x"C0", -- 0x0298
    x"20",x"3D",x"C2",x"9E",x"0A",x"13",x"0D",x"C2", -- 0x02A0
    x"93",x"0A",x"C9",x"21",x"50",x"20",x"C3",x"4B", -- 0x02A8
    x"02",x"3E",x"40",x"C3",x"86",x"19",x"3E",x"80", -- 0x02B0
    x"C3",x"D7",x"0A",x"E1",x"C3",x"72",x"00",x"3A", -- 0x02B8
    x"C1",x"20",x"0F",x"DA",x"BB",x"0A",x"0F",x"DA", -- 0x02C0
    x"68",x"18",x"0F",x"DA",x"AB",x"0A",x"C9",x"00", -- 0x02C8
    x"00",x"00",x"00",x"00",x"00",x"00",x"00",x"32", -- 0x02D0
    x"C0",x"20",x"3A",x"C0",x"20",x"A7",x"C2",x"DA", -- 0x02D8
    x"0A",x"C9",x"21",x"C2",x"20",x"06",x"0C",x"C3", -- 0x02E0
    x"32",x"1A",x"AF",x"D3",x"03",x"D3",x"05",x"CD", -- 0x02E8
    x"82",x"19",x"FB",x"CD",x"B1",x"0A",x"3A",x"EC", -- 0x02F0
    x"20",x"A7",x"21",x"17",x"30",x"0E",x"04",x"C2", -- 0x02F8
    x"E8",x"0B",x"11",x"FA",x"1C",x"CD",x"93",x"0A", -- 0x0300
    x"CD",x"B1",x"0A",x"CD",x"7F",x"0C",x"3A",x"90", -- 0x0308
    x"1F",x"CD",x"64",x"0E",x"00",x"00",x"00",x"3A", -- 0x0310
    x"EC",x"20",x"A7",x"C2",x"4A",x"0B",x"11",x"95", -- 0x0318
    x"1A",x"CD",x"E2",x"0A",x"CD",x"80",x"0A",x"11", -- 0x0320
    x"B0",x"1B",x"CD",x"E2",x"0A",x"CD",x"80",x"0A", -- 0x0328
    x"CD",x"B1",x"0A",x"11",x"C9",x"1F",x"CD",x"E2", -- 0x0330
    x"0A",x"CD",x"80",x"0A",x"CD",x"B1",x"0A",x"21", -- 0x0338
    x"B7",x"33",x"06",x"0A",x"CD",x"CB",x"14",x"CD", -- 0x0340
    x"B6",x"0A",x"CD",x"DD",x"0E",x"3A",x"FF",x"21", -- 0x0348
    x"A7",x"C2",x"5D",x"0B",x"CD",x"D1",x"08",x"32", -- 0x0350
    x"FF",x"21",x"CD",x"7F",x"1A",x"CD",x"E4",x"01", -- 0x0358
    x"CD",x"C0",x"01",x"CD",x"EF",x"01",x"CD",x"1A", -- 0x0360
    x"02",x"3E",x"01",x"32",x"C1",x"20",x"CD",x"CF", -- 0x0368
    x"01",x"CD",x"18",x"16",x"CD",x"F1",x"0B",x"D3", -- 0x0370
    x"06",x"CD",x"59",x"0A",x"CA",x"71",x"0B",x"AF", -- 0x0378
    x"32",x"25",x"20",x"CD",x"59",x"0A",x"C2",x"83", -- 0x0380
    x"0B",x"AF",x"32",x"C1",x"20",x"CD",x"B1",x"0A", -- 0x0388
    x"CD",x"D6",x"09",x"0E",x"0C",x"21",x"11",x"2C", -- 0x0390
    x"11",x"90",x"1F",x"CD",x"F3",x"08",x"3A",x"EC", -- 0x0398
    x"20",x"FE",x"00",x"C2",x"AE",x"0B",x"21",x"11", -- 0x03A0
    x"33",x"3E",x"02",x"CD",x"FF",x"08",x"CD",x"2C", -- 0x03A8
    x"0C",x"CD",x"50",x"1F",x"C3",x"C6",x"0B",x"C5", -- 0x03B0
    x"CD",x"F3",x"08",x"C1",x"C9",x"CD",x"63",x"0C", -- 0x03B8
    x"CD",x"E4",x"08",x"C3",x"A9",x"08",x"3A",x"EC", -- 0x03C0
    x"20",x"FE",x"00",x"C2",x"DA",x"0B",x"11",x"D5", -- 0x03C8
    x"1F",x"CD",x"E2",x"0A",x"CD",x"80",x"0A",x"CD", -- 0x03D0
    x"9E",x"18",x"21",x"EC",x"20",x"7E",x"3C",x"E6", -- 0x03D8
    x"01",x"77",x"CD",x"D6",x"09",x"C3",x"34",x"0F", -- 0x03E0
    x"11",x"AB",x"1D",x"CD",x"93",x"0A",x"C3",x"08", -- 0x03E8
    x"0B",x"CD",x"0A",x"19",x"C3",x"9A",x"19",x"3E", -- 0x03F0
    x"C0",x"C3",x"D7",x"0A",x"01",x"18",x"3A",x"EB", -- 0x03F8
    x"20",x"FE",x"99",x"CA",x"3E",x"00",x"F5",x"DB", -- 0x0400
    x"02",x"07",x"D2",x"14",x"0C",x"F1",x"C6",x"01", -- 0x0408
    x"27",x"C3",x"38",x"00",x"3A",x"01",x"3E",x"FE", -- 0x0410
    x"07",x"C2",x"23",x"0C",x"AF",x"32",x"01",x"3E", -- 0x0418
    x"C3",x"0D",x"0C",x"3E",x"07",x"32",x"01",x"3E", -- 0x0420
    x"F1",x"C3",x"3E",x"00",x"DB",x"02",x"07",x"D2", -- 0x0428
    x"38",x"0C",x"11",x"6E",x"0F",x"C3",x"3B",x"0C", -- 0x0430
    x"11",x"7E",x"0F",x"21",x"0F",x"2A",x"0E",x"10", -- 0x0438
    x"CD",x"F3",x"08",x"DB",x"02",x"0F",x"0F",x"D8", -- 0x0440
    x"21",x"0D",x"28",x"11",x"BA",x"10",x"0E",x"14", -- 0x0448
    x"CD",x"F3",x"08",x"DB",x"02",x"07",x"D0",x"21", -- 0x0450
    x"0D",x"33",x"11",x"F8",x"1E",x"06",x"08",x"C3", -- 0x0458
    x"39",x"14",x"98",x"CD",x"1A",x"19",x"21",x"1C", -- 0x0460
    x"3D",x"CD",x"75",x"0C",x"21",x"1C",x"34",x"CD", -- 0x0468
    x"75",x"0C",x"21",x"1C",x"2B",x"11",x"D0",x"1E", -- 0x0470
    x"06",x"08",x"D3",x"06",x"C3",x"39",x"14",x"11", -- 0x0478
    x"F8",x"0C",x"21",x"15",x"2E",x"06",x"40",x"CD", -- 0x0480
    x"39",x"14",x"D3",x"06",x"11",x"38",x"0D",x"21", -- 0x0488
    x"14",x"2E",x"06",x"40",x"CD",x"39",x"14",x"CD", -- 0x0490
    x"B1",x"0A",x"11",x"78",x"0D",x"21",x"12",x"2E", -- 0x0498
    x"06",x"40",x"CD",x"39",x"14",x"11",x"B8",x"0D", -- 0x04A0
    x"21",x"11",x"2E",x"06",x"40",x"CD",x"39",x"14", -- 0x04A8
    x"CD",x"B1",x"0A",x"11",x"FA",x"0D",x"21",x"0F", -- 0x04B0
    x"2B",x"06",x"6A",x"CD",x"39",x"14",x"D3",x"06", -- 0x04B8
    x"11",x"73",x"0E",x"21",x"0E",x"2B",x"06",x"6A", -- 0x04C0
    x"CD",x"39",x"14",x"CD",x"B1",x"0A",x"11",x"F3", -- 0x04C8
    x"1F",x"21",x"0B",x"30",x"0E",x"04",x"CD",x"93", -- 0x04D0
    x"0A",x"21",x"09",x"29",x"11",x"E1",x"1F",x"0E", -- 0x04D8
    x"12",x"CD",x"93",x"0A",x"C3",x"B6",x"0A",x"18", -- 0x04E0
    x"26",x"D8",x"0F",x"13",x"26",x"F0",x"0F",x"0E", -- 0x04E8
    x"26",x"08",x"10",x"09",x"26",x"20",x"10",x"FF", -- 0x04F0
    x"00",x"3C",x"7E",x"C3",x"C3",x"C3",x"C3",x"C3", -- 0x04F8
    x"C3",x"71",x"30",x"00",x"00",x"00",x"FF",x"FF", -- 0x0500
    x"00",x"00",x"00",x"00",x"00",x"00",x"FF",x"FF", -- 0x0508
    x"00",x"00",x"00",x"FF",x"FF",x"C3",x"C3",x"C3", -- 0x0510
    x"C3",x"C3",x"C3",x"7E",x"3C",x"00",x"00",x"00", -- 0x0518
    x"FF",x"FF",x"C3",x"C3",x"C3",x"C3",x"C3",x"C3", -- 0x0520
    x"C0",x"C0",x"00",x"00",x"00",x"FF",x"FF",x"C3", -- 0x0528
    x"C3",x"C3",x"C3",x"C3",x"C3",x"7E",x"3C",x"00", -- 0x0530
    x"00",x"30",x"38",x"0C",x"0C",x"0C",x"0C",x"0C", -- 0x0538
    x"0C",x"F8",x"F0",x"00",x"00",x"00",x"F0",x"F8", -- 0x0540
    x"1C",x"0C",x"0C",x"0C",x"0C",x"1C",x"F8",x"F0", -- 0x0548
    x"00",x"00",x"00",x"FC",x"FC",x"00",x"00",x"00", -- 0x0550
    x"00",x"00",x"00",x"00",x"00",x"00",x"00",x"00", -- 0x0558
    x"FC",x"FC",x"0C",x"0C",x"0C",x"0C",x"0C",x"0C", -- 0x0560
    x"0C",x"0C",x"00",x"00",x"00",x"FC",x"FC",x"00", -- 0x0568
    x"00",x"C0",x"C0",x"30",x"30",x"0C",x"0C",x"00", -- 0x0570
    x"00",x"FF",x"FF",x"C3",x"C3",x"C3",x"C3",x"C3", -- 0x0578
    x"C3",x"C0",x"C0",x"00",x"00",x"00",x"07",x"1F", -- 0x0580
    x"38",x"70",x"C0",x"C0",x"70",x"38",x"1F",x"07", -- 0x0588
    x"00",x"00",x"00",x"FF",x"FF",x"C3",x"C3",x"C3", -- 0x0590
    x"C3",x"C3",x"C3",x"7E",x"3C",x"00",x"00",x"00", -- 0x0598
    x"C0",x"C0",x"C0",x"C0",x"FF",x"FF",x"C0",x"C0", -- 0x05A0
    x"C0",x"C0",x"00",x"00",x"00",x"FF",x"FF",x"03", -- 0x05A8
    x"03",x"03",x"03",x"03",x"03",x"FF",x"FF",x"00", -- 0x05B0
    x"00",x"FC",x"FC",x"0C",x"0C",x"0C",x"0C",x"0C", -- 0x05B8
    x"0C",x"0C",x"0C",x"00",x"00",x"00",x"FC",x"FC", -- 0x05C0
    x"C0",x"C0",x"C0",x"C0",x"C0",x"C0",x"FC",x"FC", -- 0x05C8
    x"00",x"00",x"00",x"FC",x"FC",x"00",x"00",x"C0", -- 0x05D0
    x"C0",x"30",x"30",x"0C",x"0C",x"00",x"00",x"00", -- 0x05D8
    x"00",x"00",x"00",x"00",x"FC",x"FC",x"00",x"00", -- 0x05E0
    x"00",x"00",x"00",x"00",x"00",x"FC",x"FC",x"00", -- 0x05E8
    x"00",x"00",x"00",x"00",x"00",x"FC",x"FC",x"00", -- 0x05F0
    x"5E",x"0F",x"00",x"00",x"00",x"00",x"00",x"C0", -- 0x05F8
    x"C0",x"C0",x"C0",x"FF",x"FF",x"C0",x"C0",x"C0", -- 0x0600
    x"C0",x"00",x"00",x"00",x"FF",x"FF",x"0C",x"0C", -- 0x0608
    x"03",x"03",x"00",x"00",x"FF",x"FF",x"00",x"00", -- 0x0610
    x"00",x"FF",x"FF",x"00",x"00",x"00",x"00",x"00", -- 0x0618
    x"00",x"FF",x"FF",x"00",x"00",x"00",x"07",x"1F", -- 0x0620
    x"38",x"70",x"C0",x"C0",x"70",x"38",x"1F",x"07", -- 0x0628
    x"00",x"00",x"00",x"3C",x"7E",x"C3",x"C3",x"C3", -- 0x0630
    x"C3",x"C3",x"C3",x"71",x"30",x"00",x"00",x"00", -- 0x0638
    x"C0",x"C0",x"C0",x"C0",x"FF",x"FF",x"C0",x"C0", -- 0x0640
    x"C0",x"C0",x"00",x"00",x"00",x"3F",x"7F",x"C0", -- 0x0648
    x"C0",x"C0",x"C0",x"C0",x"C0",x"7F",x"3F",x"00", -- 0x0650
    x"00",x"00",x"FF",x"FF",x"0C",x"0C",x"03",x"03", -- 0x0658
    x"00",x"00",x"FF",x"FF",x"1E",x"0A",x"2A",x"F8", -- 0x0660
    x"0D",x"4E",x"1D",x"CA",x"00",x"18",x"81",x"23", -- 0x0668
    x"C3",x"69",x"0E",x"00",x"00",x"00",x"00",x"00", -- 0x0670
    x"0C",x"0C",x"0C",x"0C",x"FC",x"FC",x"0C",x"0C", -- 0x0678
    x"0C",x"0C",x"00",x"00",x"00",x"FC",x"FC",x"00", -- 0x0680
    x"00",x"00",x"00",x"C0",x"C0",x"FC",x"FC",x"00", -- 0x0688
    x"00",x"00",x"C0",x"C0",x"30",x"30",x"0C",x"0C", -- 0x0690
    x"30",x"30",x"C0",x"C0",x"00",x"00",x"00",x"FC", -- 0x0698
    x"FC",x"C0",x"C0",x"C0",x"C0",x"C0",x"C0",x"FC", -- 0x06A0
    x"FC",x"00",x"00",x"00",x"30",x"38",x"0C",x"0C", -- 0x06A8
    x"0C",x"0C",x"0C",x"0C",x"F8",x"F0",x"00",x"00", -- 0x06B0
    x"00",x"0C",x"0C",x"0C",x"0C",x"FC",x"FC",x"0C", -- 0x06B8
    x"0C",x"0C",x"0C",x"00",x"00",x"00",x"F0",x"F8", -- 0x06C0
    x"0C",x"0C",x"0C",x"0C",x"0C",x"0C",x"F8",x"F0", -- 0x06C8
    x"00",x"00",x"00",x"FC",x"FC",x"00",x"00",x"00", -- 0x06D0
    x"00",x"C0",x"C0",x"FC",x"FC",x"CD",x"D6",x"09", -- 0x06D8
    x"21",x"16",x"28",x"0E",x"14",x"11",x"8E",x"0F", -- 0x06E0
    x"CD",x"56",x"0F",x"0E",x"12",x"21",x"14",x"29", -- 0x06E8
    x"CD",x"56",x"0F",x"21",x"12",x"29",x"CD",x"56", -- 0x06F0
    x"0F",x"21",x"10",x"29",x"CD",x"56",x"0F",x"21", -- 0x06F8
    x"0D",x"29",x"11",x"38",x"10",x"0E",x"12",x"CD", -- 0x0700
    x"B7",x"0B",x"21",x"0B",x"29",x"CD",x"B7",x"0B", -- 0x0708
    x"21",x"09",x"29",x"CD",x"B7",x"0B",x"21",x"07", -- 0x0710
    x"29",x"CD",x"B7",x"0B",x"21",x"05",x"29",x"CD", -- 0x0718
    x"B7",x"0B",x"21",x"05",x"2E",x"06",x"08",x"DB", -- 0x0720
    x"02",x"07",x"00",x"D2",x"9C",x"1F",x"CD",x"F7", -- 0x0728
    x"0B",x"C3",x"D6",x"09",x"01",x"27",x"18",x"CD", -- 0x0730
    x"56",x"18",x"DA",x"43",x"0F",x"CD",x"44",x"18", -- 0x0738
    x"C3",x"37",x"0F",x"01",x"E7",x"0C",x"CD",x"56", -- 0x0740
    x"18",x"DA",x"47",x"14",x"C5",x"0E",x"18",x"CD", -- 0x0748
    x"93",x"0A",x"C1",x"C3",x"46",x"0F",x"C5",x"CD", -- 0x0750
    x"93",x"0A",x"C1",x"C9",x"02",x"0E",x"0C",x"0F", -- 0x0758
    x"04",x"13",x"08",x"13",x"08",x"15",x"04",x"26", -- 0x0760
    x"26",x"15",x"08",x"03",x"04",x"0E",x"1B",x"26", -- 0x0768
    x"02",x"0E",x"08",x"0D",x"26",x"26",x"1B",x"26", -- 0x0770
    x"02",x"11",x"04",x"03",x"08",x"13",x"1C",x"02", -- 0x0778
    x"0E",x"08",x"0D",x"12",x"26",x"27",x"26",x"1B", -- 0x0780
    x"02",x"11",x"04",x"03",x"08",x"13",x"13",x"00", -- 0x0788
    x"0A",x"04",x"26",x"0E",x"15",x"04",x"11",x"26", -- 0x0790
    x"02",x"0E",x"0C",x"0C",x"00",x"0D",x"03",x"26", -- 0x0798
    x"0E",x"05",x"04",x"00",x"11",x"13",x"07",x"26", -- 0x07A0
    x"01",x"00",x"12",x"04",x"26",x"00",x"0D",x"03", -- 0x07A8
    x"26",x"14",x"12",x"04",x"18",x"0E",x"14",x"11", -- 0x07B0
    x"26",x"12",x"0A",x"08",x"0B",x"0B",x"26",x"13", -- 0x07B8
    x"0E",x"26",x"12",x"13",x"0E",x"0F",x"13",x"07", -- 0x07C0
    x"04",x"26",x"04",x"00",x"11",x"13",x"07",x"26", -- 0x07C8
    x"08",x"0D",x"15",x"00",x"12",x"08",x"0E",x"0D", -- 0x07D0
    x"26",x"26",x"12",x"02",x"0E",x"11",x"04",x"26", -- 0x07D8
    x"1B",x"1A",x"1A",x"26",x"0F",x"0E",x"08",x"0D", -- 0x07E0
    x"13",x"12",x"26",x"05",x"0E",x"11",x"26",x"26", -- 0x07E8
    x"26",x"26",x"12",x"02",x"0E",x"11",x"04",x"26", -- 0x07F0
    x"1C",x"1A",x"1A",x"26",x"0F",x"0E",x"08",x"0D"  -- 0x07F8
  );

begin

  p_rom : process
  begin
    wait until rising_edge(CLK);
     DATA <= ROM(to_integer(unsigned(ADDR)));
  end process;
end RTL;
