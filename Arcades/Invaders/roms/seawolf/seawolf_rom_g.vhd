--
-- Multicore 2 / Multicore 2+
--
-- Copyright (c) 2017-2020 - Victor Trucco
--
-- All rights reserved
--
-- Redistribution and use in source and synthezised forms, with or without
-- modification, are permitted provided that the following conditions are met:
--
-- Redistributions of source code must retain the above copyright notice,
-- this list of conditions and the following disclaimer.
--
-- Redistributions in synthesized form must reproduce the above copyright
-- notice, this list of conditions and the following disclaimer in the
-- documentation and/or other materials provided with the distribution.
--
-- Neither the name of the author nor the names of other contributors may
-- be used to endorse or promote products derived from this software without
-- specific prior written permission.
--
-- THIS CODE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
-- AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
-- THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
-- PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE
-- LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
-- CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
-- SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
-- INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
-- CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
-- ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
-- POSSIBILITY OF SUCH DAMAGE.
--
-- You are responsible for any legal issues arising from your use of this code.
--
		
-- generated with romgen by MikeJ
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity SEAWOLF_ROM_G is
  port (
    CLK         : in    std_logic;
    ADDR        : in    std_logic_vector(10 downto 0);
    DATA        : out   std_logic_vector(7 downto 0)
    );
end;

architecture RTL of SEAWOLF_ROM_G is


  type ROM_ARRAY is array(0 to 2047) of std_logic_vector(7 downto 0);
  constant ROM : ROM_ARRAY := (
    x"29",x"04",x"19",x"BE",x"3E",x"40",x"CA",x"0E", -- 0x0000
    x"04",x"21",x"32",x"04",x"19",x"7E",x"21",x"E9", -- 0x0008
    x"21",x"19",x"77",x"E1",x"13",x"0C",x"0C",x"3E", -- 0x0010
    x"12",x"B9",x"C2",x"F4",x"03",x"21",x"E9",x"21", -- 0x0018
    x"11",x"08",x"30",x"3E",x"08",x"CD",x"30",x"0B", -- 0x0020
    x"76",x"8D",x"79",x"00",x"1F",x"58",x"6D",x"EA", -- 0x0028
    x"C5",x"2A",x"48",x"48",x"47",x"47",x"46",x"46", -- 0x0030
    x"45",x"45",x"CD",x"A2",x"08",x"DB",x"02",x"E6", -- 0x0038
    x"E0",x"FE",x"E0",x"CC",x"EC",x"03",x"21",x"02", -- 0x0040
    x"20",x"3E",x"09",x"06",x"00",x"70",x"23",x"3D", -- 0x0048
    x"C2",x"4D",x"04",x"21",x"29",x"09",x"22",x"00", -- 0x0050
    x"20",x"FB",x"21",x"59",x"04",x"E5",x"2A",x"00", -- 0x0058
    x"20",x"7E",x"A7",x"C2",x"7D",x"04",x"CD",x"A4", -- 0x0060
    x"06",x"CD",x"CE",x"04",x"CD",x"BF",x"04",x"3A", -- 0x0068
    x"02",x"20",x"A7",x"C8",x"CD",x"4C",x"07",x"CD", -- 0x0070
    x"B8",x"08",x"C3",x"8C",x"04",x"23",x"EB",x"21", -- 0x0078
    x"E8",x"09",x"07",x"4F",x"06",x"00",x"09",x"7E", -- 0x0080
    x"23",x"66",x"6F",x"E9",x"3A",x"03",x"20",x"FE", -- 0x0088
    x"1D",x"F8",x"01",x"02",x"20",x"11",x"E9",x"21", -- 0x0090
    x"CD",x"82",x"0A",x"EB",x"CD",x"7A",x"0A",x"23", -- 0x0098
    x"36",x"2C",x"23",x"EB",x"01",x"2B",x"20",x"CD", -- 0x00A0
    x"82",x"0A",x"EB",x"CD",x"7A",x"0A",x"23",x"36", -- 0x00A8
    x"30",x"23",x"36",x"30",x"21",x"E9",x"21",x"11", -- 0x00B0
    x"2F",x"3E",x"3E",x"06",x"C3",x"30",x"0B",x"21", -- 0x00B8
    x"2A",x"20",x"7E",x"A7",x"C8",x"36",x"00",x"21", -- 0x00C0
    x"A6",x"09",x"22",x"00",x"20",x"C9",x"21",x"20", -- 0x00C8
    x"20",x"7E",x"A7",x"C8",x"36",x"00",x"1F",x"DC", -- 0x00D0
    x"01",x"06",x"1F",x"DC",x"0E",x"06",x"1F",x"DC", -- 0x00D8
    x"F7",x"04",x"1F",x"DC",x"34",x"06",x"1F",x"DC", -- 0x00E0
    x"E9",x"05",x"1F",x"DC",x"73",x"05",x"1F",x"DC", -- 0x00E8
    x"6C",x"05",x"1F",x"DC",x"11",x"05",x"C9",x"F5", -- 0x00F0
    x"21",x"26",x"20",x"7E",x"A7",x"CA",x"0F",x"05", -- 0x00F8
    x"35",x"3E",x"04",x"D3",x"05",x"3E",x"19",x"32", -- 0x0100
    x"23",x"20",x"3E",x"0F",x"32",x"25",x"20",x"F1", -- 0x0108
    x"C9",x"21",x"2E",x"20",x"7E",x"A7",x"C2",x"3D", -- 0x0110
    x"05",x"36",x"01",x"3A",x"07",x"20",x"0F",x"E6", -- 0x0118
    x"70",x"CA",x"3D",x"05",x"C6",x"09",x"21",x"2B", -- 0x0120
    x"20",x"BE",x"D2",x"3D",x"05",x"3E",x"20",x"32", -- 0x0128
    x"02",x"20",x"21",x"33",x"0F",x"11",x"03",x"3C", -- 0x0130
    x"3E",x"0C",x"C3",x"30",x"0B",x"21",x"C9",x"20", -- 0x0138
    x"01",x"1E",x"00",x"09",x"7D",x"FE",x"5F",x"CA", -- 0x0140
    x"5C",x"05",x"7E",x"A7",x"F2",x"43",x"05",x"AF", -- 0x0148
    x"32",x"21",x"20",x"32",x"2D",x"20",x"3E",x"01", -- 0x0150
    x"32",x"02",x"20",x"C9",x"21",x"29",x"09",x"22", -- 0x0158
    x"00",x"20",x"3A",x"2B",x"20",x"21",x"06",x"20", -- 0x0160
    x"BE",x"D8",x"77",x"C9",x"21",x"63",x"09",x"22", -- 0x0168
    x"00",x"20",x"C9",x"2A",x"00",x"20",x"23",x"22", -- 0x0170
    x"00",x"20",x"C9",x"C8",x"3A",x"02",x"20",x"A7", -- 0x0178
    x"C8",x"3A",x"21",x"20",x"A7",x"C0",x"21",x"2D", -- 0x0180
    x"20",x"7E",x"E6",x"1F",x"C8",x"7E",x"E6",x"0F", -- 0x0188
    x"1F",x"06",x"20",x"A7",x"CA",x"99",x"05",x"06", -- 0x0190
    x"10",x"B0",x"77",x"D3",x"02",x"21",x"21",x"20", -- 0x0198
    x"36",x"08",x"E6",x"10",x"C2",x"A9",x"05",x"36", -- 0x01A0
    x"3C",x"3E",x"02",x"D3",x"05",x"3E",x"0F",x"32", -- 0x01A8
    x"25",x"20",x"21",x"C9",x"20",x"11",x"1E",x"00", -- 0x01B0
    x"19",x"7E",x"A7",x"FA",x"B8",x"05",x"11",x"08", -- 0x01B8
    x"00",x"19",x"36",x"0E",x"2B",x"36",x"75",x"2B", -- 0x01C0
    x"36",x"9C",x"2B",x"36",x"E0",x"2B",x"36",x"FA", -- 0x01C8
    x"2B",x"2B",x"11",x"5E",x"0F",x"EB",x"3A",x"08", -- 0x01D0
    x"20",x"E6",x"1F",x"4F",x"06",x"00",x"09",x"7E", -- 0x01D8
    x"EB",x"77",x"2B",x"36",x"00",x"2B",x"36",x"C0", -- 0x01E0
    x"C9",x"F5",x"21",x"2D",x"20",x"7E",x"E6",x"10", -- 0x01E8
    x"C2",x"FF",x"05",x"3E",x"1F",x"D3",x"02",x"77", -- 0x01F0
    x"3E",x"08",x"D3",x"05",x"CD",x"EA",x"07",x"F1", -- 0x01F8
    x"C9",x"F5",x"AF",x"D3",x"05",x"D3",x"01",x"3A", -- 0x0200
    x"2D",x"20",x"D3",x"02",x"F1",x"C9",x"F5",x"21", -- 0x0208
    x"F0",x"21",x"7E",x"A7",x"CA",x"32",x"06",x"36", -- 0x0210
    x"00",x"23",x"57",x"5E",x"36",x"00",x"23",x"FE", -- 0x0218
    x"2C",x"01",x"03",x"0A",x"DA",x"2A",x"06",x"01", -- 0x0220
    x"05",x"20",x"EB",x"CD",x"3F",x"0A",x"EB",x"C3", -- 0x0228
    x"12",x"06",x"F1",x"C9",x"F5",x"3A",x"03",x"20", -- 0x0230
    x"E6",x"0F",x"F6",x"50",x"32",x"22",x"20",x"01", -- 0x0238
    x"29",x"20",x"0A",x"3C",x"FE",x"07",x"C2",x"4A", -- 0x0240
    x"06",x"AF",x"02",x"21",x"DE",x"0F",x"85",x"6F", -- 0x0248
    x"7E",x"47",x"FE",x"06",x"C2",x"6B",x"06",x"3E", -- 0x0250
    x"04",x"D3",x"05",x"3E",x"19",x"32",x"23",x"20", -- 0x0258
    x"3E",x"02",x"32",x"26",x"20",x"3E",x"0F",x"32", -- 0x0260
    x"25",x"20",x"78",x"21",x"2C",x"20",x"11",x"0D", -- 0x0268
    x"00",x"19",x"3D",x"C2",x"71",x"06",x"78",x"EB", -- 0x0270
    x"21",x"1E",x"20",x"7E",x"34",x"21",x"7E",x"0F", -- 0x0278
    x"1F",x"D2",x"8B",x"06",x"21",x"AE",x"0F",x"78", -- 0x0280
    x"F6",x"10",x"47",x"78",x"3D",x"07",x"07",x"07", -- 0x0288
    x"E6",x"38",x"85",x"6F",x"0E",x"08",x"7E",x"23", -- 0x0290
    x"12",x"1B",x"0D",x"C2",x"96",x"06",x"78",x"F6", -- 0x0298
    x"C0",x"12",x"F1",x"C9",x"21",x"C1",x"21",x"7E", -- 0x02A0
    x"A7",x"C8",x"36",x"00",x"23",x"56",x"E5",x"21", -- 0x02A8
    x"24",x"20",x"01",x"0D",x"00",x"09",x"3D",x"C2", -- 0x02B0
    x"B5",x"06",x"01",x"08",x"00",x"09",x"36",x"0E", -- 0x02B8
    x"2B",x"36",x"55",x"2B",x"2B",x"2B",x"36",x"01", -- 0x02C0
    x"2B",x"2B",x"72",x"2B",x"36",x"00",x"2B",x"46", -- 0x02C8
    x"36",x"E0",x"3A",x"02",x"20",x"A7",x"C2",x"DB", -- 0x02D0
    x"06",x"E1",x"C9",x"78",x"01",x"57",x"0F",x"E6", -- 0x02D8
    x"07",x"81",x"4F",x"11",x"E9",x"21",x"CD",x"82", -- 0x02E0
    x"0A",x"3E",x"30",x"12",x"13",x"12",x"0A",x"21", -- 0x02E8
    x"2B",x"20",x"86",x"27",x"77",x"E1",x"4E",x"23", -- 0x02F0
    x"46",x"23",x"E5",x"78",x"C6",x"20",x"21",x"C2", -- 0x02F8
    x"09",x"DA",x"07",x"07",x"21",x"BA",x"09",x"79", -- 0x0300
    x"07",x"07",x"07",x"E6",x"07",x"85",x"6F",x"7E", -- 0x0308
    x"D3",x"01",x"3E",x"01",x"D3",x"05",x"3E",x"1E", -- 0x0310
    x"32",x"25",x"20",x"78",x"16",x"24",x"C6",x"20", -- 0x0318
    x"FA",x"25",x"07",x"16",x"28",x"79",x"0F",x"0F", -- 0x0320
    x"0F",x"E6",x"1F",x"CA",x"2F",x"07",x"3D",x"FE", -- 0x0328
    x"1E",x"C2",x"35",x"07",x"3D",x"F6",x"A0",x"5F", -- 0x0330
    x"CD",x"DB",x"07",x"3E",x"2D",x"32",x"24",x"20", -- 0x0338
    x"21",x"EA",x"21",x"3E",x"03",x"CD",x"30",x"0B", -- 0x0340
    x"E1",x"C3",x"A7",x"06",x"21",x"A3",x"21",x"7E", -- 0x0348
    x"A7",x"C8",x"23",x"C6",x"10",x"07",x"07",x"07", -- 0x0350
    x"E6",x"07",x"11",x"67",x"20",x"01",x"0D",x"00", -- 0x0358
    x"EB",x"09",x"09",x"3D",x"C2",x"61",x"07",x"1A", -- 0x0360
    x"D6",x"08",x"96",x"FE",x"EC",x"D2",x"71",x"07", -- 0x0368
    x"09",x"2B",x"2B",x"36",x"00",x"EB",x"2B",x"7E", -- 0x0370
    x"C6",x"30",x"E6",x"F0",x"57",x"36",x"00",x"23", -- 0x0378
    x"5E",x"23",x"E5",x"CD",x"00",x"0A",x"7B",x"E6", -- 0x0380
    x"1F",x"CA",x"96",x"07",x"3D",x"CA",x"96",x"07", -- 0x0388
    x"3D",x"FE",x"1C",x"F2",x"90",x"07",x"5F",x"CD", -- 0x0390
    x"DB",x"07",x"42",x"04",x"04",x"4B",x"0C",x"C5", -- 0x0398
    x"7B",x"C6",x"60",x"5F",x"D5",x"42",x"0C",x"C5", -- 0x03A0
    x"3E",x"1E",x"32",x"25",x"20",x"3E",x"0F",x"32", -- 0x03A8
    x"24",x"20",x"3E",x"10",x"D3",x"05",x"7B",x"E6", -- 0x03B0
    x"02",x"21",x"40",x"0F",x"85",x"6F",x"5E",x"23", -- 0x03B8
    x"56",x"EB",x"D1",x"7E",x"23",x"CD",x"30",x"0B", -- 0x03C0
    x"D1",x"7E",x"23",x"CD",x"30",x"0B",x"D1",x"21", -- 0x03C8
    x"B5",x"0E",x"3E",x"03",x"CD",x"30",x"0B",x"E1", -- 0x03D0
    x"C3",x"4F",x"07",x"21",x"F0",x"21",x"7E",x"23", -- 0x03D8
    x"B6",x"23",x"C2",x"DE",x"07",x"2B",x"73",x"2B", -- 0x03E0
    x"72",x"C9",x"3A",x"2B",x"20",x"FE",x"40",x"DA", -- 0x03E8
    x"F4",x"07",x"3E",x"39",x"32",x"2C",x"20",x"21", -- 0x03F0
    x"7F",x"20",x"11",x"50",x"50",x"7E",x"A7",x"FA", -- 0x03F8
    x"00",x"00",x"00",x"00",x"00",x"00",x"00",x"00", -- 0x0400
    x"00",x"00",x"00",x"00",x"00",x"00",x"00",x"00", -- 0x0408
    x"00",x"00",x"00",x"00",x"00",x"00",x"00",x"00", -- 0x0410
    x"00",x"00",x"00",x"00",x"00",x"00",x"00",x"00", -- 0x0418
    x"00",x"00",x"00",x"00",x"00",x"00",x"00",x"00", -- 0x0420
    x"00",x"00",x"00",x"00",x"00",x"00",x"00",x"00", -- 0x0428
    x"00",x"00",x"00",x"00",x"00",x"00",x"00",x"00", -- 0x0430
    x"00",x"00",x"00",x"00",x"00",x"00",x"00",x"00", -- 0x0438
    x"00",x"00",x"00",x"00",x"00",x"00",x"00",x"00", -- 0x0440
    x"00",x"00",x"00",x"00",x"00",x"00",x"00",x"00", -- 0x0448
    x"00",x"00",x"00",x"00",x"00",x"00",x"00",x"00", -- 0x0450
    x"00",x"00",x"00",x"00",x"00",x"00",x"00",x"00", -- 0x0458
    x"00",x"00",x"00",x"00",x"00",x"00",x"00",x"00", -- 0x0460
    x"00",x"00",x"00",x"00",x"00",x"00",x"00",x"00", -- 0x0468
    x"00",x"00",x"00",x"00",x"00",x"00",x"00",x"00", -- 0x0470
    x"00",x"00",x"00",x"00",x"00",x"00",x"00",x"00", -- 0x0478
    x"00",x"00",x"00",x"00",x"00",x"00",x"00",x"00", -- 0x0480
    x"00",x"00",x"00",x"00",x"00",x"00",x"00",x"00", -- 0x0488
    x"00",x"00",x"00",x"00",x"00",x"00",x"00",x"00", -- 0x0490
    x"00",x"00",x"00",x"00",x"00",x"00",x"00",x"00", -- 0x0498
    x"00",x"00",x"00",x"00",x"00",x"00",x"00",x"00", -- 0x04A0
    x"00",x"00",x"00",x"00",x"00",x"00",x"00",x"00", -- 0x04A8
    x"00",x"00",x"00",x"00",x"00",x"00",x"00",x"00", -- 0x04B0
    x"00",x"00",x"00",x"00",x"00",x"00",x"00",x"00", -- 0x04B8
    x"00",x"00",x"00",x"00",x"00",x"00",x"00",x"00", -- 0x04C0
    x"00",x"00",x"00",x"00",x"00",x"00",x"00",x"00", -- 0x04C8
    x"00",x"00",x"00",x"00",x"00",x"00",x"00",x"00", -- 0x04D0
    x"00",x"00",x"00",x"00",x"00",x"00",x"00",x"00", -- 0x04D8
    x"00",x"00",x"00",x"00",x"00",x"00",x"00",x"00", -- 0x04E0
    x"00",x"00",x"00",x"00",x"00",x"00",x"00",x"00", -- 0x04E8
    x"00",x"00",x"00",x"00",x"00",x"00",x"00",x"00", -- 0x04F0
    x"00",x"00",x"00",x"00",x"00",x"00",x"00",x"00", -- 0x04F8
    x"00",x"00",x"00",x"00",x"00",x"00",x"00",x"00", -- 0x0500
    x"00",x"00",x"00",x"00",x"00",x"00",x"00",x"00", -- 0x0508
    x"00",x"00",x"00",x"00",x"00",x"00",x"00",x"00", -- 0x0510
    x"00",x"00",x"00",x"00",x"00",x"00",x"00",x"00", -- 0x0518
    x"00",x"00",x"00",x"00",x"00",x"00",x"00",x"00", -- 0x0520
    x"00",x"00",x"00",x"00",x"00",x"00",x"00",x"00", -- 0x0528
    x"00",x"00",x"00",x"00",x"00",x"00",x"00",x"00", -- 0x0530
    x"00",x"00",x"00",x"00",x"00",x"00",x"00",x"00", -- 0x0538
    x"00",x"00",x"00",x"00",x"00",x"00",x"00",x"00", -- 0x0540
    x"00",x"00",x"00",x"00",x"00",x"00",x"00",x"00", -- 0x0548
    x"00",x"00",x"00",x"00",x"00",x"00",x"00",x"00", -- 0x0550
    x"00",x"00",x"00",x"00",x"00",x"00",x"00",x"00", -- 0x0558
    x"00",x"00",x"00",x"00",x"00",x"00",x"00",x"00", -- 0x0560
    x"00",x"00",x"00",x"00",x"00",x"00",x"00",x"00", -- 0x0568
    x"00",x"00",x"00",x"00",x"00",x"00",x"00",x"00", -- 0x0570
    x"00",x"00",x"00",x"00",x"00",x"00",x"00",x"00", -- 0x0578
    x"00",x"00",x"00",x"00",x"00",x"00",x"00",x"00", -- 0x0580
    x"00",x"00",x"00",x"00",x"00",x"00",x"00",x"00", -- 0x0588
    x"00",x"00",x"00",x"00",x"00",x"00",x"00",x"00", -- 0x0590
    x"00",x"00",x"00",x"00",x"00",x"00",x"00",x"00", -- 0x0598
    x"00",x"00",x"00",x"00",x"00",x"00",x"00",x"00", -- 0x05A0
    x"00",x"00",x"00",x"00",x"00",x"00",x"00",x"00", -- 0x05A8
    x"00",x"00",x"00",x"00",x"00",x"00",x"00",x"00", -- 0x05B0
    x"00",x"00",x"00",x"00",x"00",x"00",x"00",x"00", -- 0x05B8
    x"00",x"00",x"00",x"00",x"00",x"00",x"00",x"00", -- 0x05C0
    x"00",x"00",x"00",x"00",x"00",x"00",x"00",x"00", -- 0x05C8
    x"00",x"00",x"00",x"00",x"00",x"00",x"00",x"00", -- 0x05D0
    x"00",x"00",x"00",x"00",x"00",x"00",x"00",x"00", -- 0x05D8
    x"00",x"00",x"00",x"00",x"00",x"00",x"00",x"00", -- 0x05E0
    x"00",x"00",x"00",x"00",x"00",x"00",x"00",x"00", -- 0x05E8
    x"00",x"00",x"00",x"00",x"00",x"00",x"00",x"00", -- 0x05F0
    x"00",x"00",x"00",x"00",x"00",x"00",x"00",x"00", -- 0x05F8
    x"00",x"00",x"00",x"00",x"00",x"00",x"00",x"00", -- 0x0600
    x"00",x"00",x"00",x"00",x"00",x"00",x"00",x"00", -- 0x0608
    x"00",x"00",x"00",x"00",x"00",x"00",x"00",x"00", -- 0x0610
    x"00",x"00",x"00",x"00",x"00",x"00",x"00",x"00", -- 0x0618
    x"00",x"00",x"00",x"00",x"00",x"00",x"00",x"00", -- 0x0620
    x"00",x"00",x"00",x"00",x"00",x"00",x"00",x"00", -- 0x0628
    x"00",x"00",x"00",x"00",x"00",x"00",x"00",x"00", -- 0x0630
    x"00",x"00",x"00",x"00",x"00",x"00",x"00",x"00", -- 0x0638
    x"00",x"00",x"00",x"00",x"00",x"00",x"00",x"00", -- 0x0640
    x"00",x"00",x"00",x"00",x"00",x"00",x"00",x"00", -- 0x0648
    x"00",x"00",x"00",x"00",x"00",x"00",x"00",x"00", -- 0x0650
    x"00",x"00",x"00",x"00",x"00",x"00",x"00",x"00", -- 0x0658
    x"00",x"00",x"00",x"00",x"00",x"00",x"00",x"00", -- 0x0660
    x"00",x"00",x"00",x"00",x"00",x"00",x"00",x"00", -- 0x0668
    x"00",x"00",x"00",x"00",x"00",x"00",x"00",x"00", -- 0x0670
    x"00",x"00",x"00",x"00",x"00",x"00",x"00",x"00", -- 0x0678
    x"00",x"00",x"00",x"00",x"00",x"00",x"00",x"00", -- 0x0680
    x"00",x"00",x"00",x"00",x"00",x"00",x"00",x"00", -- 0x0688
    x"00",x"00",x"00",x"00",x"00",x"00",x"00",x"00", -- 0x0690
    x"00",x"00",x"00",x"00",x"00",x"00",x"00",x"00", -- 0x0698
    x"00",x"00",x"00",x"00",x"00",x"00",x"00",x"00", -- 0x06A0
    x"00",x"00",x"00",x"00",x"00",x"00",x"00",x"00", -- 0x06A8
    x"00",x"00",x"00",x"00",x"00",x"00",x"00",x"00", -- 0x06B0
    x"00",x"00",x"00",x"00",x"00",x"00",x"00",x"00", -- 0x06B8
    x"00",x"00",x"00",x"00",x"00",x"00",x"00",x"00", -- 0x06C0
    x"00",x"00",x"00",x"00",x"00",x"00",x"00",x"00", -- 0x06C8
    x"00",x"00",x"00",x"00",x"00",x"00",x"00",x"00", -- 0x06D0
    x"00",x"00",x"00",x"00",x"00",x"00",x"00",x"00", -- 0x06D8
    x"00",x"00",x"00",x"00",x"00",x"00",x"00",x"00", -- 0x06E0
    x"00",x"00",x"00",x"00",x"00",x"00",x"00",x"00", -- 0x06E8
    x"00",x"00",x"00",x"00",x"00",x"00",x"00",x"00", -- 0x06F0
    x"00",x"00",x"00",x"00",x"00",x"00",x"00",x"00", -- 0x06F8
    x"00",x"00",x"00",x"00",x"00",x"00",x"00",x"00", -- 0x0700
    x"00",x"00",x"00",x"00",x"00",x"00",x"00",x"00", -- 0x0708
    x"00",x"00",x"00",x"00",x"00",x"00",x"00",x"00", -- 0x0710
    x"00",x"00",x"00",x"00",x"00",x"00",x"00",x"00", -- 0x0718
    x"00",x"00",x"00",x"00",x"00",x"00",x"00",x"00", -- 0x0720
    x"00",x"00",x"00",x"00",x"00",x"00",x"00",x"00", -- 0x0728
    x"00",x"00",x"00",x"00",x"00",x"00",x"00",x"00", -- 0x0730
    x"00",x"00",x"00",x"00",x"00",x"00",x"00",x"00", -- 0x0738
    x"00",x"00",x"00",x"00",x"00",x"00",x"00",x"00", -- 0x0740
    x"00",x"00",x"00",x"00",x"00",x"00",x"00",x"00", -- 0x0748
    x"00",x"00",x"00",x"00",x"00",x"00",x"00",x"00", -- 0x0750
    x"00",x"00",x"00",x"00",x"00",x"00",x"00",x"00", -- 0x0758
    x"00",x"00",x"00",x"00",x"00",x"00",x"00",x"00", -- 0x0760
    x"00",x"00",x"00",x"00",x"00",x"00",x"00",x"00", -- 0x0768
    x"00",x"00",x"00",x"00",x"00",x"00",x"00",x"00", -- 0x0770
    x"00",x"00",x"00",x"00",x"00",x"00",x"00",x"00", -- 0x0778
    x"00",x"00",x"00",x"00",x"00",x"00",x"00",x"00", -- 0x0780
    x"00",x"00",x"00",x"00",x"00",x"00",x"00",x"00", -- 0x0788
    x"00",x"00",x"00",x"00",x"00",x"00",x"00",x"00", -- 0x0790
    x"00",x"00",x"00",x"00",x"00",x"00",x"00",x"00", -- 0x0798
    x"00",x"00",x"00",x"00",x"00",x"00",x"00",x"00", -- 0x07A0
    x"00",x"00",x"00",x"00",x"00",x"00",x"00",x"00", -- 0x07A8
    x"00",x"00",x"00",x"00",x"00",x"00",x"00",x"00", -- 0x07B0
    x"00",x"00",x"00",x"00",x"00",x"00",x"00",x"00", -- 0x07B8
    x"00",x"00",x"00",x"00",x"00",x"00",x"00",x"00", -- 0x07C0
    x"00",x"00",x"00",x"00",x"00",x"00",x"00",x"00", -- 0x07C8
    x"00",x"00",x"00",x"00",x"00",x"00",x"00",x"00", -- 0x07D0
    x"00",x"00",x"00",x"00",x"00",x"00",x"00",x"00", -- 0x07D8
    x"00",x"00",x"00",x"00",x"00",x"00",x"00",x"00", -- 0x07E0
    x"00",x"00",x"00",x"00",x"00",x"00",x"00",x"00", -- 0x07E8
    x"00",x"00",x"00",x"00",x"00",x"00",x"00",x"00", -- 0x07F0
    x"00",x"00",x"00",x"00",x"00",x"00",x"00",x"00"  -- 0x07F8
  );

begin

  p_rom : process
  begin
    wait until rising_edge(CLK);
     DATA <= ROM(to_integer(unsigned(ADDR)));
  end process;
end RTL;
