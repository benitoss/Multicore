/*
  
   Multicore 2 / Multicore 2+
  
   Copyright (c) 2017-2020 - Victor Trucco

  
   All rights reserved
  
   Redistribution and use in source and synthezised forms, with or without
   modification, are permitted provided that the following conditions are met:
  
   Redistributions of source code must retain the above copyright notice,
   this list of conditions and the following disclaimer.
  
   Redistributions in synthesized form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
  
   Neither the name of the author nor the names of other contributors may
   be used to endorse or promote products derived from this software without
   specific prior written permission.
  
   THIS CODE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
   AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
   THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
   PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE
   LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
   CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
   INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
   CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
   ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
   POSSIBILITY OF SUCH DAMAGE.
  
   You are responsible for any legal issues arising from your use of this code.
  
*/`timescale 1ns / 1ps
module m_range_check
  #(parameter WIDTH = 6)
   (input logic [WIDTH-1:0] val, low, high,
    output logic is_between);

   logic 	 smallEnough, largeEnough;
  
   m_comparator #(WIDTH) lc(,,largeEnough, low, val);
   m_comparator #(WIDTH) hc(,,smallEnough, val, high);

   assign is_between = ~smallEnough & ~largeEnough;
   
endmodule: m_range_check

module m_offset_check
  #(parameter WIDTH = 6)
   (input logic [WIDTH-1:0] val, low, delta,
    output logic is_between);

   logic 	 [WIDTH-1:0] high;
   
   m_adder #(WIDTH) add(high,, low, delta, 1'b0);
   m_range_check #(WIDTH) rc(.*);

endmodule: m_offset_check

module m_comparator
  #(parameter WIDTH = 6)
  (output logic AltB, AeqB, AgtB,
   input logic [WIDTH-1:0] A, B);

   assign AltB = (A < B);
   assign AeqB = (A == B);
   assign AgtB = (A > B);

endmodule: m_comparator

module m_adder
  #(parameter WIDTH = 6)
   (output logic [WIDTH-1:0] Sum,
    output logic Cout,
    input logic [WIDTH-1:0] A, B,
    input logic Cin);
   
   assign {Cout, Sum} = A + B + Cin;

endmodule: m_adder

module m_mux
  #(parameter WIDTH = 6)
   (output logic Y,
    input logic [WIDTH-1:0] I,
    input logic [$clog2(WIDTH)-1:0] Sel);

   assign Y = I[Sel];

endmodule: m_mux

module m_mux2to1
  #(parameter WIDTH = 6)
   (output logic [WIDTH-1:0] Y,
    input logic [WIDTH-1:0] I0, I1,
    input logic Sel);

   assign Y = (Sel ? I1 : I0);

endmodule: m_mux2to1

module m_decoder
  #(parameter WIDTH = 6)
   (output logic [(1 << WIDTH)-1:0] D,
    input logic [WIDTH-1:0] I,
    input logic en);

   assign D = en << I;

endmodule: m_decoder

module m_register
  #(parameter WIDTH = 6)
   (output logic [WIDTH-1:0] Q,
    input logic [WIDTH-1:0] D,
    input logic clr, en, clk);

   always_ff @(posedge clk)
      if(clr)
	Q <= 0;
      else if(en)
	Q <= D;
   
endmodule: m_register

module m_counter
  #(parameter WIDTH = 6)
   (output logic [WIDTH-1:0] Q,
    input logic [WIDTH-1:0] D,
    input logic clk, clr, load, en, up);

   always_ff @(posedge clk) begin
      if(clr)
	Q <= 0;
      else if(load)
	Q <= D;
      else if(en)
	Q <= (up ? Q + 1 : Q - 1);
      end
endmodule: m_counter

module m_shift_register
  #(parameter WIDTH = 6)
   (output logic [WIDTH-1:0] Q,
    input logic clk, en, left, s_in, clr);

   always_ff @(posedge clk)
     if (clr) begin
	Q <= 'd0;
     end
     else if(en) begin
	if(left) begin
	   Q <= (Q << 1);
	   Q[0] <= s_in;
	end
	else begin
	   Q <= (Q >> 1);
	   Q[WIDTH-1] <= s_in;
	end
     end
endmodule: m_shift_register
