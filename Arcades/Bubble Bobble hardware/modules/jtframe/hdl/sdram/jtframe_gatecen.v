/*
  
   Multicore 2 / Multicore 2+
  
   Copyright (c) 2017-2020 - Victor Trucco

  
   All rights reserved
  
   Redistribution and use in source and synthezised forms, with or without
   modification, are permitted provided that the following conditions are met:
  
   Redistributions of source code must retain the above copyright notice,
   this list of conditions and the following disclaimer.
  
   Redistributions in synthesized form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
  
   Neither the name of the author nor the names of other contributors may
   be used to endorse or promote products derived from this software without
   specific prior written permission.
  
   THIS CODE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
   AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
   THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
   PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE
   LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
   CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
   INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
   CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
   ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
   POSSIBILITY OF SUCH DAMAGE.
  
   You are responsible for any legal issues arising from your use of this code.
  
*//*  This file is part of JTFRAME.
      JTFRAME program is free software: you can redistribute it and/or modify
      it under the terms of the GNU General Public License as published by
      the Free Software Foundation, either version 3 of the License, or
      (at your option) any later version.

      JTFRAME program is distributed in the hope that it will be useful,
      but WITHOUT ANY WARRANTY; without even the implied warranty of
      MERCHANTABILITY or FITNESS FOR addr PARTICULAR PURPOSE.  See the
      GNU General Public License for more details.

      You should have received a copy of the GNU General Public License
      along with JTFRAME.  If not, see <http://www.gnu.org/licenses/>.

      Author: Jose Tejada Gomez. Twitter: @topapate
      Version: 1.0
      Date: 2-6-2020

*/

// Gates the clock enable signals for two clock cycles if rom_cs 
// has risen or if the rom address has changed while rom_cs was high

// Depending on how the CPU and the rom_cs decoder logic, rom_cs might
// not toggle in between ROM address changes, so the address must be
// tracked

// if rom_cs is constantly high, rom_ok will take one clock cycle to come
// down after an address change. If the cen frequency allows for at least
// two clock cycles between two cen pulses, then checking the ROM address
// is not necessary

module jtframe_gatecen #(parameter ROMW=12)(
    input             clk,
    input             rst,
    input             cen,
    input  [ROMW-1:0] rom_addr,
    input             rom_cs,
    input             rom_ok,
    output            wait_cen
);

reg  [     1:0] last_cs;
reg  [ROMW-1:0] last_addr;
reg             waitn;
wire            new_addr = last_addr != rom_addr;

assign          wait_cen = cen & waitn;

always @(posedge clk) begin
    if( rst ) begin
        waitn     <= 1;
        last_cs   <= 0;
        last_addr <= {ROMW{1'b0}};
    end else begin
        last_cs   <= { last_cs[0] & ~new_addr, rom_cs };
        last_addr <= rom_addr;
        if( rom_cs && (!last_cs[0] || new_addr) ) waitn <= 0;
        else if( rom_ok && last_cs[1] ) waitn <= 1;
    end
end

endmodule