/*
  
   Multicore 2 / Multicore 2+
  
   Copyright (c) 2017-2020 - Victor Trucco

  
   All rights reserved
  
   Redistribution and use in source and synthezised forms, with or without
   modification, are permitted provided that the following conditions are met:
  
   Redistributions of source code must retain the above copyright notice,
   this list of conditions and the following disclaimer.
  
   Redistributions in synthesized form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
  
   Neither the name of the author nor the names of other contributors may
   be used to endorse or promote products derived from this software without
   specific prior written permission.
  
   THIS CODE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
   AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
   THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
   PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE
   LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
   CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
   INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
   CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
   ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
   POSSIBILITY OF SUCH DAMAGE.
  
   You are responsible for any legal issues arising from your use of this code.
  
*//* This file is part of JT12.


    JT12 program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    JT12 program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with JT12.  If not, see <http://www.gnu.org/licenses/>.

    Author: Jose Tejada Gomez. Twitter: @topapate
    Version: 1.0
    Date: 21-03-2019
*/

// calculates d=a/b
// a = b*d + r

module jt10_adpcm_div #(parameter dw=16)(
    input               rst_n,
    input               clk,    // CPU clock
    input               cen,
    input               start,  // strobe
    input      [dw-1:0] a,
    input      [dw-1:0] b,
    output reg [dw-1:0] d,
    output reg [dw-1:0] r,
    output              working
);

reg  [dw-1:0] cycle;
assign working = cycle[0];

wire [dw:0] sub = { r[dw-2:0], d[dw-1] } - b;  

always @(posedge clk or negedge rst_n)
    if( !rst_n ) begin
        cycle <= 'd0;
    end else if(cen) begin
        if( start ) begin
            cycle <= {dw{1'd1}};
            r     <= 'd0;
            d     <= a;
        end else if(cycle[0]) begin
            cycle <= { 1'b0, cycle[dw-1:1] };
            if( sub[dw] == 0 ) begin
                r <= sub[dw-1:0];
                d <= { d[dw-2:0], 1'b1};
            end else begin
                r <= { r[dw-2:0], d[dw-1] };
                d <= { d[dw-2:0], 1'b0 };
            end
        end
    end

endmodule // jt10_adpcm_div
