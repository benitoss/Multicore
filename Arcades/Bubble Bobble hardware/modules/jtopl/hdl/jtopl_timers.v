/*
  
   Multicore 2 / Multicore 2+
  
   Copyright (c) 2017-2020 - Victor Trucco

  
   All rights reserved
  
   Redistribution and use in source and synthezised forms, with or without
   modification, are permitted provided that the following conditions are met:
  
   Redistributions of source code must retain the above copyright notice,
   this list of conditions and the following disclaimer.
  
   Redistributions in synthesized form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
  
   Neither the name of the author nor the names of other contributors may
   be used to endorse or promote products derived from this software without
   specific prior written permission.
  
   THIS CODE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
   AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
   THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
   PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE
   LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
   CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
   INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
   CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
   ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
   POSSIBILITY OF SUCH DAMAGE.
  
   You are responsible for any legal issues arising from your use of this code.
  
*//*  This file is part of JTOPL.

    JTOPL is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    JTOPL is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with JTOPL.  If not, see <http://www.gnu.org/licenses/>.

    Author: Jose Tejada Gomez. Twitter: @topapate
    Version: 1.0
    Date: 10-6-2020

    */

module jtopl_timers(
  input         clk,
  input         rst,
  input         cenop,
  input         zero,
  input [7:0]   value_A,
  input [7:0]   value_B,
  input         load_A,
  input         load_B,
  input         clr_flag_A,
  input         clr_flag_B,
  output        flag_A,
  output        flag_B,
  input         flagen_A,
  input         flagen_B,
  output        overflow_A,
  output        irq_n
);

wire   pre_A, pre_B;

assign flag_A = pre_A & flagen_A;
assign flag_B = pre_B & flagen_B;

assign irq_n = ~( flag_A | flag_B );

// 1 count per 288 master clock ticks
jtopl_timer #(.MW(2)) timer_A (
    .clk        ( clk       ), 
    .rst        ( rst       ),
    .cenop      ( cenop     ),
    .zero       ( zero      ),
    .start_value( value_A   ),
    .load       ( load_A    ),
    .clr_flag   ( clr_flag_A),
    .flag       ( pre_A     ),
    .overflow   ( overflow_A)
);

// 1 count per 288*4 master clock ticks
jtopl_timer #(.MW(4)) timer_B(
    .clk        ( clk       ), 
    .rst        ( rst       ),
    .cenop      ( cenop     ),
    .zero       ( zero      ),
    .start_value( value_B   ),
    .load       ( load_B    ),
    .clr_flag   ( clr_flag_B),
    .flag       ( pre_B     ),
    .overflow   (           )
);

endmodule

module jtopl_timer #(parameter MW=2) (
    input      clk, 
    input      rst,
    input      cenop,
    input      zero,
    input      [7:0] start_value,
    input      load,
    input      clr_flag,
    output reg flag,
    output reg overflow
);

localparam CW=8+MW;

reg [CW-1:0] cnt, next, init;

always@(posedge clk)
    if( clr_flag || rst)
        flag <= 1'b0;
    else if(overflow) flag<=1'b1;

always @(*) begin
    {overflow, next } = {1'b0, cnt}+1'b1;
    init = { start_value, {MW{1'b0}} };
end

always @(posedge clk)
    if( ~load || rst) begin
      cnt  <= { start_value, {MW{1'b0}} };
    end
    else if( cenop && zero )
      cnt <= overflow ? init : next;

endmodule
