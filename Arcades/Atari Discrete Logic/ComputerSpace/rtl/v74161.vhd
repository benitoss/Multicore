--
-- Multicore 2 / Multicore 2+
--
-- Copyright (c) 2017-2020 - Victor Trucco
--
-- All rights reserved
--
-- Redistribution and use in source and synthezised forms, with or without
-- modification, are permitted provided that the following conditions are met:
--
-- Redistributions of source code must retain the above copyright notice,
-- this list of conditions and the following disclaimer.
--
-- Redistributions in synthesized form must reproduce the above copyright
-- notice, this list of conditions and the following disclaimer in the
-- documentation and/or other materials provided with the distribution.
--
-- Neither the name of the author nor the names of other contributors may
-- be used to endorse or promote products derived from this software without
-- specific prior written permission.
--
-- THIS CODE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
-- AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
-- THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
-- PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE
-- LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
-- CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
-- SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
-- INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
-- CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
-- ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
-- POSSIBILITY OF SUCH DAMAGE.
--
-- You are responsible for any legal issues arising from your use of this code.
--
		
-- 74161 counter - extended to 8 bits
-- code found on the internet
-- adjustments made for 8 bit counter structure

library IEEE;
use IEEE.std_logic_1164.all;
use ieee.numeric_std.all;

entity v74161 is
port ( CLK, CLRN, LDN, ENP, ENT: in STD_LOGIC :='0';
D: in UNSIGNED (7 downto 0) := "00000000";
Q: out UNSIGNED (7 downto 0):= "00000000";
RCO: out STD_LOGIC  :='0');
end v74161;

architecture V74x161_arch of v74161 is
 signal IQ: UNSIGNED (7 downto 0) := "00000000";
 signal IRCO: STD_LOGIC := '0';
 begin

process (CLK, CLRN, IQ, ENT)
begin

if CLRN='0' then IQ <= "00000000";
elsif rising_edge(CLK) then
if LDN='0' then IQ <= D;
elsif (ENT and ENP)='1' then IQ <= IQ + 1;
end if;
end if;

--if (IQ=15) and (ENT='1') then IRCO <= '1';
if (IQ=255) and (ENT='1') then IRCO <= '1';
else IRCO <= '0';
end if;

end process;



Q <= IQ;
RCO <= IRCO;

end V74x161_arch;
