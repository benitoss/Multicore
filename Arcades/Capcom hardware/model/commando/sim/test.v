/*
  
   Multicore 2 / Multicore 2+
  
   Copyright (c) 2017-2020 - Victor Trucco

  
   All rights reserved
  
   Redistribution and use in source and synthezised forms, with or without
   modification, are permitted provided that the following conditions are met:
  
   Redistributions of source code must retain the above copyright notice,
   this list of conditions and the following disclaimer.
  
   Redistributions in synthesized form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
  
   Neither the name of the author nor the names of other contributors may
   be used to endorse or promote products derived from this software without
   specific prior written permission.
  
   THIS CODE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
   AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
   THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
   PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE
   LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
   CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
   INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
   CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
   ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
   POSSIBILITY OF SUCH DAMAGE.
  
   You are responsible for any legal issues arising from your use of this code.
  
*/`timescale 1ns/1ps

module test;

reg  CLK12;
wire phiB;
wire [8:0] H;
wire [8:0] V;
wire HINIT;
wire LHBL;
wire LVBL;

initial begin
    CLK12 = 1'b0;
    forever #(1000.000/12/2) CLK12 = ~CLK12;    // 12 MHz
end

integer blanks=0;

always @(negedge LVBL) begin
    $display("Vertical blank");
    blanks <= blanks+1;
    if(blanks==3) $finish;
end

`ifdef SIMTIME
real simtime = `SIMTIME;
initial begin
    $display("Simulation will finish after %.1f ms", simtime );
    simtime = simtime * 1000_000;
    #(simtime) $finish;
end
`endif

initial begin
    `ifdef VCD
    $dumpfile("test.vcd");
    `else 
    $dumpfile("test.lxt");
    `endif
    `ifdef DUMPALL
    $dumpvars;
    `else
    $dumpvars(1,test.uut);
    $dumpvars(1,test);
    `endif
    $dumpon;
end

wire SCREN, LSCREN, phiSC, phiMAIN;
wire [3:0] AB = 4'd0;
wire [7:0] DB = 8'd0;

reg scr_csn;

initial begin
    scr_csn = 1'b1;
    #5000 scr_csn = 1'b0;
    #5100 scr_csn = 1'b1;
end

pcb uut(
    .CLK12  ( CLK12   ),
    .phiB   ( phiB    ),
    .H      ( H       ), 
    .V      ( V       ),
    .HINIT  ( HINIT   ), 
    .LHBL   ( LHBL    ), 
    .LVBL   ( LVBL    ),
    // SCROLLH
    .FLIP   ( 1'b0    ),
    .AB     ( AB      ),
    .DB     ( DB      ),
    .SCREN  ( SCREN   ),
    .LSCREN ( LSCREN  ),
    .phiSC  ( phiSC   ),
    .phiMAIN( phiMAIN ),
    .C8CS   ( 1'b1    ),
    .D8CS   ( scr_csn )
);

endmodule
