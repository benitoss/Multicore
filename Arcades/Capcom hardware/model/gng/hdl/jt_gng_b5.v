/*
  
   Multicore 2 / Multicore 2+
  
   Copyright (c) 2017-2020 - Victor Trucco

  
   All rights reserved
  
   Redistribution and use in source and synthezised forms, with or without
   modification, are permitted provided that the following conditions are met:
  
   Redistributions of source code must retain the above copyright notice,
   this list of conditions and the following disclaimer.
  
   Redistributions in synthesized form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
  
   Neither the name of the author nor the names of other contributors may
   be used to endorse or promote products derived from this software without
   specific prior written permission.
  
   THIS CODE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
   AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
   THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
   PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE
   LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
   CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
   INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
   CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
   ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
   POSSIBILITY OF SUCH DAMAGE.
  
   You are responsible for any legal issues arising from your use of this code.
  
*/`timescale 1ns/1ps

/*

	Schematic sheet: 85606-B-2-5/9 Line buffer 1

*/

module jt_gng_b5(
	input	[7:0]	OBJ2,	// OBJ2
	inout	[7:0]	COL,
	input			ST_b,
	input			OBFLIP,
	input			OB6M,
	input	[7:0]	DF,
	input			LOAD_b,
	input			HOVER,
	input			CL_b,
	input			WR_b,
	input			L6MB,

	input			DISPIM_bq,
	input			LV1_bq,
	output	reg [7:0]	OBJ // C14-C17
);

wire [7:0]	OBJ1;

// These pullups are on B6 in the original
pullup( COL[7],
		COL[6],
		COL[5],
		COL[4],
		COL[3],
		COL[2],
		COL[1],
		COL[0]);

jt_gng_b6 buffer (
	.OBJ   (OBJ1  ),
	.COL   (COL   ),
	.ST_b  (ST_b  ),
	.OBFLIP(OBFLIP),
	.OB6M  (OB6M  ),
	.DF    (DF    ),
	.LOAD_b(LOAD_b),
	.HOVER (HOVER ),
	.CL_b  (CL_b  ),
	.WR_b  (WR_b  ),
	.L6MB  (L6MB  )
);

always @(*)
	if( DISPIM_bq )
		OBJ = 8'hFF; // pullup
	else OBJ = LV1_bq ? OBJ1 : OBJ2;

endmodule // jt_gng_b5