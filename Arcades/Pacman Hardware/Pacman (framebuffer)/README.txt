---------------------------------------------------------------------------------
-- 
-- Pacman hardware with framebuffer
-- Victor Trucco - 2020
-- 
---------------------------------------------------------------------------------
-- A simulation model of Pacman hardware
-- Copyright (c) MikeJ - January 2006
---------------------------------------------------------------------------------
-- 
-- Unified source code
-- Screen rotation
-- HDMI
-- Dip Swtichs
-- Joystick Sega 3 or 6 button support
-- Alternative ROMs support
--
-- MAME style keyboard :
--
--   5    : Coin
--   2    : Start 2 players
--   1    : Start 1 player
--   UP,DOWN,LEFT,RIGHT arrows : Movements
--
---------------------------------------------------------------------------------
