//----------------------------------------------------------------------------
//
//  Arcade: Mario Bros by gaz68 (https://github.com/gaz68)
//
//  June 2020
//
//  Based on the original Donkey Kong core by Katsumi Degawa.
//
//  Original Donkey Kong port to MiSTer
//  Copyright (C) 2017 Sorgelig
//
//  This program is free software; you can redistribute it and/or modify it
//  under the terms of the GNU General Public License as published by the Free
//  Software Foundation; either version 2 of the License, or (at your option)
//  any later version.
//
//  This program is distributed in the hope that it will be useful, but WITHOUT
//  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
//  more details.
//
//  You should have received a copy of the GNU General Public License along
//  with this program; if not, write to the Free Software Foundation, Inc.,
//  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
//
//----------------------------------------------------------------------------

//============================================================================
//
//  Multicore 2+ Top by Victor Trucco
//
//============================================================================
`default_nettype none


module MarioBros_mc2p
(
  // Clocks
    input wire  clock_50_i,

    // Buttons
    input wire [4:1]    btn_n_i,

    // SRAMs (AS7C34096)
    output wire [20:0]sram_addr_o  = 20'b000000000000000000000,
    inout wire  [7:0]sram_data_io   = 8'bzzzzzzzz,
    output wire sram_we_n_o     = 1'b1,
    output wire sram_oe_n_o     = 1'b1,
        
    // SDRAM    (H57V256)
    output [12:0] SDRAM_A,
    output  [1:0] SDRAM_BA,
    inout  [15:0] SDRAM_DQ,
    output        SDRAM_DQMH,
    output        SDRAM_DQML,
    output        SDRAM_CKE,
    output        SDRAM_nCS,
    output        SDRAM_nWE,
    output        SDRAM_nRAS,
    output        SDRAM_nCAS,
    output        SDRAM_CLK,

    // PS2
    inout wire  ps2_clk_io          = 1'bz,
    inout wire  ps2_data_io         = 1'bz,
    inout wire  ps2_mouse_clk_io  = 1'bz,
    inout wire  ps2_mouse_data_io = 1'bz,

    // SD Card
    output wire sd_cs_n_o           = 1'b1,
    output wire sd_sclk_o           = 1'b0,
    output wire sd_mosi_o           = 1'b0,
    input wire  sd_miso_i,

    // Joysticks
   output wire joy_clock_o         = 1'b1,
   output wire joy_load_o          = 1'b1,
   input  wire joy_data_i,
   output wire joy_p7_o            = 1'b1,

    // Audio
    output        AUDIO_L,
    output        AUDIO_R,
    input wire  ear_i,
    output wire mic_o                   = 1'b0,

        // VGA
    output  [4:0] VGA_R,
    output  [4:0] VGA_G,
    output  [4:0] VGA_B,
    output        VGA_HS,
    output        VGA_VS,

        //STM32
    input wire  stm_tx_i,
    output wire stm_rx_o,
    output wire stm_rst_o           = 1'bz, // '0' to hold the microcontroller reset line, to free the SD card

    input         SPI_SCK,
    output        SPI_DO,
    input         SPI_DI,
    input         SPI_SS2,
   output wire   SPI_WAIT        = 1'b1, // '0' to hold the microcontroller data streaming
   inout [31:0] GPIO,
   output LED                    = 1'b1 // '0' is LED on
);

localparam CONF_STR = {
   "P,Mario Bros.dat;",
    "S,DAT,Alternative ROM...;", 
//    "O12,Screen Rotate,0,90,180,270;",
    "O34,Scanlines,Off,25%,50%,75%;",
    "O6,Double Video Buffer,On,Off;",
    "O5,Blend,Off,On;",
    "O7,Scandoubler,On,Off;",
    "O89,Lives,3,4,5,6;",
    "OAB,Coin/Credit,1/1,2/1,1/2,1/3;",
    "OCD,Extra Life,20000,30000,40000,None;",
    "OEF,Difficulty,Easy,Medium,Hard,Hardest;",
   "-;",

   "T0,Reset;"
};



//---------------------------------------------------------
//-- MC2+ defaults
//---------------------------------------------------------
assign GPIO = 32'bzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzz;
assign stm_rst_o    = 1'bZ;
assign stm_rx_o = 1'bZ;
assign LED  = ~ioctl_download;

//no SRAM for this core
assign sram_we_n_o  = 1'b1;
assign sram_oe_n_o  = 1'b1;

assign SDRAM_nCS = 1'b1;

//all the SD reading goes thru the microcontroller for this core
assign sd_cs_n_o = 1'bZ;
assign sd_sclk_o = 1'bZ;
assign sd_mosi_o = 1'bZ;

wire joy1_up_i, joy1_down_i, joy1_left_i, joy1_right_i, joy1_p6_i, joy1_p9_i;
wire joy2_up_i, joy2_down_i, joy2_left_i, joy2_right_i, joy2_p6_i, joy2_p9_i;

joystick_serial  joystick_serial 
(
    .clk_i           ( clk_sys ),
    .joy_data_i      ( joy_data_i ),
    .joy_clk_o       ( joy_clock_o ),
    .joy_load_o      ( joy_load_o ),

    .joy1_up_o       ( joy1_up_i ),
    .joy1_down_o     ( joy1_down_i ),
    .joy1_left_o     ( joy1_left_i ),
    .joy1_right_o    ( joy1_right_i ),
    .joy1_fire1_o    ( joy1_p6_i ),
    .joy1_fire2_o    ( joy1_p9_i ),

    .joy2_up_o       ( joy2_up_i ),
    .joy2_down_o     ( joy2_down_i ),
    .joy2_left_o     ( joy2_left_i ),
    .joy2_right_o    ( joy2_right_i ),
    .joy2_fire1_o    ( joy2_p6_i ),
    .joy2_fire2_o    ( joy2_p9_i )
);

// ROM Data Pump
reg [7:0] pump_s = 8'b11111111;
PumpSignal PumpSignal (clk_sys, ~pll_locked, ioctl_download, pump_s);

//-----------------------------------------------------------------

////////////////////   CLOCKS   ///////////////////

wire clk_sys;
wire clk_sub;
wire clk_main, pll_locked;

pll pll
(
   .inclk0(clock_50_i),
   .c0(clk_sys),  // 24 Mhz
   .c1(clk_sub),  // 11 Mhz
   .c2(clk_main),  //  4 Mhz
   .locked (pll_locked)
);

wire clk_25m2,clk_40;    
pll_vga pll_vga(
    .inclk0(clock_50_i),
    .c0(clk_25m2),
    .c1(clk_40)
    );

///////////////////////////////////////////////////

wire [31:0] status;
wire  [1:0] buttons;
wire        forced_scandoubler;
wire        direct_video;

wire        ioctl_download;
wire        ioctl_wr;
wire [24:0] ioctl_addr;
wire  [7:0] ioctl_dout;
wire  [7:0] ioctl_index;

data_io #(.STRLEN(($size(CONF_STR)>>3)))
data_io(
    .clk_sys       ( clk_sys      ),
    .SPI_SCK       ( SPI_SCK      ),
    .SPI_SS2       ( SPI_SS2      ),
    .SPI_DI        ( SPI_DI       ),
    .SPI_DO        ( SPI_DO       ),
    
    .data_in       ( pump_s & osd_s ),
    .conf_str      ( CONF_STR     ),
    .status        ( status       ),
    //.core_mod      ( core_mod      ),

    .ioctl_download( ioctl_download  ),
    .ioctl_index   ( ioctl_index  ),
    .ioctl_wr      ( ioctl_wr     ),
    .ioctl_addr    ( ioctl_addr   ),
    .ioctl_dout    ( ioctl_dout   )
);

wire [10:0] ps2_key;

wire [15:0] joy_0, joy_1;
wire [21:0] gamma_bus;


wire [7:0]m_sw1={~m_tilt,~{btn_two_players},~{btn_one_player},((~m_fireA)&&(~m_fireB)&&(~m_fireC)&&(~m_fireD)),1'b1,1'b1,~m_left,~m_right};
wire [7:0]m_sw2={1'b1,1'b1,~{btn_coin},((~m_fire2A)&&(~m_fire2B)&&(~m_fire2C)&&(~m_fire2D)),1'b1,1'b1,~m_left2,~m_right2};

wire hblank, vblank;
wire hs, vs;
wire [2:0] r,g;
wire [1:0] b;


wire [7:0] vga_col_s;
wire vga_hs_s, vga_vs_s;

framebuffer #(256,224,8) framebuffer
(
        .clk_sys    ( clk_sys ),
        .clk_i      ( ce_vid ),
        .RGB_i      ((blankn) ? {r,g,b} : 8'b00000000 ),
        .hblank_i   ( hblank ),
        .vblank_i   ( vblank ),
        
        .rotate_i   ( status[2:1] ), 

        .clk_vga_i  ( (status[1]) ? clk_40 : clk_25m2 ), //800x600 or 640x480
        .RGB_o      ( vga_col_s ),
        .hsync_o    ( vga_hs_s ),
        .vsync_o    ( vga_vs_s ),
        .blank_o    (  ),

        .odd_line_o (  )
);

wire blankn = ~(hblank | vblank);
wire direct_video_s = ~status[7] ^ direct_video;

mist_video #(.COLOR_DEPTH(3),.SD_HCNT_WIDTH(10), .USE_FRAMEBUFFER(1)) mist_video(
    .clk_sys( direct_video_s ? clk_sys :(status[1]) ? clk_40 : clk_25m2),
    .SPI_SCK(SPI_SCK),
    .SPI_SS3(SPI_SS2),
    .SPI_DI(SPI_DI),

    .R       ( (direct_video_s) ? (blankn) ? r : 3'b000 : vga_col_s[7:5] ),
    .G       ( (direct_video_s) ? (blankn) ? g : 3'b000 : vga_col_s[4:2]  ),
    .B       ( (direct_video_s) ? (blankn) ? {b, b[0]} : 3'b000 : {vga_col_s[1:0],vga_col_s[0]}  ),
    .HSync   ( (direct_video_s) ? ~hs : vga_hs_s ),
    .VSync   ( (direct_video_s) ? ~vs : vga_vs_s ),
    
    .VGA_R(VGA_R),
    .VGA_G(VGA_G),
    .VGA_B(VGA_B),
    .VGA_VS(VGA_VS),
    .VGA_HS(VGA_HS),

    .ce_divider ( 1'b1 ),
    .blend      ( status[5] ),
    .scandoubler_disable(direct_video_s),
    .scanlines  ( status[4:3] ),
    .no_csync   ( ~direct_video_s ),
    .rotate     ( osd_rotate ),
    .osd_enable ( osd_enable )
    );
/*
arcade_video#(256,8) arcade_video
(
   .*,

   .clk_video(clk_sys),
   .ce_pix(ce_vid),

   .RGB_in({r,g,b}),
   .HBlank(hblank),
   .VBlank(vblank),
   .HSync(~hs),
   .VSync(~vs),

   .fx(status[5:3])
);
*/
assign hblank = hbl[8];

reg  ce_vid;
wire clk_pix;
wire hbl0;
reg [8:0] hbl;
always @(posedge clk_sys) begin
   reg old_pix;
   old_pix <= clk_pix;
   ce_vid <= 0;
   if(~old_pix & clk_pix) begin
      ce_vid <= 1;
      hbl <= (hbl<<1)|hbl0;
   end
end

wire signed [15:0] audio;

mario_top mariobros 
(
   .I_CLK_24M(clk_sys),
   .I_CLK_4M(clk_main),
   .I_CLK_11M(clk_sub),
   .I_RESETn(~(status[0] | ~btn_n_o[4])),

   .dn_addr(ioctl_addr),
   .dn_data(ioctl_dout),
   .dn_wr(ioctl_wr && ioctl_index==0),

   .O_PIX(clk_pix),

   .I_SW1(m_sw1),
   .I_SW2(m_sw2),
   .I_DIPSW(status[15:8] ),
   //.I_DIPSW(m_dip),
   .I_ANLG_VOL(status[16:13]),

   .O_VGA_R(r),
   .O_VGA_G(g),
   .O_VGA_B(b),
   .O_VGA_HSYNCn(hs),
   .O_VGA_VSYNCn(vs),
   .O_HBLANK(hbl0),
   .O_VBLANK(vblank),

   .O_SOUND_DAT(audio)
);

dac #(
    .C_bits(16))
dac(
    .clk_i(clk_sys),
    .res_n_i(1'b1),
    .dac_i({~audio[15],audio[14:0]}),
    .dac_o(AUDIO_L)
    );


//------------------------------------------------------------
    
wire m_up, m_down, m_left, m_right, m_fireA, m_fireB, m_fireC, m_fireD, m_fireE, m_fireF, m_fireG;
wire m_up2, m_down2, m_left2, m_right2, m_fire2A, m_fire2B, m_fire2C, m_fire2D, m_fire2E, m_fire2F, m_fire2G;
wire m_tilt, m_coin1, m_coin2, m_coin3, m_coin4, m_one_player, m_two_players, m_three_players, m_four_players;

wire m_right4, m_left4, m_down4, m_up4, m_right3, m_left3, m_down3, m_up3;

wire btn_one_player  = ~btn_n_o[1] | m_one_player;
wire btn_two_players = ~btn_n_o[2] | m_two_players;
wire btn_coin        = ~btn_n_o[3] | m_coin1;
wire [7:0] osd_s;
wire [15:0]joy1_s;
wire [15:0]joy2_s;
wire [8:0]controls_s;
wire osd_enable;
wire [1:0]osd_rotate;
wire [4:1]btn_n_o;
//translate scancode to joystick
MC2_HID #( .OSD_CMD    ( 3'b011 ), .USE_VKP( 1'b1), .CLK_SPEED(24000)) k_hid
(
    .clk          ( clk_sys ), 
    .kbd_clk      ( ps2_clk_io ),
    .kbd_dat      ( ps2_data_io ),

    .joystick_0   ({ joy1_p9_i, joy1_p6_i, joy1_up_i, joy1_down_i, joy1_left_i, joy1_right_i }),
    .joystick_1   ({ joy2_p9_i, joy2_p6_i, joy2_up_i, joy2_down_i, joy2_left_i, joy2_right_i }),
      
    //-- joystick_0 and joystick_1 should be swapped
    .joyswap      ( 0 ),

    //-- player1 and player2 should get both joystick_0 and joystick_1
    .oneplayer    ( 1 ),

    //-- tilt, coin4-1, start4-1
    .controls     ( {m_tilt, m_coin4, m_coin3, m_coin2, m_coin1, m_four_players, m_three_players, m_two_players, m_one_player} ),

    //-- fire12-1, up, down, left, right

    .player1      ( {m_fireG,  m_fireF, m_fireE, m_fireD, m_fireC, m_fireB, m_fireA, m_up, m_down, m_left, m_right} ),
    .player2      ( {m_fire2G, m_fire2F, m_fire2E, m_fire2D, m_fire2C, m_fire2B, m_fire2A, m_up2, m_down2, m_left2, m_right2} ),

    .direct_video ( direct_video ),
    .osd_rotate   ( osd_rotate ),

    //-- keys to the OSD
    .osd_o        ( osd_s ),
    .osd_enable   ( osd_enable ),

    //-- sega joystick
    .sega_strobe  ( joy_p7_o ),

	 .front_buttons_i ( btn_n_i ),
	 .front_buttons_o ( btn_n_o )
        
);

endmodule
