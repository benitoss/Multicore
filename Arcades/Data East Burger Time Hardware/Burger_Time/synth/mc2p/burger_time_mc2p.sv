/*
  
   Multicore 2 / Multicore 2+
  
   Copyright (c) 2017-2020 - Victor Trucco

  
   All rights reserved
  
   Redistribution and use in source and synthezised forms, with or without
   modification, are permitted provided that the following conditions are met:
  
   Redistributions of source code must retain the above copyright notice,
   this list of conditions and the following disclaimer.
  
   Redistributions in synthesized form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
  
   Neither the name of the author nor the names of other contributors may
   be used to endorse or promote products derived from this software without
   specific prior written permission.
  
   THIS CODE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
   AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
   THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
   PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE
   LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
   CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
   INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
   CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
   ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
   POSSIBILITY OF SUCH DAMAGE.
  
   You are responsible for any legal issues arising from your use of this code.
  
*///============================================================================
//
//  Multicore 2+ Top by Oduvaldo Pavan Junior ( ducasp@gmail.com )
//
//============================================================================

`default_nettype none

module burger_time_mc2p
(
// Clocks
    input wire  clock_50_i,

    // Buttons
    input wire [4:1]    btn_n_i,

    // SRAMs (AS7C34096)
    output wire [20:0]sram_addr_o  = 21'b000000000000000000000,
    inout wire  [7:0]sram_data_io   = 8'bzzzzzzzz,
    output wire sram_we_n_o     = 1'b1,
    output wire sram_oe_n_o     = 1'b1,
        
    // SDRAM    (H57V256)
    output [12:0] SDRAM_A,
    output  [1:0] SDRAM_BA,
    inout  [15:0] SDRAM_DQ,
    output        SDRAM_DQMH,
    output        SDRAM_DQML,
    output        SDRAM_CKE,
    output        SDRAM_nCS,
    output        SDRAM_nWE,
    output        SDRAM_nRAS,
    output        SDRAM_nCAS,
    output        SDRAM_CLK,

    // PS2
    inout wire  ps2_clk_io          = 1'bz,
    inout wire  ps2_data_io         = 1'bz,
    inout wire  ps2_mouse_clk_io  = 1'bz,
    inout wire  ps2_mouse_data_io = 1'bz,

    // SD Card
    output wire sd_cs_n_o           = 1'b1,
    output wire sd_sclk_o           = 1'b0,
    output wire sd_mosi_o           = 1'b0,
    input wire  sd_miso_i,

    // Joysticks
   output wire joy_clock_o         = 1'b1,
   output wire joy_load_o          = 1'b1,
   input  wire joy_data_i,
   output wire joy_p7_o            = 1'b1,

    // Audio
    output        AUDIO_L,
    output        AUDIO_R,
    input wire  ear_i,
    output wire mic_o                   = 1'b0,

        // VGA
    output  [4:0] VGA_R,
    output  [4:0] VGA_G,
    output  [4:0] VGA_B,
    output        VGA_HS,
    output        VGA_VS,

        //STM32
    input wire  stm_tx_i,
    output wire stm_rx_o,
    output wire stm_rst_o           = 1'bz, // '0' to hold the microcontroller reset line, to free the SD card

    input         SPI_SCK,
    output        SPI_DO,
    input         SPI_DI,
    input         SPI_SS2,
    inout [31:0]  GPIO,
    output        LED               = 1'b1 // '0' is LED on
);

`include "..\..\rtl\build_id.v" 

localparam CONF_STR = {
    "P,BurgerTime.ini;",
    "O12,Screen Rotate,0,90,180,270;",
    "O34,Scanlines,Off,25%,50%,75%;", //30
    "O8,Double Video Buffer,On,Off;",
    "O5,Blend,Off,On;", // 16
    "OG,Scandoubler,On,Off;",
    "T6,Reset;", // 9
    "V,v1.10." // 8
    };

wire [31:0] status;
wire  [1:0] buttons;
wire  [1:0] switches;
wire  [9:0] kbjoy;
wire  [7:0] joystick_0;
wire  [7:0] joystick_1;
wire        scandoublerD;
wire        ps2_kbd_clk, ps2_kbd_data;

//assign LED = 1;
assign GPIO = 32'bzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzz;
assign sram_we_n_o      = 1'b1;
assign sram_oe_n_o      = 1'b1;
assign stm_rst_o            = 1'bz;

wire joy1_up_i, joy1_down_i, joy1_left_i, joy1_right_i, joy1_p6_i, joy1_p9_i;
wire joy2_up_i, joy2_down_i, joy2_left_i, joy2_right_i, joy2_p6_i, joy2_p9_i;

joystick_serial joystick_serial 
(
    .clk_i           ( clk_12 ),
    .joy_data_i      ( joy_data_i ),
    .joy_clk_o       ( joy_clock_o ),
    .joy_load_o      ( joy_load_o ),

    .joy1_up_o       ( joy1_up_i ),
    .joy1_down_o     ( joy1_down_i ),
    .joy1_left_o     ( joy1_left_i ),
    .joy1_right_o    ( joy1_right_i ),
    .joy1_fire1_o    ( joy1_p6_i ),
    .joy1_fire2_o    ( joy1_p9_i ),

    .joy2_up_o       ( joy2_up_i ),
    .joy2_down_o     ( joy2_down_i ),
    .joy2_left_o     ( joy2_left_i ),
    .joy2_right_o    ( joy2_right_i ),
    .joy2_fire1_o    ( joy2_p6_i ),
    .joy2_fire2_o    ( joy2_p9_i )
);

wire clk_12, clk_24;
wire pll_locked;

pll pll(
    .inclk0(clock_50_i),
    .areset(0),
    .c0(clk_12),
    .c1(clk_24),
    .locked(pll_locked)
    );

wire clk_25m2,clk_40;
pll_vga pll_vga(
    .inclk0(clock_50_i),
    .c0(clk_25m2),
    .c1(clk_40)
    );

burger_time burger_time(
    .clock_12(clk_12),
    .reset(status[0] | status[6] | ~btn_n_i[4]),
    .PCLK(PCLK),
    .video_r(r),
    .video_g(g),
    .video_b(b),
    .video_csync(),
    .video_blankn(blankn),
    .video_hs(hs),
    .video_vs(vs),
    .h_blank(hb),
    .v_blank(vb),
    .audio_out(audio),  
    .start2(btn_two_players),
    .start1(btn_one_player),
    .coin1(btn_coin),
    .fire1(m_fire),
    .right1(m_right),
    .left1(m_left),
    .down1(m_down),
    .up1(m_up),
    .fire2(m_fire),
    .right2(m_right),
    .left2(m_left),
    .down2(m_down),
    .up2(m_up),
    .dbg_cpu_addr()
    );

wire [10:0] audio;

dac #(11) dac(
    .clk_i(clk_12),
    .res_n_i(1),
    .dac_i(audio),
    .dac_o(AUDIO_L)
    );

assign AUDIO_R = AUDIO_L;

wire hs, vs;
wire [2:0] r, g;
wire [1:0] b;
wire       blankn;
wire       hb, vb;
wire [7:0] vga_col_s;
wire vga_hs_s, vga_vs_s, PCLK;

framebuffer #(240,240,8,1) framebuffer
(
        .clk_sys    ( clk_24 ),
        .clk_i      ( PCLK ),
        .RGB_i      ((blankn) ? {r,g,b} : 8'b00000000 ),
        .hblank_i   ( hb ),
        .vblank_i   ( vb ),
        .dis_db_i   ( status[8] ),
        .rotate_i   ( status[2:1] ), 

        .clk_vga_i  ( (status[1]) ? clk_40 : clk_25m2 ), //800x600 or 640x480
        .RGB_o      ( vga_col_s ),
        .hsync_o    ( vga_hs_s ),
        .vsync_o    ( vga_vs_s ),
        .blank_o    (  ),

        .odd_line_o (  )
);

assign scandoublerD = ~status[16] ^ direct_video;
wire [1:0] osd_rotate;

mist_video #(.COLOR_DEPTH(3), .SD_HCNT_WIDTH(10), .USE_FRAMEBUFFER(1)) mist_video(
    .clk_sys        ( (scandoublerD) ? clk_24 : (status[1]) ? clk_40 : clk_25m2 ),
    .SPI_SCK        ( SPI_SCK          ),
    .SPI_SS3        ( SPI_SS2          ),
    .SPI_DI         ( SPI_DI           ),
    .R              ((scandoublerD) ? blankn ? r : 0 : vga_col_s[7:5]),
    .G              ((scandoublerD) ? blankn ? g : 0 : vga_col_s[4:2]),
    .B              ((scandoublerD) ? blankn ? {b ,b[0]} : 0 : {vga_col_s[1:0],vga_col_s[0]}),
    .HSync          ((scandoublerD) ? hs : vga_hs_s),
    .VSync          ((scandoublerD) ? vs : vga_vs_s),
    .VGA_R          ( VGA_R            ),
    .VGA_G          ( VGA_G            ),
    .VGA_B          ( VGA_B            ),
    .VGA_VS         ( VGA_VS           ),
    .VGA_HS         ( VGA_HS           ),
    .rotate         ( osd_rotate       ),
    .ce_divider     ( 1'b1             ),
    .blend          ( status[5]        ),
    .scandoubler_disable( scandoublerD ),
    .no_csync       ( 1'b0             ),
    .osd_enable     ( osd_enable       ),
    .scanlines      ( status[4:3]      )
    );

data_io #(
    .STRLEN($size(CONF_STR)>>3))
data_io(
    .clk_sys       ( clk_24          ),
    .SPI_SCK       ( SPI_SCK         ),
    .SPI_SS2       ( SPI_SS2         ),
    .SPI_DI        ( SPI_DI          ),
    .SPI_DO        ( SPI_DO          ),

    .ioctl_download( ioctl_download  ),
    .data_in       ( keys_s& pump_s  ),
    .conf_str      ( CONF_STR        ),
    .status        ( status          )
);

reg [7:0] pump_s = 8'b11111111;
wire ioctl_download;
PumpSignal PumpSignal (clk_24, ~pll_locked, ioctl_download, pump_s);

wire m_up     = JoyPCFRLDU[0];
wire m_down   = JoyPCFRLDU[1];
wire m_left   = JoyPCFRLDU[2];
wire m_right  = JoyPCFRLDU[3];
wire m_fire   = JoyPCFRLDU[4] | JoyPCFRLDU[5] | JoyPCFRLDU[6];

wire btn_one_player =   ~btn_n_i[1] | m_one_player;
wire btn_two_players =  ~btn_n_i[2] | m_two_players;
wire btn_coin  =        ~btn_n_i[3] | m_coin1;

wire kbd_intr;
wire [8:0] JoyPCFRLDU;
wire [7:0] kbd_scancode;
wire [7:0] keys_s;
wire m_tilt, m_coin1, m_coin2, m_coin3, m_coin4, m_one_player, m_two_players, m_three_players, m_four_players;
wire osd_enable, direct_video;

//get scancode from keyboard
io_ps2_keyboard #(.CLK_SPEED(12000)) keyboard 
 (
  .clk       ( clk_12 ),
  .kbd_clk   ( ps2_clk_io ),
  .kbd_dat   ( ps2_data_io ),
  .interrupt ( kbd_intr ),
  .scancode  ( kbd_scancode )
);

//translate scancode to joystick
kbd_joystick #( .OSD_CMD( 3'b011 ), .USE_VKP( 1'b1), .CLK_SPEED(12000)) k_joystick
(
  .clk          ( clk_12 ),
  .kbdint       ( kbd_intr ),
  .kbdscancode  ( kbd_scancode ), 
  .joystick_0   ({ joy1_p9_i, joy1_p6_i, joy1_up_i, joy1_down_i, joy1_left_i, joy1_right_i }),
  .joystick_1   ({ joy2_p9_i, joy2_p6_i, joy2_up_i, joy2_down_i, joy2_left_i, joy2_right_i }),
  //-- joystick_0 and joystick_1 should be swapped
  .joyswap      ( 0 ),
  //-- player1 and player2 should get both joystick_0 and joystick_1
  .oneplayer    ( 1 ),
  .direct_video ( direct_video ),
  .osd_rotate   ( osd_rotate ),
  .player1      ({ JoyPCFRLDU[7], JoyPCFRLDU[6], JoyPCFRLDU[5], JoyPCFRLDU[4], JoyPCFRLDU[0], JoyPCFRLDU[1], JoyPCFRLDU[2], JoyPCFRLDU[3] }),
  //-- tilt, coin4-1, start4-1
  .controls     ( {m_tilt, m_coin4, m_coin3, m_coin2, m_coin1, m_four_players, m_three_players, m_two_players, m_one_player} ),

  .osd_o        ( keys_s ),
  .osd_enable   ( osd_enable ),
  //-- sega joystick
  .sega_strobe  ( joy_p7_o )
);

endmodule
