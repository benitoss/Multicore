/*
  
   Multicore 2 / Multicore 2+
  
   Copyright (c) 2017-2020 - Victor Trucco

  
   All rights reserved
  
   Redistribution and use in source and synthezised forms, with or without
   modification, are permitted provided that the following conditions are met:
  
   Redistributions of source code must retain the above copyright notice,
   this list of conditions and the following disclaimer.
  
   Redistributions in synthesized form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
  
   Neither the name of the author nor the names of other contributors may
   be used to endorse or promote products derived from this software without
   specific prior written permission.
  
   THIS CODE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
   AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
   THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
   PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE
   LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
   CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
   INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
   CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
   ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
   POSSIBILITY OF SUCH DAMAGE.
  
   You are responsible for any legal issues arising from your use of this code.
  
*/// Green Beret (Rusn'n Attack) MiST top-level

//============================================================================
//
//  Multicore 2+ Top by Oduvaldo Pavan Junior - ducasp@gmail.com
//
//============================================================================

`default_nettype none


module gberet_mc2p (
// Clocks
    input wire  clock_50_i,

    // Buttons
    input wire [4:1]    btn_n_i,

    // SRAMs (AS7C34096)
    output wire [20:0]sram_addr_o  = 20'b000000000000000000000,
    inout wire  [7:0]sram_data_io   = 8'bzzzzzzzz,
    output wire sram_we_n_o     = 1'b1,
    output wire sram_oe_n_o     = 1'b1,
        
    // SDRAM    (H57V256)
    output [12:0] SDRAM_A,
    output  [1:0] SDRAM_BA,
    inout  [15:0] SDRAM_DQ,
    output        SDRAM_DQMH,
    output        SDRAM_DQML,
    output        SDRAM_CKE,
    output        SDRAM_nCS,
    output        SDRAM_nWE,
    output        SDRAM_nRAS,
    output        SDRAM_nCAS,
    output        SDRAM_CLK,

    // PS2
    inout wire  ps2_clk_io          = 1'bz,
    inout wire  ps2_data_io         = 1'bz,
    inout wire  ps2_mouse_clk_io  = 1'bz,
    inout wire  ps2_mouse_data_io = 1'bz,

    // SD Card
    output wire sd_cs_n_o           = 1'b1,
    output wire sd_sclk_o           = 1'b0,
    output wire sd_mosi_o           = 1'b0,
    input wire  sd_miso_i,

    // Joysticks
   output wire joy_clock_o         = 1'b1,
   output wire joy_load_o          = 1'b1,
   input  wire joy_data_i,
   output wire joy_p7_o            = 1'b1,

    // Audio
    output        AUDIO_L,
    output        AUDIO_R,
    input wire  ear_i,
    output wire mic_o                   = 1'b0,

        // VGA
    output  [4:0] VGA_R,
    output  [4:0] VGA_G,
    output  [4:0] VGA_B,
    output        VGA_HS,
    output        VGA_VS,

        //STM32
    input wire  stm_tx_i,
    output wire stm_rx_o,
    output wire stm_rst_o           = 1'bz, // '0' to hold the microcontroller reset line, to free the SD card

    input         SPI_SCK,
    output        SPI_DO,
    input         SPI_DI,
    input         SPI_SS2,
   output wire   SPI_WAIT        = 1'b1, // '0' to hold the microcontroller data streaming
   inout [31:0] GPIO,
   output LED                    = 1'b1 // '0' is LED on
);

`include "..\..\rtl\build_id.v" 

localparam CONF_STR = {
    "P,CORE_NAME.dat;", // 16
//    "P,GreenBeret.dat;",
    "S,DAT,Alternative ROM...;", //25
    "O34,Scanlines,Off,25%,50%,75%;",
    "O5,Blend,Off,On;", // 16
    "O7,Scandoubler,On,Off;",
    "O1,Double Video Buffer,On,Off;",
    "O89,Lives,2,3,5,7;", // 18
    "OAB,Extend,30k/ev.70k,40k/ev.80k,50k/ev.100k;50k/ev.200k;", // 56
    "OCD,Difficulty,Easy,Medium,Hard,Hardest;", // 40 
    "OE,Demo Sound,Off,On;", // 21
    "T0,Reset;", // 9
    "V,v1.00." // 8
};

wire  [1:0] dsLives   = ~status[9:8];
wire  [1:0] dsExtend  = ~status[11:10];
wire  [1:0] dsDiff    = ~status[13:12];
wire        dsDemoSnd = ~status[14];

assign GPIO = 32'bzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzz;
assign      LED = ~ioctl_downl;
assign      stm_rst_o = 1'bz;
assign      sram_we_n_o = 1'b1;
assign      sram_oe_n_o = 1'b1;

assign      AUDIO_R = AUDIO_L;
//assign        SDRAM_CLK = clock_48;
assign      SDRAM_CKE = 1;


wire joy1_up_i, joy1_down_i, joy1_left_i, joy1_right_i, joy1_p6_i, joy1_p9_i;
wire joy2_up_i, joy2_down_i, joy2_left_i, joy2_right_i, joy2_p6_i, joy2_p9_i;

wire kbd_clk;

always @(posedge clock_24) begin
    kbd_clk <= ~kbd_clk;
end

joystick_serial  joystick_serial 
(
    .clk_i           ( clock_48 ),
    .joy_data_i      ( joy_data_i ),
    .joy_clk_o       ( joy_clock_o ),
    .joy_load_o      ( joy_load_o ),

    .joy1_up_o       ( joy1_up_i ),
    .joy1_down_o     ( joy1_down_i ),
    .joy1_left_o     ( joy1_left_i ),
    .joy1_right_o    ( joy1_right_i ),
    .joy1_fire1_o    ( joy1_p6_i ),
    .joy1_fire2_o    ( joy1_p9_i ),

    .joy2_up_o       ( joy2_up_i ),
    .joy2_down_o     ( joy2_down_i ),
    .joy2_left_o     ( joy2_left_i ),
    .joy2_right_o    ( joy2_right_i ),
    .joy2_fire1_o    ( joy2_p6_i ),
    .joy2_fire2_o    ( joy2_p9_i )
);

wire clock_48, clock_24, pll_locked;
pll pll(
    .inclk0(clock_50_i),
    .c0(clock_48),
    .c1(SDRAM_CLK),
    .c2(clock_24),
    .locked(pll_locked)
    );

wire clk_25m2,clk_40;
pll_vga pll_vga(
    .inclk0(clock_50_i),
    .c0(clk_25m2),
    .c1(clk_40)
    );

wire [31:0] status;
wire  [1:0] buttons;
wire  [1:0] switches;
wire  [7:0] joystick_0;
wire  [7:0] joystick_1;
wire        scandoublerD;
wire  [7:0] audio;
wire        hs, vs;
wire        hb, vb;
wire        blankn = ~(hb | vb);
wire [3:0]  r, g, b;
wire        key_strobe;
wire        key_pressed;
wire  [7:0] key_code;
wire [15:0] rom_addr;
wire [15:0] rom_do;
wire [15:1] spr_addr;
wire [15:0] spr_do;

wire        ioctl_downl;
wire  [7:0] ioctl_index;
wire        ioctl_wr;
wire [24:0] ioctl_addr;
wire  [7:0] ioctl_dout;

// Don't delete!
// ROM structure
// 00000 - 0FFFF - maincpu -  64k - 10c+8c+7c+7c
// 10000 - 1FFFF - gfx2    -  64k - 5e+4e+4f+3e
// 20000 - 23FFF - gfx1    -  16k - 3f
// 24000 - 240FF - sprites - 256b - 5f
// 24100 - 241FF - chars   - 256b - 6f
// 24200 - 2421F - pal     -  32b - 2f

data_io #(.STRLEN($size(CONF_STR)>>3)) data_io(
    .clk_sys       ( clock_48      ),
    .SPI_SCK       ( SPI_SCK      ),
    .SPI_SS2       ( SPI_SS2      ),
    .SPI_DI        ( SPI_DI       ),
    .SPI_DO        ( SPI_DO       ),
    
    .data_in       ( pump_s & keys_s ),
    .conf_str      ( CONF_STR ),
    .status        ( status ),
    
    .ioctl_download( ioctl_downl  ),
    .ioctl_index   ( ioctl_index  ),
    .ioctl_wr      ( ioctl_wr     ),
    .ioctl_addr    ( ioctl_addr   ),
    .ioctl_dout    ( ioctl_dout   )
);

wire [24:0] bg_ioctl_addr = ioctl_addr - 17'h10000;

reg port1_req, port2_req;
sdram sdram(
    .*,
    .init_n        ( pll_locked   ),
    .clk           ( clock_48      ),

    .port1_req     ( port1_req    ),
    .port1_ack     ( ),
    .port1_a       ( ioctl_addr[23:1] ),
    .port1_ds      ( {ioctl_addr[0], ~ioctl_addr[0]} ),
    .port1_we      ( ioctl_downl ),
    .port1_d       ( {ioctl_dout, ioctl_dout} ),
    .port1_q       ( ),

    .cpu1_addr     ( ioctl_downl ? 16'hffff : {1'b0, rom_addr[15:1]} ),
    .cpu1_q        ( rom_do ),

    // port2 for sprite graphics
    .port2_req     ( port2_req ),
    .port2_ack     ( ),
    .port2_a       ( bg_ioctl_addr[23:1] ),
    .port2_ds      ( {bg_ioctl_addr[0], ~bg_ioctl_addr[0]} ),
    .port2_we      ( ioctl_downl ),
    .port2_d       ( {ioctl_dout, ioctl_dout} ),
    .port2_q       ( ),

    .spr_addr      ( ioctl_downl ? 15'h7fff : spr_addr ),
    .spr_q         ( spr_do )
);

// ROM download controller
always @(posedge clock_48) begin
    reg        ioctl_wr_last = 0;

    ioctl_wr_last <= ioctl_wr;
    if (ioctl_downl) begin
        if (~ioctl_wr_last && ioctl_wr) begin
            port1_req <= ~port1_req;
            port2_req <= ~port2_req;
        end
    end
end

reg reset = 1;
reg rom_loaded = 0;
always @(posedge clock_48) begin
    reg ioctl_downlD;
    ioctl_downlD <= ioctl_downl;
    if (ioctl_downlD & ~ioctl_downl) rom_loaded <= 1;
    reset <= status[0] | ~btn_n_i[4] | ioctl_downl | ~rom_loaded;
end

//////////////////////////////////////////////

wire        PCLK;
wire        PCLK_EN;
wire  [8:0] HPOS,VPOS;
wire [11:0] POUT;

HVGEN hvgen
(
    .HPOS(HPOS),.VPOS(VPOS),.CLK(clock_48),.PCLK_EN(PCLK_EN),.iRGB(POUT),
    .oRGB({b,g,r}),.HBLK(hb),.VBLK(vb),.HSYN(hs),.VSYN(vs)
);

wire  [5:0] INP0 = { m_fireB, m_fireA, {m_left, m_down, m_right, m_up} };
wire  [5:0] INP1 = { m_fireB, m_fireA, {m_left, m_down, m_right, m_up} };
wire  [2:0] INP2 = { btn_coin, btn_two_players, btn_one_player };

wire  [7:0] DSW0 = {dsDemoSnd,dsDiff,dsExtend,1'b0,dsLives};
wire  [7:0] DSW1 = 8'hFF;
wire  [7:0] DSW2 = 8'hFF;

FPGA_GreenBeret GameCore (
    .reset(reset),.clk48M(clock_48),
    .INP0(INP0),.INP1(INP1),.INP2(INP2),
    .DSW0(DSW0),.DSW1(DSW1),.DSW2(DSW2),

    .PH(HPOS),.PV(VPOS),.PCLK(PCLK),.PCLK_EN(PCLK_EN),.POUT(POUT),
    .SND(audio),

    .CPU_ROMA(rom_addr), .CPU_ROMDT(rom_addr[0] ? rom_do[15:8] : rom_do[7:0]),
    .SP_ROMA(spr_addr), .SP_ROMD(spr_do),
    .ROMCL(clock_48),.ROMAD(ioctl_addr),.ROMDT(ioctl_dout),.ROMEN(ioctl_wr)
);

//////////////////////////////////////////////
wire [11:0] vga_col_s;
wire vga_hs_s, vga_vs_s;

framebuffer #(240,224,12,1) framebuffer
(
        .clk_sys    ( clock_48 ),
        .clk_i      ( PCLK_EN ),
        .RGB_i      ( (blankn) ? {r,g,b} : 12'b000000000000 ),
        .hblank_i   ( hb ),
        .vblank_i   ( vb ),
        .dis_db_i   ( status[1] ),        
        .rotate_i   ( 2'b00 ), 

        .clk_vga_i  ( clk_25m2 ), //640x480
        .RGB_o      ( vga_col_s ),
        .hsync_o    ( vga_hs_s ),
        .vsync_o    ( vga_vs_s ),
        .blank_o    (  ),

        .odd_line_o (  )
);

wire direct_video_s = ~status[7] ^ direct_video;
mist_video #(.COLOR_DEPTH(4), .SD_HCNT_WIDTH(10), .USE_FRAMEBUFFER(1)) mist_video(

    .SPI_SCK        ( SPI_SCK          ),
    .SPI_SS3        ( SPI_SS2          ),
    .SPI_DI         ( SPI_DI           ),
/*
    .clk_sys        ( clock_48         ),
    .R              ( blankn ? r : 0   ),
    .G              ( blankn ? g : 0   ),
    .B              ( blankn ? b : 0   ),
    .HSync          ( hs               ),
    .VSync          ( vs               ),
    .scandoubler_disable( scandoublerD ), 
*/       
    .clk_sys        ( direct_video_s ? clock_48 : clk_25m2 ),
    .R      ( (direct_video_s) ? (blankn) ? r  : 4'b0000 : vga_col_s[11:8] ),
    .G      ( (direct_video_s) ? (blankn) ? g  : 4'b0000 : vga_col_s[7:4] ),
    .B      ( (direct_video_s) ? (blankn) ? b  : 4'b0000 : vga_col_s[3:0] ),
    .HSync  ( (direct_video_s) ? hs : vga_hs_s ),
    .VSync  ( (direct_video_s) ? vs : vga_vs_s ),
    .scandoubler_disable( direct_video_s ), 
    .no_csync       ( ~direct_video_s ),

    .VGA_R          ( VGA_R            ),
    .VGA_G          ( VGA_G            ),
    .VGA_B          ( VGA_B            ),
    .VGA_VS         ( VGA_VS           ),
    .VGA_HS         ( VGA_HS           ),
    .rotate         ( {1'b0,1'b0}      ),

    .ce_divider     ( 1'b1 ),
    .blend          ( status[5]        ),
    .scanlines      ( status[4:3]      ),
    .osd_enable     ( osd_enable       )
    );


/*
user_io #(.STRLEN(($size(CONF_STR)>>3)))user_io(
    .clk_sys        ( clock_48         ),
    .conf_str       ( CONF_STR         ),
    .SPI_CLK        ( SPI_SCK          ),
    .SPI_SS_IO      ( CONF_DATA0       ),
    .SPI_MISO       ( SPI_DO           ),
    .SPI_MOSI       ( SPI_DI           ),
    .buttons        ( buttons          ),
    .switches       ( switches         ),
    .scandoubler_disable ( scandoublerD ),
    .ypbpr          ( ypbpr            ),
    .key_strobe     ( key_strobe       ),
    .key_pressed    ( key_pressed      ),
    .key_code       ( key_code         ),
    .joystick_0     ( joystick_0       ),
    .joystick_1     ( joystick_1       ),
    .status         ( status           )
    );
*/

dac #(8) dac(
    .clk_i(clock_48),
    .res_n_i(1),
    .dac_i(audio),
    .dac_o(AUDIO_L)
    );

reg [7:0] pump_s = 8'b11111111;
PumpSignal PumpSignal (clock_48, ~pll_locked, ioctl_downl, pump_s);



wire m_up, m_down, m_left, m_right, m_fireA, m_fireB, m_fireC, m_fireD, m_fireE, m_fireF, m_fireG;
wire m_up2, m_down2, m_left2, m_right2, m_fire2A, m_fire2B, m_fire2C, m_fire2D, m_fire2E, m_fire2F, m_fire2G;
wire m_tilt, m_coin1, m_coin2, m_coin3, m_coin4, m_one_player, m_two_players, m_three_players, m_four_players;

wire m_right4, m_left4, m_down4, m_up4, m_right3, m_left3, m_down3, m_up3;

wire btn_one_player =   ~btn_n_i[1] | m_one_player;
wire btn_two_players =  ~btn_n_i[2] | m_two_players;
wire btn_coin  =        ~btn_n_i[3] | m_coin1;

wire kbd_intr;
wire [7:0] kbd_scancode;
wire [7:0] keys_s;

//get scancode from keyboard
io_ps2_keyboard #(.CLK_SPEED(24000)) keyboard 
 (
  .clk       ( clock_24 ),
  .kbd_clk   ( ps2_clk_io ),
  .kbd_dat   ( ps2_data_io ),
  .interrupt ( kbd_intr ),
  .scancode  ( kbd_scancode )
);

wire [15:0]joy1_s;
wire [15:0]joy2_s;
wire [8:0]controls_s;
wire osd_enable, direct_video;

//translate scancode to joystick
kbd_joystick #( .OSD_CMD    ( 3'b011 ), .USE_VKP( 1'b1), .CLK_SPEED(24000)) k_joystick
(
  .clk          ( clock_24 ),
  .kbdint       ( kbd_intr ),
  .kbdscancode  ( kbd_scancode ), 
  
    .joystick_0     ({ joy1_p9_i, joy1_p6_i, joy1_up_i, joy1_down_i, joy1_left_i, joy1_right_i }),
    .joystick_1     ({ joy2_p9_i, joy2_p6_i, joy2_up_i, joy2_down_i, joy2_left_i, joy2_right_i }),
          
    //-- joystick_0 and joystick_1 should be swapped
    .joyswap        ( 0 ),
        
    //-- player1 and player2 should get both joystick_0 and joystick_1
    .oneplayer      ( 1 ),

    //-- tilt, coin4-1, start4-1
    .controls    ( {m_tilt, m_coin4, m_coin3, m_coin2, m_coin1, m_four_players, m_three_players, m_two_players, m_one_player} ),
        
    //-- fire12-1, up, down, left, right

    .player1     ( {m_fireG,  m_fireF, m_fireE, m_fireD, m_fireC, m_fireB, m_fireA, m_up, m_down, m_left, m_right} ),
    .player2     ( {m_fire2G, m_fire2F, m_fire2E, m_fire2D, m_fire2C, m_fire2B, m_fire2A, m_up2, m_down2, m_left2, m_right2} ),
        
    //-- keys to the OSD
    .osd_o         ( keys_s ),
    .osd_enable     ( osd_enable ),
    .direct_video ( direct_video ),
    
    //-- sega joystick
    //--.sega_clk       ( hs ),
    .sega_strobe    ( joy_p7_o )
        
        
);

endmodule
