--
-- Multicore 2 / Multicore 2+
--
-- Copyright (c) 2017-2020 - Victor Trucco
--
-- All rights reserved
--
-- Redistribution and use in source and synthezised forms, with or without
-- modification, are permitted provided that the following conditions are met:
--
-- Redistributions of source code must retain the above copyright notice,
-- this list of conditions and the following disclaimer.
--
-- Redistributions in synthesized form must reproduce the above copyright
-- notice, this list of conditions and the following disclaimer in the
-- documentation and/or other materials provided with the distribution.
--
-- Neither the name of the author nor the names of other contributors may
-- be used to endorse or promote products derived from this software without
-- specific prior written permission.
--
-- THIS CODE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
-- AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
-- THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
-- PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE
-- LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
-- CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
-- SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
-- INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
-- CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
-- ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
-- POSSIBILITY OF SUCH DAMAGE.
--
-- You are responsible for any legal issues arising from your use of this code.
--
		
-- Gear Shift
-- (c) 2019 alanswx


library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.STD_LOGIC_ARITH.all;
use IEEE.STD_LOGIC_UNSIGNED.all;

entity gearshift is 
port(   
			Clk				: in	std_logic;
			reset				: in	std_logic;
			gearup			: in	std_logic;
			geardown			: in	std_logic;
			gearout			: out std_logic_vector(2 downto 0);
			gear1				: out std_logic;
			gear2				: out std_logic;
			gear3				: out std_logic
			
			);
end gearshift;

architecture rtl of gearshift is

signal gear : std_logic_vector(2 downto 0):= (others =>'0');
signal old_gear_up : std_logic:='0';
signal old_gear_down : std_logic:='0';


begin

gearout<=gear;

process (clk, gear)
begin

  if rising_edge(clk) then


  if (reset='1') then
		gear<="000";
  elsif (gearup='1') then
   if (old_gear_up='0') then
		old_gear_up<='1';
		if (gear < 3) then
			gear<= gear +1;
		end if;
	end if;
  elsif (geardown='1') then
   if (old_gear_down='0') then
	   old_gear_down<='1';
		if (gear>0) then
			gear<=gear-1;
		end if;
	end if;
  else
    old_gear_up<='0';
	 old_gear_down<='0';
  end if;

  end if;

   case gear is
        when "000" => gear1 <=  '0' ;
        when "001" => gear1 <=  '1' ;
        when "010" => gear1 <=  '1' ;
        when "011" => gear1 <=  '1' ;
        when others => gear1 <= '1' ;
    end case;
   case gear is
        when "000" => gear2 <=  '1' ;
        when "001" => gear2 <=  '0' ;
        when "010" => gear2 <=  '1' ;
        when "011" => gear2 <=  '1' ;
        when others => gear2 <= '1' ;
    end case;
   case gear is
        when "000" => gear3 <=  '1' ;
        when "001" => gear3 <=  '1' ;
        when "010" => gear3 <=  '0' ;
        when "011" => gear3 <=  '1' ;
        when others => gear3 <= '1' ;
    end case;
	
end process;


end rtl;