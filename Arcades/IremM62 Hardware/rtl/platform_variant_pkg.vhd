--
-- Multicore 2 / Multicore 2+
--
-- Copyright (c) 2017-2020 - Victor Trucco
--
-- All rights reserved
--
-- Redistribution and use in source and synthezised forms, with or without
-- modification, are permitted provided that the following conditions are met:
--
-- Redistributions of source code must retain the above copyright notice,
-- this list of conditions and the following disclaimer.
--
-- Redistributions in synthesized form must reproduce the above copyright
-- notice, this list of conditions and the following disclaimer in the
-- documentation and/or other materials provided with the distribution.
--
-- Neither the name of the author nor the names of other contributors may
-- be used to endorse or promote products derived from this software without
-- specific prior written permission.
--
-- THIS CODE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
-- AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
-- THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
-- PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE
-- LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
-- CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
-- SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
-- INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
-- CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
-- ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
-- POSSIBILITY OF SUCH DAMAGE.
--
-- You are responsible for any legal issues arising from your use of this code.
--
		
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;

library work;

use work.platform_pkg.all;

package platform_variant_pkg is


  constant HW_LDRUN    : integer := 0;
  constant HW_LDRUN2   : integer := 1;
  constant HW_LDRUN3   : integer := 2;
  constant HW_LDRUN4   : integer := 3;
  constant HW_KUNGFUM  : integer := 4;
  constant HW_HORIZON  : integer := 5;
  constant HW_BATTROAD : integer := 6;
  constant HW_KIDNIKI  : integer := 7;
  constant HW_LOTLOT   : integer := 8;
  constant HW_SPELUNKR : integer := 9;
  constant HW_SPELUNK2 : integer := 10;
  constant HW_YOUJYUDN : integer := 11;

  subtype HWSEL_t is integer range 0 to 11;

  type rom_a is array (natural range <>) of string;

  type pal_rgb_t is array (0 to 2) of std_logic_vector(7 downto 0);
  type pal_a is array (natural range <>) of pal_rgb_t;

  -- table of sprite heights
  type prom_a is array (natural range <>) of integer range 0 to 3;

end package platform_variant_pkg;
