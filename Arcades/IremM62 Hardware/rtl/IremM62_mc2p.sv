/*
  
   Multicore 2 / Multicore 2+
  
   Copyright (c) 2017-2020 - Victor Trucco

  
   All rights reserved
  
   Redistribution and use in source and synthezised forms, with or without
   modification, are permitted provided that the following conditions are met:
  
   Redistributions of source code must retain the above copyright notice,
   this list of conditions and the following disclaimer.
  
   Redistributions in synthesized form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
  
   Neither the name of the author nor the names of other contributors may
   be used to endorse or promote products derived from this software without
   specific prior written permission.
  
   THIS CODE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
   AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
   THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
   PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE
   LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
   CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
   INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
   CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
   ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
   POSSIBILITY OF SUCH DAMAGE.
  
   You are responsible for any legal issues arising from your use of this code.
  
*///============================================================================
//
//  Multicore 2+ Top by Victor Trucco
//
//============================================================================

`default_nettype none

module IremM62_MC2P(
     // Clocks
    input wire  clock_50_i,

    // Buttons
    input wire [4:1]    btn_n_i,

    // SRAM (IS61WV20488FBLL-10)
    output wire [20:0]sram_addr_o  = 21'b000000000000000000000,
    inout wire  [7:0]sram_data_io   = 8'bzzzzzzzz,
    output wire sram_we_n_o     = 1'b1,
    output wire sram_oe_n_o     = 1'b1,
        
    // SDRAM (W9825G6KH-6)
    output [12:0] SDRAM_A,
    output  [1:0] SDRAM_BA,
    inout  [15:0] SDRAM_DQ,
    output        SDRAM_DQMH,
    output        SDRAM_DQML,
    output        SDRAM_CKE,
    output        SDRAM_nCS,
    output        SDRAM_nWE,
    output        SDRAM_nRAS,
    output        SDRAM_nCAS,
    output        SDRAM_CLK,

    // PS2
    inout wire  ps2_clk_io        = 1'bz,
    inout wire  ps2_data_io       = 1'bz,
    inout wire  ps2_mouse_clk_io  = 1'bz,
    inout wire  ps2_mouse_data_io = 1'bz,

    // SD Card
    output wire sd_cs_n_o         = 1'bZ,
    output wire sd_sclk_o         = 1'bZ,
    output wire sd_mosi_o         = 1'bZ,
    input wire  sd_miso_i,

    // Joysticks
    output wire joy_clock_o       = 1'b1,
    output wire joy_load_o        = 1'b1,
    input  wire joy_data_i,
    output wire joy_p7_o          = 1'b1,

    // Audio
    output      AUDIO_L,
    output      AUDIO_R,
    input wire  ear_i,
    output wire mic_o             = 1'b0,

    // VGA
    output  [4:0] VGA_R,
    output  [4:0] VGA_G,
    output  [4:0] VGA_B,
    output        VGA_HS,
    output        VGA_VS,

    //STM32
    input wire  stm_tx_i,
    output wire stm_rx_o,
    output wire stm_rst_o           = 1'bz, // '0' to hold the microcontroller reset line, to free the SD card
   
    input         SPI_SCK,
    output        SPI_DO,
    input         SPI_DI,
    input         SPI_SS2,
    output wire   SPI_nWAIT        = 1'b1, // '0' to hold the microcontroller data streaming

    inout [31:0] GPIO,

    output LED                    = 1'b1 // '0' is LED on
);

`include "../rtl/build_id.v" 

`define CORE_NAME "LDRUN"
wire [6:0] core_mod;

localparam CONF_STR = {
    "P,CORE_NAME.dat;",
//    "P,Kung Fu Master.dat;",
    "S,DAT,Alternative ROM...;",
 //   "O1,Video Timings,PAL 50Hz,Original;",
    "O34,Scanlines,Off,25%,50%,75%;",
    "O5,Blending,Off,On;",
    "OG,Scandoubler,On,Off;",
    "O7,Service,Off,On;",
    "T0,Reset;",
//    "OF,Patrons list,Off,On;",
    "V,v1.0.",`BUILD_DATE
};


//---------------------------------------------------------
//-- MC2+ defaults
//---------------------------------------------------------

assign GPIO = 32'bzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzz;
assign stm_rst_o    = 1'bZ;
assign stm_rx_o = 1'bZ;

//no SRAM for this core
assign sram_we_n_o  = 1'b1;
assign sram_oe_n_o  = 1'b1;

//all the SD reading goes thru the microcontroller for this core
assign sd_cs_n_o = 1'bZ;
assign sd_sclk_o = 1'bZ;
assign sd_mosi_o = 1'bZ;

wire joy1_up_i, joy1_down_i, joy1_left_i, joy1_right_i, joy1_p6_i, joy1_p9_i;
wire joy2_up_i, joy2_down_i, joy2_left_i, joy2_right_i, joy2_p6_i, joy2_p9_i;

joystick_serial  joystick_serial 
(
    .clk_i           ( clk_sys ),
    .joy_data_i      ( joy_data_i ),
    .joy_clk_o       ( joy_clock_o ),
    .joy_load_o      ( joy_load_o ),

    .joy1_up_o       ( joy1_up_i ),
    .joy1_down_o     ( joy1_down_i ),
    .joy1_left_o     ( joy1_left_i ),
    .joy1_right_o    ( joy1_right_i ),
    .joy1_fire1_o    ( joy1_p6_i ),
    .joy1_fire2_o    ( joy1_p9_i ),

    .joy2_up_o       ( joy2_up_i ),
    .joy2_down_o     ( joy2_down_i ),
    .joy2_left_o     ( joy2_left_i ),
    .joy2_right_o    ( joy2_right_i ),
    .joy2_fire1_o    ( joy2_p6_i ),
    .joy2_fire2_o    ( joy2_p9_i )
);

reg [7:0] pump_s = 8'b11111111;
PumpSignal PumpSignal (clk_sys, ~pll_locked, ioctl_downl, pump_s);

//-- END defaults -------------------------------------------------------


wire       palmode   = 0; //~status[1];
wire       rotate    = status[2];
wire [1:0] scanlines = status[4:3];
wire       blend     = status[5];
wire       service   = status[7];
wire       joyswap   = status[6];

reg  [1:0] orientation = 2'b10;
reg        oneplayer;
wire [7:0] DSW1 = {/*coinage*/4'hf, ~status[11:8]};

reg signed [11:0] adj_x;
reg signed [11:0] adj_y;

reg signed [11:0] scroll;

always @(*) begin

 //   core_mod = 7'd4;

  orientation = 2'b00;
  oneplayer = 1;
  
    adj_x = -600;
    adj_y = -500;
    
    scroll = +11'd1;    

  case (core_mod)
  7'h3: oneplayer = 0; // LDRUN4
  
  7'h6: begin
            orientation = 2'b11; // BATTROAD
            adj_x = -1200;
            adj_y = 500;
        end 
        
  7'hB: begin 
            orientation = 2'b01; // YOUJYUDN
            adj_x = 1000; //-1600 e 1800 demoram muito
            adj_y = 400;
            scroll = -11'd1;    
        end
            
  default: ;
  endcase
end

assign SDRAM_CLK = clk_sd;
assign SDRAM_CKE = 1; 

wire clk_sys, clk_vid, clk_aud, clk_sd;
wire pll_locked;
pll_mist pll(
    .inclk0(clock_50_i),
    .areset(0),
    .c0(clk_sd),
    .c1(clk_sys),
    .c2(clk_vid),
    .c3(clk_aud),
    .locked(pll_locked)
    );

wire [31:0] status;
wire  [1:0] buttons;
wire  [1:0] switches;
wire  [7:0] joystick_0;
wire  [7:0] joystick_1;
wire        scandoublerD;
wire        ypbpr;
wire        no_csync;
wire        key_pressed;
wire  [7:0] key_code;
wire        key_strobe;
/*
user_io #(
    .STRLEN(($size(CONF_STR)>>3)),
    .ROM_DIRECT_UPLOAD(1'b1))
user_io(
    .clk_sys        (clk_sys        ),
    .conf_str       (CONF_STR       ),
    .SPI_CLK        (SPI_SCK        ),
    .SPI_SS_IO      (CONF_DATA0     ),
    .SPI_MISO       (SPI_DO         ),
    .SPI_MOSI       (SPI_DI         ),
    .buttons        (buttons        ),
    .switches       (switches       ),
    .scandoubler_disable (scandoublerD    ),
    .ypbpr          (ypbpr          ),
    .no_csync       (no_csync       ),
    .core_mod       (core_mod       ),
    .key_strobe     (key_strobe     ),
    .key_pressed    (key_pressed    ),
    .key_code       (key_code       ),
    .joystick_0     (joystick_0     ),
    .joystick_1     (joystick_1     ),
    .status         (status         )
    );
*/
wire [16:0] rom_addr;
wire [15:0] rom_do;

wire [17:0] snd_addr;
wire [15:0] snd_rom_addr;
wire [15:0] snd_do;
wire        snd_vma;

wire [14:0] chr1_addr;
wire [31:0] chr1_do;
wire [15:0] sp_addr;
wire [31:0] sp_do;
wire [14:0] chr2_addr;
wire [31:0] chr2_do;

/* ROM structure
00000-1FFFF CPU1 128k
20000-2FFFF CPU2  64k
30000-4FFFF GFX1 128k
50000-8FFFF GFX2 256k

90000-9FFFF GFX3  64k
A0000-A02FF spr_color_proms 3*256b
A0300-A05FF chr_color_proms 3*256b
A0600-A08FF fg_color_proms  3*256b
A0900-A091F spr_height_prom 32b
*/

wire        ioctl_downl;
wire  [7:0] ioctl_index;
wire        ioctl_wr;
wire [24:0] ioctl_addr;
wire  [7:0] ioctl_dout;

data_io #(
    .STRLEN(($size(CONF_STR)>>3)))
data_io(
    .clk_sys       ( clk_sys      ),
    .SPI_SCK       ( SPI_SCK      ),
    .SPI_SS2       ( SPI_SS2      ),
    .SPI_DI        ( SPI_DI       ),
    .SPI_DO        ( SPI_DO       ),
    
    .data_in       ( pump_s & keys_s ),
    .conf_str      ( CONF_STR     ),
    .status        ( status       ),
    .core_mod      ( core_mod     ),
    
    .ioctl_download( ioctl_downl  ),
    .ioctl_index   ( ioctl_index  ),
    .ioctl_wr      ( ioctl_wr     ),
    .ioctl_addr    ( ioctl_addr   ),
    .ioctl_dout    ( ioctl_dout   )
);

wire [24:0] sp_ioctl_addr = ioctl_addr - 20'h30000;
wire clkref;

reg port1_req, port2_req;
sdram sdram(
    .*,
    .init_n        ( pll_locked   ),
    .clk           ( clk_sd       ),
    .clkref        ( clkref       ),

    // port1 used for main + sound CPU
    .port1_req     ( port1_req    ),
    .port1_ack     ( ),
    .port1_a       ( ioctl_addr[23:1] ),
    .port1_ds      ( {ioctl_addr[0], ~ioctl_addr[0]} ),
    .port1_we      ( ioctl_downl ),
    .port1_d       ( {ioctl_dout, ioctl_dout} ),
    .port1_q       ( ),

    .cpu1_addr     ( ioctl_downl ? 17'h1ffff : {1'b0, rom_addr[16:1]} ),
    .cpu1_q        ( rom_do ),
    .cpu2_addr     ( ioctl_downl ? 17'h1ffff : snd_addr[17:1] ),
    .cpu2_q        ( snd_do ),

    // port2 for sprite graphics
    .port2_req     ( port2_req ),
    .port2_ack     ( ),
    .port2_a       ( sp_ioctl_addr[23:1] ),
    .port2_ds      ( {sp_ioctl_addr[0], ~sp_ioctl_addr[0]} ),
    .port2_we      ( ioctl_downl ),
    .port2_d       ( {ioctl_dout, ioctl_dout} ),
    .port2_q       ( ),

    .chr1_addr     ( chr1_addr ),
    .chr1_q        ( chr1_do   ),
    .chr2_addr     ( 17'h18000 + chr2_addr ),
    .chr2_q        ( chr2_do   ),
    .sp_addr       ( 16'h8000 + sp_addr ),
    .sp_q          ( sp_do     )
);

// ROM download controller
always @(posedge clk_sys) begin
    reg        ioctl_wr_last = 0;
    reg        snd_vma_r, snd_vma_r2;

    ioctl_wr_last <= ioctl_wr;
    if (ioctl_downl) begin
        if (~ioctl_wr_last && ioctl_wr) begin
            port1_req <= ~port1_req;
            if (ioctl_addr >= 20'h30000) port2_req <= ~port2_req;
        end
    end

    // async clock domain crossing here (clk_snd -> clk_sys)
    snd_vma_r <= snd_vma; snd_vma_r2 <= snd_vma_r;
    if (snd_vma_r2) snd_addr <= snd_rom_addr + 18'h20000;
end

// reset signal generation
reg reset = 1;
reg rom_loaded = 0;
always @(posedge clk_sys) begin
    reg ioctl_downlD;
    reg [15:0] reset_count;
    ioctl_downlD <= ioctl_downl;

    if (status[0] | ~btn_n_i[4] | ~rom_loaded) reset_count <= 16'hffff;
    else if (reset_count != 0) reset_count <= reset_count - 1'd1;

    if (ioctl_downlD & ~ioctl_downl) rom_loaded <= 1;
    reset <= reset_count != 16'h0000;

end

wire [11:0] audio;
wire        hs, vs;
wire        blankn = 1'b1;//todo
wire  [3:0] g,b,r;

target_top target_top(
    .clock_sys(clk_sys),//24 MHz
    .vid_clk_en(clkref),
    .clk_aud(clk_aud),//0.895MHz
    .reset_in(reset),
    .hwsel(core_mod),
    .palmode(palmode),
    .audio_out(audio),
    .switches_i(DSW1),
    .usr_coin1(btn_coin),
    .usr_coin2(m_coin2),
    .usr_service(service),
    .usr_start1(btn_one_player),
    .usr_start2(btn_two_players),
    .p1_up(m_up),
    .p1_dw(m_down),
    .p1_lt(m_left),
    .p1_rt(m_right),
    .p1_f1(m_fireA),
    .p1_f2(m_fireB),
    .p2_up(m_up2),
    .p2_dw(m_down2),
    .p2_lt(m_left2),
    .p2_rt(m_right2),
    .p2_f1(m_fire2A),
    .p2_f2(m_fire2B),
    .VGA_VS(vs),
    .VGA_HS(hs),
    .VGA_R(r),
    .VGA_G(g),
    .VGA_B(b),

    .dl_addr(ioctl_addr - 20'hA0000),
    .dl_data(ioctl_dout),
    .dl_wr(ioctl_wr),

    .cpu_rom_addr(rom_addr),
    .cpu_rom_do( rom_addr[0] ? rom_do[15:8] : rom_do[7:0] ),
    .snd_rom_addr(snd_rom_addr),
    .snd_rom_do(snd_rom_addr[0] ? snd_do[15:8] : snd_do[7:0]),
    .snd_vma(snd_vma),
    .gfx1_addr(chr1_addr),
    .gfx1_do(chr1_do),
    .gfx3_addr(chr2_addr),
    .gfx3_do(chr2_do),
    .gfx2_addr(sp_addr),
    .gfx2_do(sp_do)
  ); 

wire scandoubler_disable = ~status[16] ^ direct_video;
wire vga_hs_o,vga_vs_o;

mist_video #(.COLOR_DEPTH(4), .SD_HCNT_WIDTH(11)) mist_video(
    .clk_sys        ( clk_vid          ),
    .SPI_SCK        ( SPI_SCK          ),
    .SPI_SS3        ( SPI_SS2          ),
    .SPI_DI         ( SPI_DI           ),
    
    .R              ( blankn ? r : 0   ),
    .G              ( blankn ? g : 0   ),
    .B              ( blankn ? b : 0   ),
    .HSync          ( ~hs               ),
    .VSync          ( ~vs               ),
    
    .VGA_R          ( VGA_R          ),
    .VGA_G          ( VGA_G          ),
    .VGA_B          ( VGA_B          ),
    .VGA_VS         ( vga_vs_o         ),
    .VGA_HS         ( vga_hs_o         ),
    
    .rotate         ( orientation ),
    .ce_divider     ( 1'b1             ),
    .scandoubler_disable( scandoubler_disable ),
    .scanlines      ( scanlines        ),
    .blend          ( blend            ),
    
//    .patrons        ( status[15]      ),    
//    .PATRON_ADJ_X   ( adj_x ),
//    .PATRON_ADJ_Y   ( adj_y ), 
//    .PATRON_DOUBLE_WIDTH ( 1 ),
//    .PATRON_DOUBLE_HEIGHT ( 1 ),
//    .PATRON_SCROLL  ( scroll ), 
    .osd_enable      ( osd_enable )
    
    );

assign VGA_HS = (scandoubler_disable) ? ~(vga_hs_o ^ vga_vs_o) : vga_hs_o;
assign VGA_VS = (scandoubler_disable) ? 1'b1                   : vga_vs_o;


wire dac_o;
assign AUDIO_L = dac_o;
assign AUDIO_R = dac_o;

dac #(
    .C_bits(12))
dac(
    .clk_i(clk_aud),
    .res_n_i(1),
    .dac_i(audio),
    .dac_o(dac_o)
    );
    

//-----------------------

wire m_up, m_down, m_left, m_right, m_fireA, m_fireB, m_fireC, m_fireD, m_fireE, m_fireF, m_fireG;
wire m_up2, m_down2, m_left2, m_right2, m_fire2A, m_fire2B, m_fire2C, m_fire2D, m_fire2E, m_fire2F, m_fire2G;
wire m_tilt, m_coin1, m_coin2, m_coin3, m_coin4, m_one_player, m_two_players, m_three_players, m_four_players;

wire m_right4, m_left4, m_down4, m_up4, m_right3, m_left3, m_down3, m_up3;

wire btn_one_player =   ~btn_n_i[1] | m_one_player;
wire btn_two_players =  ~btn_n_i[2] | m_two_players;
wire btn_coin  =        ~btn_n_i[3] | m_coin1;

wire kbd_intr;
wire [7:0] kbd_scancode;
wire [7:0] keys_s;

//get scancode from keyboard
io_ps2_keyboard keyboard 
 (
  .clk       ( clk_sys ),
  .kbd_clk   ( ps2_clk_io ),
  .kbd_dat   ( ps2_data_io ),
  .interrupt ( kbd_intr ),
  .scancode  ( kbd_scancode )
);

wire [15:0]joy1_s;
wire [15:0]joy2_s;
wire [8:0]controls_s;
wire osd_enable, direct_video;

//translate scancode to joystick
kbd_joystick #( .OSD_CMD    ( 3'b011 ), .USE_VKP( 1'b1), .CLK_SPEED (24000)) k_joystick
(
    .clk          ( clk_sys ),
    .kbdint       ( kbd_intr ),
    .kbdscancode  ( kbd_scancode ), 
    .direct_video ( direct_video ),

    .joystick_0     ({ joy1_p9_i, joy1_p6_i, joy1_up_i, joy1_down_i, joy1_left_i, joy1_right_i }),
    .joystick_1     ({ joy2_p9_i, joy2_p6_i, joy2_up_i, joy2_down_i, joy2_left_i, joy2_right_i }),
      
    //-- joystick_0 and joystick_1 should be swapped
    .joyswap        ( 0 ),

    //-- player1 and player2 should get both joystick_0 and joystick_1
    .oneplayer      ( oneplayer ),

    //-- tilt, coin4-1, start4-1
    .controls    ( {m_tilt, m_coin4, m_coin3, m_coin2, m_coin1, m_four_players, m_three_players, m_two_players, m_one_player} ),

    //-- fire12-1, up, down, left, right

    .player1     ( {m_fireG,  m_fireF, m_fireE, m_fireD, m_fireC, m_fireB, m_fireA, m_up, m_down, m_left, m_right} ),
    .player2     ( {m_fire2G, m_fire2F, m_fire2E, m_fire2D, m_fire2C, m_fire2B, m_fire2A, m_up2, m_down2, m_left2, m_right2} ),

    //-- keys to the OSD
    .osd_o         ( keys_s ),
    .osd_enable     ( osd_enable ),

    //-- sega joystick
    .sega_strobe    ( joy_p7_o )
        
        
);

endmodule 