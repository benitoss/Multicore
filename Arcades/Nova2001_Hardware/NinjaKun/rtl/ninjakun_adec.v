/*
  
   Multicore 2 / Multicore 2+
  
   Copyright (c) 2017-2020 - Victor Trucco

  
   All rights reserved
  
   Redistribution and use in source and synthezised forms, with or without
   modification, are permitted provided that the following conditions are met:
  
   Redistributions of source code must retain the above copyright notice,
   this list of conditions and the following disclaimer.
  
   Redistributions in synthesized form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
  
   Neither the name of the author nor the names of other contributors may
   be used to endorse or promote products derived from this software without
   specific prior written permission.
  
   THIS CODE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
   AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
   THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
   PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE
   LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
   CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
   INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
   CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
   ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
   POSSIBILITY OF SUCH DAMAGE.
  
   You are responsible for any legal issues arising from your use of this code.
  
*/module ninjakun_adec
(
	input [15:0] CP0AD,
	input			 CP0WR,

	input [15:0] CP1AD,
	input			 CP1WR,

	output		 CS_IN0,
	output		 CS_IN1,

	output		 CS_SH0,
	output		 CS_SH1,

	output		 SYNWR0,
	output		 SYNWR1
);

assign CS_IN0 = (CP0AD[15:2] == 14'b1010_0000_0000_00); 
assign CS_IN1 = (CP1AD[15:2] == 14'b1010_0000_0000_00); 

assign CS_SH0 = (CP0AD[15:11] == 5'b1110_0); 
assign CS_SH1 = (CP1AD[15:11] == 5'b1110_0); 

assign SYNWR0 = CS_IN0 & (CP0AD[1:0]==2) & CP0WR;
assign SYNWR1 = CS_IN1 & (CP1AD[1:0]==2) & CP1WR;

endmodule 