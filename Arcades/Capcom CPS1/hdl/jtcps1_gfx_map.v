/*
  
   Multicore 2 / Multicore 2+
  
   Copyright (c) 2017-2020 - Victor Trucco

  
   All rights reserved
  
   Redistribution and use in source and synthezised forms, with or without
   modification, are permitted provided that the following conditions are met:
  
   Redistributions of source code must retain the above copyright notice,
   this list of conditions and the following disclaimer.
  
   Redistributions in synthesized form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
  
   Neither the name of the author nor the names of other contributors may
   be used to endorse or promote products derived from this software without
   specific prior written permission.
  
   THIS CODE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
   AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
   THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
   PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE
   LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
   CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
   INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
   CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
   ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
   POSSIBILITY OF SUCH DAMAGE.
  
   You are responsible for any legal issues arising from your use of this code.
  
*//*  This file is part of JTCPS1.
    JTCPS1 program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    JTCPS1 program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR a PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with JTCPS1.  If not, see <http://www.gnu.org/licenses/>.

    Author: Jose Tejada Gomez. Twitter: @topapate
    Version: 1.0
    Date: 13-1-2020 */
    
`timescale 1ns/1ps

// A[22:20]   Usage
// 000        OBJ
// 001        SCROLL 1
// 010        SCROLL 2
// 011        SCROLL 3
// 100        Star field
/*
function match;
    input [8:0] code_start;
    input [8:0] code_end;
    input [5:0] gfx_type;
    input [5:0] bank_type;

    match = code_in[8:0] >= code_start && 
            code_in[8:0] <= code_end &&
            (gfx_type & bank_type)!=6'd0;
endfunction

wire [5:0] 
*/
module jtcps1_gfx_map#(
    localparam CFG_LEN=9+9+6+4
)(
    input      [ 2: 0] gfx_type,
    input      [19:10] code_in,    
    output reg [19:10] code_out,
    // Configuration
    input      [ 7:0]  bank0_start,
    input      [ 7:0]  bank1_start,
    input      [ 7:0]  bank2_start,
    input      [ 7:0]  bank3_start,

    input  [CFG_LEN*8-1:0]  config // 3x8=24 bytes
);

wire [ 7:0] match;
wire [79:0] masked;

generate
    genvar m, n0;
    for( m=0; m<8; m=m+1 ) begin : match_array
        n0 = CFG_LEN*m;
        match u_match(
            .code0   ( config[8+n0:n0]     ),
            .code1   ( config[17+n0:8+n0]  ),
            .gfx_type( config[22+n0:18+n0] ),
            .code_in ( code_in             ),
            .code_out( masked[32+n0:23+n0] ),
            .match   ( match[k]            )
        );
    end
endgenerate

reg [7:0]

endmodule