--
-- Multicore 2 / Multicore 2+
--
-- Copyright (c) 2017-2020 - Victor Trucco
--
-- All rights reserved
--
-- Redistribution and use in source and synthezised forms, with or without
-- modification, are permitted provided that the following conditions are met:
--
-- Redistributions of source code must retain the above copyright notice,
-- this list of conditions and the following disclaimer.
--
-- Redistributions in synthesized form must reproduce the above copyright
-- notice, this list of conditions and the following disclaimer in the
-- documentation and/or other materials provided with the distribution.
--
-- Neither the name of the author nor the names of other contributors may
-- be used to endorse or promote products derived from this software without
-- specific prior written permission.
--
-- THIS CODE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
-- AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
-- THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
-- PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE
-- LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
-- CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
-- SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
-- INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
-- CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
-- ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
-- POSSIBILITY OF SUCH DAMAGE.
--
-- You are responsible for any legal issues arising from your use of this code.
--
		
-------------------------------------------------------------------------------
--
-- $Id: cv_console_comp_pack-p.vhd,v 1.3 2006/02/28 22:29:55 arnim Exp $
--
-- Copyright (c) 2006, Arnim Laeuger (arnim.laeuger@gmx.net)
--
-- All rights reserved
--
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

package cv_console_comp_pack is

  component cv_console
    generic (
      is_pal_g        : integer := 0;
      compat_rgb_g    : integer := 0
    );
    port (
      clk_i           : in  std_logic;
      clk_en_10m7_i   : in  std_logic;
      reset_n_i       : in  std_logic;
      por_n_o         : out std_logic;
      ctrl_p1_i       : in  std_logic_vector( 1 downto 0);
      ctrl_p2_i       : in  std_logic_vector( 1 downto 0);
      ctrl_p3_i       : in  std_logic_vector( 1 downto 0);
      ctrl_p4_i       : in  std_logic_vector( 1 downto 0);
      ctrl_p5_o       : out std_logic_vector( 1 downto 0);
      ctrl_p6_i       : in  std_logic_vector( 1 downto 0);
      ctrl_p7_i       : in  std_logic_vector( 1 downto 0);
      ctrl_p8_o       : out std_logic_vector( 1 downto 0);
      ctrl_p9_i       : in  std_logic_vector( 1 downto 0);
      bios_rom_a_o    : out std_logic_vector(12 downto 0);
      bios_rom_ce_n_o : out std_logic;
      bios_rom_d_i    : in  std_logic_vector( 7 downto 0);
      cpu_ram_a_o     : out std_logic_vector( 14 downto 0);
      cpu_ram_ce_n_o  : out std_logic;
      cpu_ram_we_n_o  : out std_logic;
      cpu_ram_d_i     : in  std_logic_vector( 7 downto 0);
      cpu_ram_d_o     : out std_logic_vector( 7 downto 0);
      vram_a_o        : out std_logic_vector(13 downto 0);
      vram_we_o       : out std_logic;
      vram_d_o        : out std_logic_vector( 7 downto 0);
      vram_d_i        : in  std_logic_vector( 7 downto 0);
      cart_a_o        : out std_logic_vector(19 downto 0);
      cart_pages_i    : in  std_logic_vector(5 downto 0);
      cart_en_80_n_o  : out std_logic;
      cart_en_a0_n_o  : out std_logic;
      cart_en_c0_n_o  : out std_logic;
      cart_en_e0_n_o  : out std_logic;
      cart_d_i        : in  std_logic_vector( 7 downto 0);
      col_o           : out std_logic_vector( 3 downto 0);
      rgb_r_o         : out std_logic_vector( 7 downto 0);
      rgb_g_o         : out std_logic_vector( 7 downto 0);
      rgb_b_o         : out std_logic_vector( 7 downto 0);
      hsync_n_o       : out std_logic;
      vsync_n_o       : out std_logic;
      comp_sync_n_o   : out std_logic;
      audio_o         : out signed(7 downto 0)
    );
  end component;

end cv_console_comp_pack;
