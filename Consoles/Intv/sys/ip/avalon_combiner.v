/*
  
   Multicore 2 / Multicore 2+
  
   Copyright (c) 2017-2020 - Victor Trucco

  
   All rights reserved
  
   Redistribution and use in source and synthezised forms, with or without
   modification, are permitted provided that the following conditions are met:
  
   Redistributions of source code must retain the above copyright notice,
   this list of conditions and the following disclaimer.
  
   Redistributions in synthesized form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
  
   Neither the name of the author nor the names of other contributors may
   be used to endorse or promote products derived from this software without
   specific prior written permission.
  
   THIS CODE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
   AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
   THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
   PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE
   LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
   CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
   INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
   CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
   ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
   POSSIBILITY OF SUCH DAMAGE.
  
   You are responsible for any legal issues arising from your use of this code.
  
*/// avalon_combiner.v

`timescale 1 ps / 1 ps
module avalon_combiner
(
	input  wire        clk,                //      clock.clk
	input  wire        rst,                //      reset.reset

	output wire [6:0]  mixer_address,      //  ctl_mixer.address
	output wire [3:0]  mixer_byteenable,   //           .byteenable
	output wire        mixer_write,        //           .write
	output wire [31:0] mixer_writedata,    //           .writedata
	input  wire        mixer_waitrequest,  //           .waitrequest

	output wire [6:0]  scaler_address,     // ctl_scaler.address
	output wire [3:0]  scaler_byteenable,  //           .byteenable
	input  wire        scaler_waitrequest, //           .waitrequest
	output wire        scaler_write,       //           .write
	output wire [31:0] scaler_writedata,   //           .writedata

	output wire [7:0]  video_address,      //  ctl_video.address
	output wire [3:0]  video_byteenable,   //           .byteenable
	input  wire        video_waitrequest,  //           .waitrequest
	output wire        video_write,        //           .write
	output wire [31:0] video_writedata,    //           .writedata

	output wire        clock,              //    control.clock
	output wire        reset,              //           .reset
	input  wire [8:0]  address,            //           .address
	input  wire        write,              //           .write
	input  wire [31:0] writedata,          //           .writedata
	output wire        waitrequest         //           .waitrequest
);

assign clock = clk;
assign reset = rst;

assign mixer_address  = address[6:0];
assign scaler_address = address[6:0];
assign video_address  = address[7:0];

assign mixer_byteenable  = 4'b1111;
assign scaler_byteenable = 4'b1111;
assign video_byteenable  = 4'b1111;

wire   en_scaler = (address[8:7] == 0);
wire   en_mixer  = (address[8:7] == 1);
wire   en_video  =  address[8];

assign mixer_write  = en_mixer  & write;
assign scaler_write = en_scaler & write;
assign video_write  = en_video  & write;

assign mixer_writedata  = writedata;
assign scaler_writedata = writedata;
assign video_writedata  = writedata;

assign waitrequest = (en_mixer & mixer_waitrequest) | (en_scaler & scaler_waitrequest) | (en_video & video_waitrequest);

endmodule
