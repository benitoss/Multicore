--
-- Multicore 2 / Multicore 2+
--
-- Copyright (c) 2017-2020 - Victor Trucco
--
-- All rights reserved
--
-- Redistribution and use in source and synthezised forms, with or without
-- modification, are permitted provided that the following conditions are met:
--
-- Redistributions of source code must retain the above copyright notice,
-- this list of conditions and the following disclaimer.
--
-- Redistributions in synthesized form must reproduce the above copyright
-- notice, this list of conditions and the following disclaimer in the
-- documentation and/or other materials provided with the distribution.
--
-- Neither the name of the author nor the names of other contributors may
-- be used to endorse or promote products derived from this software without
-- specific prior written permission.
--
-- THIS CODE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
-- AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
-- THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
-- PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE
-- LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
-- CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
-- SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
-- INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
-- CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
-- ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
-- POSSIBILITY OF SUCH DAMAGE.
--
-- You are responsible for any legal issues arising from your use of this code.
--
		
library IEEE;
use IEEE.Std_Logic_1164.all;
library STD;
use ieee.numeric_std.all;

package SSP160x_PKG is  

	constant REG_0 	: std_logic_vector(3 downto 0) := x"0";
	constant REG_X 	: std_logic_vector(3 downto 0) := x"1";
	constant REG_Y 	: std_logic_vector(3 downto 0) := x"2";
	constant REG_AH 	: std_logic_vector(3 downto 0) := x"3";
	constant REG_ST 	: std_logic_vector(3 downto 0) := x"4";
	constant REG_SP 	: std_logic_vector(3 downto 0) := x"5";
	constant REG_PC 	: std_logic_vector(3 downto 0) := x"6";
	constant REG_P 	: std_logic_vector(3 downto 0) := x"7";
	constant REG_EXT0 : std_logic_vector(3 downto 0) := x"8";
	constant REG_EXT1 : std_logic_vector(3 downto 0) := x"9";
	constant REG_EXT2 : std_logic_vector(3 downto 0) := x"A";
	constant REG_EXT3 : std_logic_vector(3 downto 0) := x"B";
	constant REG_EXT4 : std_logic_vector(3 downto 0) := x"C";
	constant REG_EXT5 : std_logic_vector(3 downto 0) := x"D";
	constant REG_EXT6 : std_logic_vector(3 downto 0) := x"E";
	constant REG_AL 	: std_logic_vector(3 downto 0) := x"F";
	
	type InstrType_t is (
		IT_NON,
		IT_LD,
		IT_ADD,
		IT_SUB,
		IT_AND,
		IT_OR,
		IT_EOR,
		IT_CMP,
		IT_SHL,
		IT_SHR,
		IT_NEG,
		IT_ABS,
		IT_MLD,
		IT_MPYA,
		IT_MPYS,
		IT_BRA,
		IT_CALL,
		IT_SETI
	);
	
	type InstrAddr_t is (
		IA_NON,
		IA_ACC,
		IA_REGX,
		IA_REGY,
		IA_PTR,
		IA_RAM,
		IA_IMM8,
		IA_IMM16,
		IA_INDPTR,
		IA_INDACC,
		IA_PREG
	);

	type Instr_r is record
		IT		: InstrType_t;
		AS		: InstrAddr_t;
		AD		: InstrAddr_t;
		X 		: std_logic_vector(3 downto 0);
		Y 		: std_logic_vector(3 downto 0);
		RAM 	: std_logic_vector(1 downto 0);
	end record;
	
	
	type PointRegs_t is array (0 to 7) of std_logic_vector(7 downto 0);
	type Stack_t is array (0 to 5) of std_logic_vector(15 downto 0);
	

	function ModAdj(data: std_logic_vector(7 downto 0); m: std_logic_vector(2 downto 0); inc: std_logic) return std_logic_vector;

end SSP160x_PKG;

package body SSP160x_PKG is

	function ModAdj(data: std_logic_vector(7 downto 0); m: std_logic_vector(2 downto 0); inc: std_logic) return std_logic_vector is
		variable mask: std_logic_vector(7 downto 0); 
		variable temp: std_logic_vector(7 downto 0); 
		variable res: std_logic_vector(7 downto 0); 
	begin
		case m is
			when "001" => 	mask := "00000001";
			when "010" => 	mask := "00000011";
			when "011" => 	mask := "00000111";
			when "100" => 	mask := "00001111";
			when "101" => 	mask := "00011111";
			when "110" => 	mask := "00111111";
			when "111" => 	mask := "01111111";
			when others => mask := "11111111";
		end case;
		if inc = '1' then
			temp := std_logic_vector(unsigned(data) + 1);
		else
			temp := std_logic_vector(unsigned(data) - 1);
		end if;
		
		res := (data and not mask) or (temp and mask);
		return res;
	end function;

end package body SSP160x_PKG;
