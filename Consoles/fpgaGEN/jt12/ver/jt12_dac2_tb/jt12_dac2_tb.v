/*
  
   Multicore 2 / Multicore 2+
  
   Copyright (c) 2017-2020 - Victor Trucco

  
   All rights reserved
  
   Redistribution and use in source and synthezised forms, with or without
   modification, are permitted provided that the following conditions are met:
  
   Redistributions of source code must retain the above copyright notice,
   this list of conditions and the following disclaimer.
  
   Redistributions in synthesized form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
  
   Neither the name of the author nor the names of other contributors may
   be used to endorse or promote products derived from this software without
   specific prior written permission.
  
   THIS CODE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
   AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
   THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
   PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE
   LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
   CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
   INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
   CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
   ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
   POSSIBILITY OF SUCH DAMAGE.
  
   You are responsible for any legal issues arising from your use of this code.
  
*/`timescale 1ns / 1ps

module jt12_dac2_tb;

wire dout;
reg rst, clk;

integer w;

initial begin
	$dumpfile("output.lxt");
	$dumpvars;
	$dumpon;
	rst = 1;
	#55 rst = 0;
	for(w=0; w<1000; w=w+1) #1000;
	$finish;
end

initial begin
	clk = 0;
	forever #10 clk = ~clk;
end

jt12_dac2 #(.width(12)) uut
(
	.clk( clk ),
	.rst( rst ),
	.din( 12'h100 ),
	.dout( dout )
);

wire [4:0] syn_sinc1;
wire [8:0] syn_sinc2;
wire [13:0] syn_sinc3;
wire signed [13:0] sinc;

sincf #(.win(1), .wout(5)) sinc1(
	.clk ( clk ),
	.din ( dout ),
	.dout( syn_sinc1 )
);

sincf #(.win(5), .wout(9)) sinc2(
	.clk ( clk ),
	.din ( syn_sinc1 ),
	.dout( syn_sinc2 )
);

sincf #(.win(9), .wout(14)) sinc3(
	.clk ( clk ),
	.din ( syn_sinc2-9'd32 ),
	.dout( syn_sinc3 )
);

assign sinc = (syn_sinc3 + 14'd2048) ^ 14'h2000;

endmodule
